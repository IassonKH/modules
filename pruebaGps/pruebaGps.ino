#include <SoftwareSerial.h>
//Test to try GPS
#define GPSControl 5

SoftwareSerial GPS_Serial(9, 13); //TX from GPS to Arduino Pin 9
const char check = 'A';       //Flag that will tell us if data exists
const char data[6] = "$GPGGA"; //Line of data we'll use + null char
bool newGpsLine = 1;          //bool becomes 0 if the data we want is not sent
bool gpsDataReady = 0;        //to break loop when GPS data is obtained
bool dataValidation = 0;
char longitudeA[12];
char latitudeA[11];
char altitudeA[7];
char rc;

bool GPSPresent=1; 

float latitude=0.0;
float altitude=0.0;
float longitude=0.0;

void setup() {
  Serial.begin(115200);
GPS_Serial.begin(9600);
  pinMode(GPSControl, OUTPUT);

  Serial.println("Initialization complete");
  }

void loop() {
  
  digitalWrite(GPSControl,HIGH);
  //delay(33000);
  while(altitude==0.0){
    checkHeader();
    commaSkip(2);
    getLatitude();
    commaSkip(1);
    getLongitude();
    commaSkip(4);
    getAltitude();
    newLineWait();
  }
  altitude=0.0;
  //digitalWrite(GPSControl,LOW);
}


//-----------------------------------------GPS Management----------------------------
//wait until there's data. Emergency error break in while loop
char readGPS() {
  int emergency = 0;
  char rc;

  while (!GPS_Serial.available()) {
    emergency++;
    delay(10);
    if (emergency > 1000) {
      Serial.println("GPS or Serial fatal error");
      break;
    }
  }
  rc = GPS_Serial.read();
  Serial.print(rc);
  return rc;
}



//------------------------------GPS String Analysis---------------------------------------
//Checks for header stored in data[] array
void checkHeader(){
  GPSPresent=1;
  if (newGpsLine == 1) {
    for (int i = 1; i < 6; i++) {
      rc = readGPS();
      if (rc != data[i]) {
        newGpsLine = 0;
  
      } 
    }
  }  
  longitudeA[12]={0};
  latitudeA[11]={0};
  altitudeA[7]={0};
}

//Read n=number of commas, next data char will be either a comma or a number

void commaSkip(int skippedCommas){
  if (newGpsLine == 1) {
    for (int i = 0; i < skippedCommas; i++) {
        rc='0';
      while(rc!=','){
        rc = readGPS();  
      }
    }
    Serial.println(" ");
  }  
}


//Gets the corresponding latitude only if header was found
void getLatitude() {
  if (newGpsLine == 1) {
    Serial.println(F(" "));
    for (int i = 0; i <= 9; i++) {
      rc = readGPS();
      latitudeA[i] = rc;
      Serial.print(rc);
    }
    if(latitudeA[0]==','){
      Serial.println();
      Serial.println("GPS sign low");
    }else{
    Serial.println("");
    latitudeA[11]='\0';
    latitude = (float)atof(latitudeA);
    latitude = latitude / 100;
      //Determine sign for latitude

      commaSkip(1);

      rc = readGPS();
      if (rc == 'S') {
        latitude = latitude * (-1.0);
      }
    Serial.print(F("Lati "));
    Serial.println(latitude,7);
    Serial.flush();
    }
  }
}

//Gets the corresponsing longitude only if header was found
void getLongitude() {
  if (newGpsLine == 1) {
    Serial.println(F(" "));
    for (int i = 0; i <= 10; i++) {
      rc = readGPS();
      longitudeA[i] = rc;
      Serial.print(rc);
    }
    Serial.println(F(""));
    longitudeA[12]='\0';
    longitude = (float)atof(longitudeA);

      longitude = longitude / 100;

      commaSkip(1);

      rc = readGPS();
      if (rc == 'W') {
        longitude = longitude * (-1.0);
      }
    Serial.println(F("Long "));
    Serial.println(longitude,7);
    Serial.flush();
  }
}


//Gets the corresponsing altitude only if header was found
void getAltitude() {
  if (newGpsLine == 1) {
    Serial.println(F(" "));
    for (int i = 0; i <= 5; i++) {
      rc = readGPS();
      if(rc==','){
      
        break;
      }
      altitudeA[i] = rc;
      
    }
    Serial.println(F(""));
    altitudeA[7]='\0';
    
    altitude = (float)atof(altitudeA);
    altitude=1;
    Serial.println(F("Alti "));
    Serial.println(altitudeA);
    Serial.flush();
  }
}

//Reads bits from GPS until new line is found. New lines all start with '$'
void newLineWait(){
  while(newGpsLine!=1){
    rc=readGPS();
    if(rc=='$') {
      newGpsLine = 1;
      Serial.print(rc);
    }
  }  
}

