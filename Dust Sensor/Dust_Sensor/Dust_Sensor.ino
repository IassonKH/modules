/*
  http://wiki.seeedstudio.com/Grove-Dust_Sensor/
  https://www.seeedstudio.com/Grove-Dust-Sensor%EF%BC%88PPD42NS%EF%BC%89-p-1050.html



#define dust 8
#define sampletime_ms  30000 //30s  for total period

bool state;

void setup() {
  Serial.begin(115200);
  pinMode(dust, INPUT);
  state = digitalRead(dust);

  Serial.println(F("Initialization Complete"));
}

void loop() {

  if (state != digitalRead(dust)) {
    !state;
    Serial.print(millis());
    Serial.print(" st: ");
    Serial.println(state);
    state = digitalRead(dust);
  }

  //readDust();
  //delay(1); //wait 5 seconds
}

void readDust() {

  unsigned long startTime = millis();//get the current time;
  unsigned long duration = pulseIn(dust, LOW); //pulse in will let sometime pass


  Serial.print(F("S time "));

  Serial.println(startTime);

  Serial.print(F("Dur "));

  Serial.println(duration);

  //if the sampel time == 30s>>
  if ((millis() - startTime) > 30000) {

    float ratio = duration / (sampletime_ms * 10.0); // Integer percentage 0%-100%
    float concentration = 1.1 * pow(ratio, 3) - 3.8 * pow(ratio, 2) + 520 * ratio + 0.62; // using spec sheet curve

    Serial.print(duration);
    Serial.print(F(","));
    Serial.print(ratio);
    Serial.print(F(","));
    Serial.println(concentration);

  }


}*/

/*This program is used to read Groove Dust Sensor, 
 it uses data sheet and pulse function to determine values
 */

int pin = 8;
unsigned long duration;
unsigned long starttime;
unsigned long sampletime_ms = 30000;//sampe 30s ;
unsigned long lowpulseoccupancy = 0;
float ratio = 0;
float concentration = 0;

void setup() 
{
    Serial.begin(115200);
    pinMode(pin,INPUT);
    starttime = millis();//get the current time;
}

void loop() 
{
    duration = pulseIn(pin, LOW);
    lowpulseoccupancy = lowpulseoccupancy+duration;

    if ((millis()-starttime) > sampletime_ms)//if the sampel time == 30s
    {
        ratio = lowpulseoccupancy/(sampletime_ms*10.0);  // Integer percentage 0=>100
        concentration = 1.1*pow(ratio,3)-3.8*pow(ratio,2)+520*ratio+0.62; // using spec sheet curve
        Serial.print(lowpulseoccupancy);
        Serial.print(",");
        Serial.print(ratio);
        Serial.print(",");
        Serial.println(concentration);
        lowpulseoccupancy = 0;
        starttime = millis();
    }
}


