/*
  PCINT2

  Pin D0 TX Serial Programming/debug
  Pin D1 RX Serial Programming/debug
  Pin D2 Anemometer Interrupts for wind direction (green wire)
  Pin D3 Anemometer Interrupts for wind speed (yellow wire)
  Pin D4 Stepper Enable for Iridium module
  Pin D5 GPS Vcc, turns on or off
  Pin D6
  Pin D7 SDI-12 Data Pin for soil sensors(blue wire)

  PCINT0

  Pin D8 Higrometer Input Capture Unit
  Pin D9 TX from GPS
  Pin D10 Thermometer Data
  Pin D11 RX Iridium Satellite
  Pin D12 TX Iridium Satellite
  Pin D13

  PCINT1

  Pin A0  Switch Select 1 (14)
  Pin A1  Switch Select 2 (15)
  Pin A2  Push-Button
  Pin A3
  Pin A4 SDA from RTC
  Pin A5 SCL from RTC
  Pin A6
  Pin A7

  Switch States
  |A0 | A1 |
  | 0 |  0 | Default Working Mode
  | 0 |  1 | Dev/debug Mode (serial messages printed and reduced run time)
  | 1 |  0 | SDI Address Assign Mode Discovery
  | 1 |  1 |
*/

#include "SDI12.h"

#define POWERPIN -1
#define DATAPIN 7
#define LED 13
#define Switch1 14
#define Switch2 15
#define Button  16

#define APISize 32


//-------------------------------------Macros------------------------------
//Set to 1,0 or read bit at "pos" of byte "by" respectively
#define BOOL_TRUE(by,pos)  by|=pos
#define BOOL_FALSE(by,pos) by&= ~(pos)
#define BOOL_READ(by,pos)  bool(by&pos)

char SDIcount = '1';
bool discovery;

byte count = 0; //number of SDI-12s found
byte index = 0; // Index into array; where the last char was stored


SDI12 mySDI12(DATAPIN);

struct SDI_info {
  char apikey[8];
  char id;
};

struct SDI_info SDI_array[5];

void setup() {
  setSerial();
  initialize();
  pinMode(4, OUTPUT);
  digitalWrite(4, HIGH);
  delay(5);

  //In here we just read state of switch, to avid cluttering main loop we will do all searching here
  //if this mode is accessed we will stay here until switches are changeds
    addressAssign();
  

}

void loop() {
  // put your main code here, to run repeatedly:

}

void initialize() {

  pinMode(LED, OUTPUT);
  pinMode(Switch1, INPUT);
  pinMode(Switch2, INPUT);
  pinMode(Button, INPUT);

  mySDI12.begin();
  delay(500);
  //Serial.println(F("Initialisation Complete"));
}

//-------------------------Serial Management-----------------------------------------
void setSerial() {
  Serial.begin(115200);
  while (!Serial) {
    //wait for port to open
  }
}

//Reads data untill enter is pressed, no more than 32 chars can be stored
char *getCharArray(int arraySize) {
  char *inData = malloc(arraySize * sizeof(char)); // Allocate some space for the string
  if (!inData) {
    return NULL;
  }
  char inChar; // Where to store the character read
  inChar = Serial.read();
  inChar = '0';

  while (!((inChar == '\n') || (inChar == '\r'))) {
    while (Serial.available() > 0) {
      inChar = Serial.read(); // Read a character

      if (inChar == '\n' || inChar == '\r' ) {
        inData[index] = "\0";
        break;
      } else {
        if (index < 31) {
          inData[index] = inChar; // Store it
          index++; // Increment where to write next
          delay(2);
        }
      }
    }
  }
  return inData;
}

//Serial.flush clears output buffer
//Input buffer can have 64 bytes, buffer is cleared by reading all bytes.
//DON'T EVER CALL THIS METHOD IF YOU KNOW DATA IS BEING RECEIVED
void clearSerialBuffer() {
  Serial.flush();
  for (int j = 0; j < 65; j++) {
    Serial.read();
  }
}

//--------------------------------------------Switch Case Controlling--------------------------------

void getState() {
  byte state = 0;

  if (digitalRead(Switch1)) {
    BOOL_TRUE(state, 2);
  } else {
    BOOL_FALSE(state, 2);
  }

  if (digitalRead(Switch2)) {
    BOOL_TRUE(state, 1);
  } else {
    BOOL_FALSE(state, 1);
  }

  switch (state) {
    case 0:
      //normal working mode
      blinky(1);
      Serial.println(F("Normal"));
      break;
    case 1:
      //Debug mode
      Serial.println(F("Debug"));
#define DEVMOD
      blinky(2);
      break;
    case 2:
      //Discovery SDI-12 mode
      discovery = 1;
      blinky(3);

      Serial.println(F("Discovery"));
      break;
    case 4:
      blinky(4);

      Serial.println(F("Extra"));
      break;
    default:

      Serial.println(F("Default Err"));
      break;
  }

}

//Method for giving new address to sensors
/*1-Disconnect all sensors.
  //2-Connect new sensor to data Pin.
  //3-Write APIKey you want associated to sensor in terminal
  //4-Press enter. Leds will flash when done
  //3-Disconnect all sensors and connect new sensor
  //4-Press push button
  //5-Repeat 1-4 for all sensors
*/
void addressAssign() {

  bool check = false;
  int arraySize = int(APISize) - 1;
  char pos;
  char *readData;

  Serial.println(F("Assigning addresses Routine"));
  Serial.println(F("Please push button when only one sensor is connected"));
  Serial.println(F(""));

  while (1) {

    buttonWait();

    Serial.println(F("Scanning..."));
    pos = '!';
    while (pos == '!') {
      pos = SDI_Search();
    }

    Serial.println(F(""));
    Serial.print(F("Sensor id was: "));
    Serial.println(pos);
    reassignAddress(pos);
    blinky(3);
    Serial.println(F("Sensor id is: "));
    Serial.println(char(SDIcount - 1));

    Serial.println(F(""));
    printInfo(pos);

    Serial.println(F("Enter 8 alphanumerical chars then press enter"));
    while (!check) {
      readData = getCharArray(arraySize);
      if (readData != NULL) {
        Serial.print(F("Entered: "));
        for (int i = 0; i <= index; i++) {
          Serial.print(readData[i]);
        }
        Serial.println(F(""));;
        check = checkAPIKey(readData);
        index = 0;
      }
    }
    clearSerialBuffer();
    check = false;


    SDI_array[count].id = pos;
    for (int i = 0; i < 8; i++) {
      SDI_array[count].apikey[i] = readData[i];
    }
    Serial.print(F("Device id: "));
    Serial.print(SDI_array[count].id);
    Serial.print(F("  API key: "));
    Serial.println(SDI_array[count].apikey);

    count++;
    free(readData);

    Serial.println(F("Please insert new sensor and then push button"));

  }
}



char SDI_Search() {

  char pos = '!';
  //Search for sdi12 devices on bus

  for (byte i = '0'; i <= '9'; i++) {
    if (checkActive(i)) pos = (char) i;
  }

  for (byte i = 'a'; i <= 'z'; i++) {
    if (checkActive(i))pos = (char) i;
  }

  for (byte i = 'A'; i <= 'Z'; i++) {
    if (checkActive(i)) pos = (char) i;
  }

  return pos;
}

void reassignAddress(char pos) {
  String myCommand;
  Serial.println(F("Readdressing sensor"));
  myCommand = "";
  myCommand += (char) pos;
  myCommand += "A";
  myCommand += (char) SDIcount;
  myCommand += "!";
  mySDI12.sendCommand(myCommand);

  delay(300);
  mySDI12.clearBuffer();

  if (SDIcount == '9') {
    SDIcount = 'a';
  } else if (SDIcount == 'z') {
    SDIcount = 'A';
  } else if (SDIcount == 'Z') {
    Serial.println(F("No more sensors can be addressed!"));
    while (1) {
      blinky(1);
    }
  } else {
    SDIcount++;
  }

  Serial.println(F("Success. Rescanning for verification."));
}


boolean checkActive(byte i) {
  // this checks for activity at a particular address
  String myCommand = "";
  myCommand = "";
  myCommand += (char) i;                 // sends basic 'acknowledge' command [address][!]
  myCommand += "!";

  for (int j = 0; j < 3; j++) {          // goes through three rapid contact attempts
    mySDI12.sendCommand(myCommand);
    if (mySDI12.available() > 1) break;
    delay(30);
  }
  if (mySDI12.available() > 2) {

    Serial.print(F("Addr "));
    Serial.print((char)i);
    Serial.print("...");
    // if it hears anything it assumes the address is occupied
    Serial.println(F("Taken"));
    mySDI12.clearBuffer();
    return true;
  }
  else {         // otherwise it is vacant.
    mySDI12.clearBuffer();
  }
  return false;
}



// gets identification information from a sensor, and prints it to the serial port
// expects a character between '0'-'9', 'a'-'z', or 'A'-'Z'.
void printInfo(char i) {
  String  soilSensorInfo = "";
  int j;
  String command = "";
  command += (char) i;
  command += "I!";
  for (j = 0; j < 1; j++) {
    mySDI12.sendCommand(command);
    delay(30);
    if (mySDI12.available() > 1) break;
    if (mySDI12.available()) mySDI12.read();
  }

  while (mySDI12.available()) {
    char c = mySDI12.read();
    if ((c != '\n') && (c != '\r')) soilSensorInfo = soilSensorInfo + c;
    delay(5);
  }

  Serial.println(F("Soil Sensor Information: "));
  Serial.println(soilSensorInfo);
  Serial.println(F(""));;
}


bool checkAPIKey(char *API) {
  bool check = false;
  Serial.print(F("Total characters: "));
  Serial.println(index);
  if (index == 8) {
    check = true;
  } else {
    Serial.println(F("Wrong size"));
  }
  for (int i = 0; i < index; i++) {
    if ( (!isdigit(API[i])) && (!isalpha(API[i]))) {
      Serial.print(F("Non permitted: "));
      Serial.println(API[i]);
      check = false;
    }
  }

  if (check) {
    Serial.println(F("API ok"));
  } else {
    Serial.println(F("Reenter 8 alphanumeric characters"));
  }
  Serial.println(F(""));;

  return check;
}



//Generic Polling Button Routine with Debouncing
void buttonWait() {
  while (1) {
    if (digitalRead(Button) == LOW) {
      delay(15);
      if (digitalRead(Button) == LOW) {
        break;
      }
    }
  }
}



void blinky(int times) {
  for (int i = 0; i++; i < times) {
    digitalWrite(LED, LOW);
    delay(500);
    digitalWrite(LED, HIGH);
    delay(500);
    digitalWrite(LED, LOW);
    delay(500);
  }
}

