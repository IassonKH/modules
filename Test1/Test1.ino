
/*
  PCINT2

  Pin D0 TX Serial Programming/debug
  Pin D1 RX Serial Programming/debug
  Pin D2
  Pin D3
  Pin D4 Stepper Enable for Iridium module
  Pin D5 GPS Vcc, turns on or off
  Pin D6 RTC Vcc, turn on to permit reading. Only relevant with ds1307
  Pin D7 SDI12 data line

  PCINT0

  Pin D8 "Sleep" Iridium. Not Really used
  Pin D9 TX from GPS
  Pin D10 Thermometer Data
  Pin D11 RX Iridium Satellite
  Pin D12 TX Iridium Satellite
  Pin D13 RX from GPS. really not used

  PCINT1

  Pin A0
  Pin A1
  Pin A2
  Pin A3
  Pin A4 SDA from RTC
  Pin A5 SCL from RTC
  Pin A6
  Pin A7
*/

//------------------------------------Application Notes------------------
/*
  Transistor must be used for powering up GPS with a digital pin, since not enough current can be given by the pins
  Pin D13 is technically free most of the time. GPS cannot recieve data and Iridium is powered down with stepper
  even so, care must be taken when using said pin.

    Digital Pins 0-7  - Port D - PCINT2
    Digital Pins 8-13 - Port B - PCINT0
    Analog Pins A0-A5 - Port C - PCINT1

  SDI and software Serial both try to assign this pins. SDI is in pcint 2(pin 7), sw keeps everyother one
  modify .cpp by commenting out declaration as seen here depending on application:
  https://github.com/EnviroDIY/Arduino-SDI-12/issues/8
  Software Serial.cpp is in your computer at Arduino/hardware/arduino/avr/libraries/SoftwareSerial

  DO NOT USE ANY SOFTWARE SERIALS OR INTERRUPTS ON DIGITAL PINS 0-7!!!

  SDI12 library taken from here: https://github.com/EnviroDIY/Arduino-SDI-12
  Iridium library taken from here:
  RTClibrary taken from here:
  SimpleTimer library taken from here:
*/

//------------------------------------Definitions------------------------

//#define ONE_BUS
#define DS1307_I2C_ADDRESS 0x68

//Pins 4 and 5 for transistor switching, 13 is debug Led
#define IridiumControl 4
#define GPSControl 5
#define RTCControl 6
#define LED 13
#define SDI12Power -1 //if powered by a Pin define it here
#define SDI12Data 7   //Pin in which data lines will be conected

#define thermoError 10 //thermometer has a problem
#define measurementsNumber 24 //times I will take measurements before sending data
#define GPSTries 10 //do a few GPS read tries to try to obtain data

//bit position of each flag
#define send_Data        1
#define thermoBounce     2
#define newGPSLine       4
#define gpsRoutineDone   8
#define RTCFail          16
#define GPSPresent       32
#define ThermoPresent    64
#define min30            128
#define noGPS_Signal     1
#define success          2
#define noAltitude       4

//-------------------------------------Macros------------------------------
//Set to 1,0 or read bit at "pos" of byte "by" respectively
#define BOOL_TRUE(by,pos)  by|=pos
#define BOOL_FALSE(by,pos) by&= ~(pos)
#define BOOL_READ(by,pos)  bool(by&pos)

//-------------------------------------Libraries---------------------------
#include "IridiumSBD.h"
#include <OneWire.h>
#include <SoftwareSerial.h>
#include "SimpleTimer.h"
#include "RTClib.h"
#include "SDI12.h"
#include "avr/power.h"

//----------------------------------Constants & Global variables----------

byte flags1 = 0;
byte flags2 = 0;

short takeReading = 0; //value in seconds for doing a reading

SimpleTimer timerSeconds; // timer for seconds declared with SimpleTimer Library

byte tries = 0; //time out fatal error variable for satellite comm
byte contT = 0; //Count how many times I've sucessfully read temperature
byte readTries = 0; //Count how many times I've tried to read temperature
byte thermoCounter = 0; //times thermometer wasn't found
byte maxPos; //array positions for highest and lowest temperatures
byte minPos;

unsigned int daysWorking;
int signalQuality = -1; //default value for iridium modem minimum signal quality

unsigned long seconds = 0; // seconds counter
unsigned long referenceTime = 0; //will crash on year 2038
unsigned long currentTime = 0;

static const char dataGPS[6] = "$GPGGA"; //Line of data we'll search for + null char

char longitudeA[12];
char latitudeA[11];
char altitudeA[7];
char rc;

float latitude  = 0.0;
float longitude = 0.0;
float altitude  = 0.0;
float average  = 0.0;

//---------------------------------Virtual Serial----------------------------------
#ifdef ONE_BUS

#define nss General_Serial
#define GPS_Serial  General_Serial

SoftwareSerial General_Serial(11, 12); //TX from devices to Arduino Pin 9
IridiumSBD isbd(nss, 8);  //to sleep iridium
OneWire ds(10);
RTC_DS1307 rtc;
SDI12 mySDI12(SDI12Data);

#else

OneWire  ds(10);  // Sensor on DIGITAL pin 10 (4.7K resistor necessary between pin 10 and 3.3V from source)
SoftwareSerial GPS_Serial(9, 13); //TX from GPS to Arduino Pin 9
SoftwareSerial nss(11, 12);
IridiumSBD isbd(nss, 8); //to sleep iridium, not really used
RTC_DS1307 rtc;
SDI12 mySDI12(SDI12Data);

#endif
//-----------Temperature Variables-----------

struct temperature {
  double measurement;
  unsigned long _time;
};//Struct for Array registering temperatures

temperature temperatures[measurementsNumber];

//Thermometer management variables
byte addr[8];  //address of thermometer
double celsius;

//-------------SDI12----------------------------

// keeps track of 62 possible addresses
// each bit represents an address:
// 1 is active (taken), 0 is inactive (available)
// setTaken('x') will set the proper bit for sensor 'x'
byte addressRegister[8] = {
  0B00000000,
  0B00000000,
  0B00000000,
  0B00000000,
  0B00000000,
  0B00000000,
  0B00000000,
  0B00000000
};

String soilSensorInfo = "";
String soilSensorMeditions1 = "";
String soilSensorMeditions2 = "";
String soilSensorMeditions3 = "";



//-------------------------------------Setup---------------------------------
void setup() {

  initialize();
  //--------------------------Serial Debug-----------------------------------
  setSerial();
  //--------------------------RTC Module-------------------------------------
  setRTC();
  //--------------------------First Run--------------------------------------
  //Gets reference time
  readDS1307time();

  //Serial.println(F("Initialisation Complete"));

}

//-----------------------------------------Loop------------------------------
void loop() {
  timerSeconds.run();
  //----------------Data Read----------------------------------------
  readData();
  //----------------Data Send----------------------------------------
  sendData();
}


void initialize() {

  pinMode(LED, OUTPUT);
  pinMode(IridiumControl, OUTPUT);
  pinMode(RTCControl,OUTPUT);
  digitalWrite(IridiumControl, LOW);
  digitalWrite(GPSControl, LOW);
  digitalWrite(RTCControl,LOW);

  pinMode(GPSControl, OUTPUT);
  pinMode(RTCControl, OUTPUT);

  daysWorking = 0;
  //library is in ms
  timerSeconds.setInterval(1000, secondsCount);

  BOOL_TRUE(flags1, send_Data);
  BOOL_FALSE(flags1, thermoBounce);
  BOOL_TRUE(flags1, newGPSLine);
  BOOL_FALSE(flags1, gpsRoutineDone);
  BOOL_FALSE(flags1, RTCFail);
  BOOL_TRUE(flags1, GPSPresent);
  BOOL_TRUE(flags1, ThermoPresent);
  BOOL_TRUE(flags1, min30);
  BOOL_FALSE(flags2, noGPS_Signal);
  BOOL_FALSE(flags2, success);
  BOOL_FALSE(flags2, noAltitude);
}
//-------------------------------Data Read---------------------------
void readData() {
  //Power up devices used
  //Get time
  if (BOOL_READ(flags1, min30)) {
    BOOL_FALSE(flags1, min30);

    powerUpRoutine();
    readDS1307time();

    //Try to read thermometer 10 times
    BOOL_FALSE(flags1, thermoBounce);

    while (!(BOOL_READ(flags1, thermoBounce))) {
      //If temperature is read, thermoBounce=1
      registerTemperature();

      if (thermoCounter == thermoError) {
        //if all 10 reads fail, send a warning

        BOOL_TRUE(flags1, thermoBounce);
        BOOL_FALSE(flags1, ThermoPresent);
      }
    }
    //If 10 read tries weren't done unsuccesfully, we got a reading
    if (BOOL_READ(flags1, ThermoPresent)) {

      //Store both in temperature Array
      temperatureArray();
    }

    thermoCounter = 0;
    readTries++;

    if (readTries >= measurementsNumber) {
      BOOL_TRUE(flags1, send_Data);
    } else {
      //Power down devices not used
      lowPowerRoutine();
    }
  }
}

//--------------------------------Data Send--------------------------
void sendData() {
  //power up devices used
  if (BOOL_READ(flags1, send_Data)) {

    BOOL_FALSE(flags1, send_Data);

    tempRoutine();
    gpsRoutine();
    satelliteRoutine();

    daysWorking++;
    //Serial.println(daysWorking);
    //reset temperature array once done

    for (short i = 0; i < measurementsNumber; i++) {
      temperatures[i].measurement = 0;
      temperatures[i]._time = 0;
    }
    //reset amount of times I've read sucessfully
    readTries = 0;
    lowPowerRoutine();
  }
  //power down devices not used
}

//-----------------------------------------------Extra Timer------------------------------

/**
   Timer for days, executed every second
*/
void secondsCount() {
  digitalWrite(LED, !digitalRead(LED));
  seconds++;
  takeReading++;
  //1800 seconds = 30 min
  //Minus 1 second to account for thermometer lecture delays
  //Minus 10 seconds to account for power up routines
  if (takeReading >= (3600 - 11)) {
    //Serial.println("30 min passed");
    BOOL_TRUE(flags1, min30);
    takeReading = 0;
  }
  if (seconds >= 86400) {
    //If RTC died, count extra days every 86400 seconds
    if (BOOL_READ(flags1, RTCFail)) {
      //Serial.println("RTC dead");
      referenceTime = referenceTime + seconds;
      daysWorking++;
    }
    seconds = 0;
  }
}

//---------------------------------------------------------Low Power------------------------------
void lowPowerRoutine() {
  //Disable adc
  ADCSRA = 0;
  //turn off unused modules until time to read comes
  power_adc_disable(); // ADC converter
  power_spi_disable(); // SPI
  power_usart0_disable();// Serial (USART)
  //power_timer0_disable();// Timer 0 not turned off to keep millis() running
  power_timer1_disable();// Timer 1
  power_timer2_disable();// Timer 2
  power_twi_disable(); // TWI (I2C)

}

void powerUpRoutine() {
  //ADC, never used, never turned on
  power_spi_enable(); // SPI
  power_usart0_enable(); // Serial (USART)
  //power_timer0_enable(); // Timer 0 never turned off, cause millis() resets
  power_timer1_enable(); // Timer 1
  power_timer2_enable(); // Timer 2
  power_twi_enable(); // TWI (I2C)
  delay(10000);
}




//-------------------------Set Serial-----------------------------------------
void setSerial() {
  Serial.begin(115200);
  while (!Serial) {
    //wait for port to open
  }
}

//----------------------------------------------------------Temperature Management-------------------------------
void tempRoutine() {

  getMaximum();
  getMinimum();
  average = getAverage();

  //Serial.print(F("Max "));
  //Serial.print(temperatures[maxPos].measurement);
  //Serial.print(F(" at: "));
  //Serial.println(temperatures[maxPos]._time);
  //Serial.print(F("Min "));
  //Serial.print(temperatures[minPos].measurement);
  //Serial.print(F(" at: "));
  //Serial.println(temperatures[minPos]._time);
  //Serial.print(F("Av "));
  //Serial.println(average);
  //Serial.flush();
}

void registerTemperature() {
  byte i;
  byte type_s = 0; //to check for thermometer type
  byte present = 0; //
  byte dataTemperature[12]; //temperature data


  if (!ds.search(addr)) {
    ds.reset_search();
    delay(220);
    thermoCounter++;
    return;
  }

  //Check if CRC is valid
  if (OneWire::crc8(addr, 7) != addr[7]) {
    thermoCounter++;
    return;
  }
  //type_s=0 for DS18B20
  /*  Following code finds Thermometer type if unknown
    //Serial.println();
    // the first ROM byte indicates which chip
    switch (addr[0]) {
    case 0x10:
      //Serial.println("  Chip = DS18S20");  // or old DS1820
      type_s = 1;
      break;
    case 0x28:
      //Serial.println("  Chip = DS18B20");
      type_s = 0;
      break;
    case 0x22:
      //Serial.println("  Chip = DS1822");
      type_s = 0;
      break;
    default:
      //Serial.println("Device is not a DS18x20 family device.");
      thermoCounter++;
      return;
    }*/
  thermoCounter = 0;
  BOOL_TRUE(flags1, thermoBounce);

  ds.reset();
  ds.select(addr);
  ds.write(0x44, 1);        // start conversion, with parasite power on

  delay(755);     // A little more than 750 ms here

  present = ds.reset();
  ds.select(addr);
  ds.write(0xBE);         // Read Scratchpad

  /*  //Serial.print("  Data = ");
    //Serial.print(present, HEX);
    //Serial.print(" ");*/
  for ( i = 0; i < 9; i++) {           // 9 bytes
    dataTemperature[i] = ds.read();
  }
  /* //Serial.print(" CRC=");
    //Serial.print(OneWire::crc8(data, 8), HEX);
    //Serial.println();*/
  // Convert the data to actual temperature
  // because the result is a 16 bit signed integer, it should
  // be stored to an "int16_t" type, which is always 16 bits
  // even when compiled on a 32 bit processor.

  int16_t raw = (dataTemperature[1] << 8) | dataTemperature[0];
  if (type_s) {
    raw = raw << 3; // 9 bit resolution default
    if (dataTemperature[7] == 0x10) {
      // "count remain" gives full 12 bit resolution
      raw = (raw & 0xFFF0) + 12 - dataTemperature[6];
    }
  } else {
    byte cfg = (dataTemperature[4] & 0x60);
    // at lower res, the low bits are undefined, so let's zero them
    if (cfg == 0x00) raw = raw & ~7;  // 9 bit resolution, 93.75 ms
    else if (cfg == 0x20) raw = raw & ~3; // 10 bit res, 187.5 ms
    else if (cfg == 0x40) raw = raw & ~1; // 11 bit res, 375 ms
    //// default is 12 bit resolution, 750 ms conversion time
  }
  BOOL_TRUE(flags1, ThermoPresent);
  celsius = (double)raw / 16.0;
}

void temperatureArray() {
  //Serial.println(contT);
  //Serial.println(celsius);
  temperatures[contT].measurement = celsius;
  temperatures[contT]._time = currentTime - referenceTime;
  //Serial.print(F("C:"));
  //Serial.print(temperatures[contT].measurement);
  //Serial.print(F("T ref: "));
  //Serial.print(" / ");
  //Serial.println(temperatures[contT]._time);
  //Serial.flush();
  contT++;

}

//---------------------------------------------------------------Arithmetic Methods---------------------------
void getMaximum() {
  maxPos = 0;
  float maximum = temperatures[0].measurement;
  for (byte i = 1; i < contT; i++) {
    if (maximum < temperatures[i].measurement) {
      maxPos = i;
      maximum = temperatures[i].measurement;
    }
  }
}

void getMinimum() {
  minPos = 0;
  float minimum = temperatures[0].measurement;
  for (byte i = 1; i < contT; i++) {
    if (minimum > temperatures[i].measurement) {
      minPos = i;
      minimum = temperatures[i].measurement;
    }
  }
}

float getAverage() {

  float average = 0;
  for (byte i = 0; i <= contT; i++) {
    average = average + temperatures[i].measurement;
  }
  average = average / contT;
  //Serial.println(F("Day over"));
  contT = 0;


  return average;
}

byte charToDec(char i) {
  if ((i >= '0') && (i <= '9')) return i - '0';
  if ((i >= 'a') && (i <= 'z')) return i - 'a' + 10;
  if ((i >= 'A') && (i <= 'Z')) return i - 'A' + 37;
  else return i;
}

//-----------------------------------------------------------GPS Methods------------------------------------
void gpsRoutine() {
  //Turn GPS on. Wait for start up, being serial comm, wait a bit and start reading.
  digitalWrite(GPSControl, HIGH);
  delay(32000);
  GPS_Serial.begin(9600);

  latitude = 0.0;
  longitude = 0.0;
  altitude = 0.0;

  //check if GPS data is available, then try to get data GPSTries times
  for (byte i = 0; i < GPSTries; i++) {
    BOOL_TRUE(flags2, noGPS_Signal);
    BOOL_FALSE(flags2, noAltitude);
    do {
      checkHeader();
      commaSkip(2);
      getLatitude();
      commaSkip(1);
      getLongitude();
      commaSkip(4);
      getAltitude();
      newLineWait();
    } while ( BOOL_READ(flags1, gpsRoutineDone) != 1);
    BOOL_FALSE(flags1, gpsRoutineDone);
    delay(6000);
    //Serial.print("Tries GPS: ");
    //Serial.println(i);
    if (BOOL_READ(flags2, noGPS_Signal) != 0) {
      break;
    }
  }

  //Turn GPS Off After Routine is Done
  GPS_Serial.end();
  digitalWrite(GPSControl, LOW);
}




//-----------------------------------------GPS Management----------------------------
//wait until there's data. Emergency error break in while loop
char readGPS() {
  byte emergency = 0;
  char rc;
  BOOL_TRUE(flags1, GPSPresent);
  while (!GPS_Serial.available()) {
    emergency++;
    delay(10);
    //if read fails 100 times stop trying to read and send warning, stops gps routine
    if (emergency > 100) {
      //Serial.println(F("GPS err"));
      BOOL_TRUE(flags1, gpsRoutineDone);
      BOOL_FALSE(flags1, GPSPresent);
      BOOL_FALSE(flags2, noGPS_Signal);

      break;
    }
  }
  if ( BOOL_FALSE(flags1, GPSPresent) != 0) {
    rc = GPS_Serial.read();
  }
  return rc;
}



//------------------------------GPS String Analysis----------------------------------


//Checks for header stored in data[] array
void checkHeader() {
  if ( BOOL_READ(flags1, newGPSLine) ) {
    for (byte i = 1; i < 6; i++) {
      rc = readGPS();
      if (rc != dataGPS[i]) {
        BOOL_FALSE(flags1, newGPSLine);
      } else {
        //Serial.print(rc);
      }
    }
  }
}


//Read n=number of commas, next data char will be either a comma or a number
void commaSkip(int skippedCommas) {
  if ( BOOL_TRUE(flags1, newGPSLine) ) {
    for (byte i = 0; i < skippedCommas; i++) {
      rc = '0';
      while (rc != ',') {
        rc = readGPS();
      }
    }
    //Serial.println(" ");
  }
}
//Gets the corresponsing altitude only if header was found
void getAltitude() {
  if ( BOOL_READ(flags1, newGPSLine) && BOOL_READ(flags1, GPSPresent)) {
    //Serial.println(F(""));
    for (byte i = 0; i <= 5; i++) {
      rc = readGPS();
      altitudeA[i] = rc;
      //Serial.print(rc);
      if ((rc != '.') && (rc > '9' || rc < '0')) {
        BOOL_TRUE(flags2, noAltitude);
        BOOL_TRUE(flags1, gpsRoutineDone);
        return 0;
      }
    }
    //Serial.println(F(""));
    altitudeA[7] = '\0';
    altitude = (float)atof(altitudeA);

    //Serial.println(F("Alti "));
    //Serial.println(altitude);
    BOOL_TRUE(flags1, gpsRoutineDone);
  }
}

//Gets the corresponding latitude only if header was found
void getLatitude() {
  if ( BOOL_READ(flags1, newGPSLine) ) {
    //Serial.println(F(""));

    for (byte i = 0; i <= 9; i++) {
      rc = readGPS();
      latitudeA[i] = rc;
      //Serial.print(rc);
      if ((rc > '9' || rc < '0') && (rc != '.')) {

        BOOL_FALSE(flags1, GPSPresent);
        BOOL_FALSE(flags2, noGPS_Signal);
        return 0;
      }
    }
    //Serial.println("");
    latitudeA[11] = '\0';
    latitude = (float)atof(latitudeA);
    latitude = latitude / 100;
    //Determine sign for latitude

    commaSkip(1);

    rc = readGPS();
    if (rc == 'S') {
      latitude = latitude * (-1.0);
    }

    //Serial.print(F("Lat "));
    //Serial.println(latitude, 7);
  }
}


//Gets the corresponsing longitude only if header was found
void getLongitude() {
  if ( BOOL_READ(flags1, newGPSLine) && BOOL_READ(flags1, GPSPresent)) {
    //Serial.println(F(""));
    for (byte i = 0; i <= 10; i++) {
      rc = readGPS();
      longitudeA[i] = rc;
      //Serial.print(rc);
      if ((rc > '9' || rc < '0') && (rc != '.')) {
        BOOL_FALSE(flags1, GPSPresent);
        BOOL_FALSE(flags2, noGPS_Signal);
        return 0;
      }
    }

    //Serial.println(F(""));


    longitudeA[12] = '\0';
    longitude = (float)atof(longitudeA);
    longitude = longitude / 100;

    commaSkip(1);

    rc = readGPS();
    if (rc == 'W') {
      longitude = longitude * (-1.0);
    }

    //Serial.println(F("Long "));
    //Serial.println(longitude, 7);

  }
}

//Reads bits from GPS until new line is found. New lines all start with '$'
void newLineWait() {
  while ( BOOL_READ(flags1, newGPSLine) != 1) {
    rc = readGPS();
    if (rc == '$') {
      BOOL_TRUE(flags1, newGPSLine);
      //Serial.print(rc);
    }
  }
}

//----------------------------------------------------------Iridium Management------------------------------


boolean ISBDCallback() {
  digitalWrite(LED_BUILTIN, (millis() / 1000) % 2 == 1 ? HIGH : LOW);
  return true;
}

void satelliteRoutine() {
  //Turn Iridium on. Wait for start up, being serial comm, wait a bit and start reading.
  digitalWrite(IridiumControl, HIGH);
  //150 sec for start up time, 120 sec minimum
  delay(60000);
  delay(60000);
  delay(30000);

  nss.begin(19200);
  blinky();

  //isbd.attachConsole(Serial);
  isbd.setPowerProfile(1);
  isbd.adjustSendReceiveTimeout(50);
  isbd.begin();

  frameRoutine();
  //reset variables for Iridium, turn off module

  BOOL_FALSE(flags2, success);

  tries = 0;

  isbd.sleep();

  digitalWrite(IridiumControl, LOW);
  //Serial.flush();
}

//Build frame depending on what Data was available
void frameRoutine() {
  //Alternative frame construction, requires testing

  String stringFrame = "6,GX8K2F3T,";
  String temporal;
  //latitude, longitude, altitude
  temporal = String_GPS();
  stringFrame += temporal;

  temporal = String_RTC();
  stringFrame += temporal;

  temporal = String_Thermo();
  stringFrame = stringFrame + temporal;

  temporal = String(daysWorking, DEC);
  stringFrame = stringFrame + temporal;

  temporal = String_CRC(stringFrame);
  stringFrame = '&' + stringFrame + temporal;

  /*
    if (noGPS_Signal == 1) {

    //Serial.println("GPS present");
    String temporal = String(latitude, 7);
    // String temporal = String(latitude, 7);
    stringFrame = String(stringFrame + temporal + ',');

    temporal = String(longitude, 7);
    stringFrame = String(stringFrame + temporal + ',');

    if (!noAltitude) {
      temporal = String(altitudeA);
      stringFrame = String(stringFrame + temporal + ',');
    } else {
      stringFrame = String(stringFrame + ',');
    }

    } else {
    //Serial.println("GPS not present");
    stringFrame = String(stringFrame + ",,,");
    }

    //timestamp
    if (!RTCFail) {
    //Serial.println("RTC present");
    String temporal = String(referenceTime, DEC);
    stringFrame = String(stringFrame + temporal + ',');
    } else {
    //Serial.println("RTC not present");
    stringFrame = String(stringFrame + ',');
    }
    //min, avg, max temperatures
    if (ThermoPresent) {
    //Serial.println("Therm present");
    String temporal = String(temperatures[minPos].measurement, 2);
    stringFrame = String(stringFrame + temporal + ',');
    if (!RTCFail) {
      temporal = String(temperatures[minPos]._time, DEC);
      stringFrame = String(stringFrame + temporal + ',');
    } else {
      stringFrame = String(stringFrame + ',');
    }
    temporal = String(average, 2);
    stringFrame = String(stringFrame + temporal + ',');
    temporal = String(temperatures[maxPos].measurement, 2);
    stringFrame = String(stringFrame + temporal + ',');
    if (!RTCFail) {
      temporal = String(temperatures[maxPos]._time, DEC);
      stringFrame = String(stringFrame + temporal + ',');
    } else {
      stringFrame = String(stringFrame + ',');
    }
    } else {
    //Serial.println("Thermo not present");
    stringFrame = String(stringFrame + ",,,,,");
    }

    temporal = String(daysWorking, DEC);
    stringFrame = String(stringFrame + temporal);

    byte bytes[stringFrame.length() + 1];
    stringFrame.getBytes(bytes, stringFrame.length() + 1);
    byte crc8 = CRC8(bytes, stringFrame.length());
    String crc;

    if (String(crc8, HEX).length() == 1) {
    crc = "0" + String(crc8, HEX);
    } else {
    crc = String(crc8, HEX);
    }

    stringFrame = String("&" + stringFrame + "*" );
    stringFrame = String(stringFrame + crc);
  */
  //char chars[stringFrame.length() + 1];
  char *chars = const_cast<char*>(stringFrame.c_str());
  //stringFrame.toCharArray(chars, stringFrame.length() + 1);

  //Serial.println(chars);
  //Serial.flush();

  int err;

  while (BOOL_READ(flags2, success) == 0) {
    if (tries < 30) {
      err = isbd.getSignalQuality(signalQuality);

      if (err != 0) {
        //Serial.print(F("Sig Quality fail: err "));
        //Serial.println(err);
        tries++;
      }

      //Serial.print(F("Quality "));
      //Serial.println(signalQuality);
      if (signalQuality < 1) {
        tries++;
        err = 11;
      } else {
        err = isbd.sendSBDText(chars);
      }
      if (err != 0) {
        //Serial.print(F("sendText err "));
        //Serial.println(err);
        tries++;
      } else {
        BOOL_TRUE(flags2, success);
      }
    } else {
      BOOL_TRUE(flags2, success);
      //Serial.println(F("Error with SDB module"));
    }
    delay(6000);
  }

  //Serial.println("Frame Routine Done");

}

String String_GPS() {
  String GPS_String = "";
  String temporal;

  if (BOOL_READ(flags2, noGPS_Signal)) {
    //Serial.println("GPS present");
    temporal = String(latitude, 7);
    GPS_String = temporal + ',';

    temporal = String(longitude, 7);
    GPS_String = GPS_String + temporal + ',';

    if (!BOOL_READ(flags2,noAltitude)) {
      temporal = String(altitudeA);
      GPS_String = GPS_String + temporal + ',';
    } else {
      GPS_String = GPS_String + ',';
    }

  } else {
    //Serial.println("GPS not present");
    GPS_String = ",,,";
  }
  //timestamp
  return GPS_String;
}


String String_RTC() {
  String RTC_String = "";
  String temporal;
  //timestamp
  if (!(BOOL_READ(flags1, RTCFail))) {
    //Serial.println("RTC present");
    temporal = String(referenceTime, DEC);
    RTC_String = temporal + ',';
  } else {
    //Serial.println("RTC not present");
    RTC_String = ',';
  }
  return RTC_String;
}

String String_Thermo() {
  String thermo_String = "";
  String temporal;

  if (BOOL_TRUE(flags1, ThermoPresent)) {
    //Serial.println("Therm present");
    temporal = String(temperatures[minPos].measurement, 2);
    thermo_String = thermo_String + temporal + ',';
    if (!(BOOL_READ(flags1, RTCFail))) {
      temporal = String(temperatures[minPos]._time, DEC);
      thermo_String = thermo_String + temporal + ',';
    } else {
      thermo_String = thermo_String + ',';
    }
    temporal = String(average, 2);
    thermo_String = thermo_String + temporal + ',';
    temporal = String(temperatures[maxPos].measurement, 2);
    thermo_String = thermo_String + temporal + ',';
    if (!(BOOL_READ(flags1, RTCFail))) {
      temporal = String(temperatures[maxPos]._time, DEC);
      thermo_String = thermo_String + temporal + ',';
    } else {
      thermo_String = thermo_String + ',';
    }
  } else {
    //Serial.println("Thermo not present");
    thermo_String = thermo_String + ",,,,,";
  }
  return thermo_String;
}


String String_CRC(String stringFrame) {
  String CRC_String = "";
  String crc;

  byte bytes[stringFrame.length() + 1];
  stringFrame.getBytes(bytes, stringFrame.length() + 1);
  byte crc8 = CRC8(bytes, stringFrame.length());


  if (String(crc8, HEX).length() == 1) {
    crc = "0" + String(crc8, HEX);
  } else {
    crc = String(crc8, HEX);
  }

  CRC_String = "*" + crc ;

  return CRC_String;
}

byte CRC8(const byte *data, byte len) {
  byte crc = 0x00;
  while (len--) {
    crc ^= *data++;
    for (byte i = 0; i < 8; i++) {
      if (crc & 0x80) crc = (crc << 1) ^ 0x07;
      else crc <<= 1;
    }
  }
  return crc;
}


//-----------------------------------------------RTC Management--------------------------------------
void setRTC() {
  //RTCFail =1 in case we can't connect to it
  if (! rtc.begin()) {
    BOOL_TRUE(flags1, RTCFail);
    while (1);
  } else {
    BOOL_FALSE(flags1, RTCFail);
  }
}

void readDS1307time() {

  DateTime now = rtc.now();

  //checks if RTC is not present
  if (now.year() == 2165 || now.month() == 165 || now.year() == 165) {
    BOOL_TRUE(flags1, RTCFail);
    //Serial.println("RTC dead");
  } else {
    BOOL_FALSE(flags1, RTCFail);33w
    currentTime = now.unixtime();
  }
  //update todays reference time only if RTC's working and readtries has reset
  if (readTries == 0 && !(BOOL_READ(flags1, RTCFail))) {
    referenceTime = currentTime;
  }

}

void displayTime() {

  // retrieve data from D1307
  readDS1307time();
  DateTime now = rtc.now();
  //Serial.print(now.year(), DEC);
  //Serial.print('/');
  //Serial.print(now.month(), DEC);
  //Serial.print('/');
  //Serial.print(now.day(), DEC);
  //Serial.print(" (");
  //Serial.print(now.hour(), DEC);
  //Serial.print(':');
  //Serial.print(now.minute(), DEC);
  //Serial.print(':');
  //Serial.print(now.second(), DEC);
  //Serial.println();
  //Serial.print("Timestamp");
  //Serial.print(now.unixtime());
  //Serial.print("s = ");
}


void blinky() {

  digitalWrite(LED, HIGH);
  delay(500);
  digitalWrite(LED, LOW);
  delay(500);
  digitalWrite(LED, HIGH);
  delay(500);
  digitalWrite(LED, LOW);
  delay(500);
}

//--------------------------------------------SDI12 Management---------------------------------------


/**
   Recieves the address of the sensor, reads its data, and stores it on a string
   @param {char} i=device id
*/

void printBufferToScreen(char i) {
  String buffer = "";
  mySDI12.read(); // consume address
  mySDI12.read(); // consume address
  while (mySDI12.available()) {
    char c = mySDI12.read();
    if (c == '+' || c == '-') {
      buffer += ',';
      if (c == '-') buffer += '-';
    }
    else if ((c != '\n') && (c != '\r')) {
      buffer += c;
    }
    delay(50);
  }
  if (i == '1') {
    soilSensorMeditions1 = "";
    soilSensorMeditions1 = soilSensorMeditions1 + buffer;
  } else if (i == '2') {
    soilSensorMeditions2 = "";
    soilSensorMeditions2 = soilSensorMeditions2 + buffer;
  } else {
    soilSensorMeditions3 = "";
    soilSensorMeditions3 = soilSensorMeditions3 + buffer;
  }
}


/**
   Recieves the address of the sensor, and sends the commands to read the data
   @param {char} i=device id
*/
void takeMeasurement(char i) {
  String command = "";
  command += i;
  command += "M!"; // SDI-12 measurement command format  [address]['M'][!]
  mySDI12.sendCommand(command);
  // wait for acknowlegement with format [address][ttt (3 char, seconds)][number of measurments available, 0-9]
  String sdiResponse = "";
  delay(30);
  while (mySDI12.available())  // build response string
  {
    char c = mySDI12.read();
    if ((c != '\n') && (c != '\r'))
    {
      sdiResponse += c;
      delay(5);
    }
  }
  mySDI12.clearBuffer();

  // find out how long we have to wait (in seconds).
  unsigned int wait = 0;
  wait = sdiResponse.substring(1, 4).toInt();

  // Set up the number of results to expect
  // int numMeasurements =  sdiResponse.substring(4,5).toInt();

  unsigned long timerStart = millis();
  while ((millis() - timerStart) < (1000 * wait)) {
    if (mySDI12.available()) // sensor can interrupt us to let us know it is done early
    {
      mySDI12.clearBuffer();
      break;
    }
  }
  // Wait for anything else and clear it out
  delay(30);
  mySDI12.clearBuffer();

  // in this example we will only take the 'DO' measurement
  command = "";
  command += i;
  command += "D0!"; // SDI-12 command to get data [address][D][dataOption][!]
  mySDI12.sendCommand(command);
  while (!mySDI12.available() > 1); // wait for acknowlegement
  delay(300); // let the data transfer
  printBufferToScreen(i);
  mySDI12.clearBuffer();
}


/**
   Checks for activity at a particular address
   expects a char, '0'-'9', 'a'-'z', or 'A'-'Z'
   @param {char} i=device id
   @returns {booleanean} checkActive
*/
boolean checkActive(char i) {
  String myCommand = "";
  myCommand = "";
  myCommand += (char) i;                 // sends basic 'acknowledge' command [address][!]
  myCommand += "!";

  for (byte j = 0; j < 3; j++) {          // goes through three rapid contact attempts
    mySDI12.sendCommand(myCommand);
    if (mySDI12.available() > 1) break;
    delay(30);
  }
  if (mySDI12.available() > 2) {   // if it hears anything it assumes the address is occupied
    mySDI12.clearBuffer();
    return true;
  }
  else {   // otherwise it is vacant.
    mySDI12.clearBuffer();
  }
  return false;
}


/**
   This quickly checks if the address has already been taken by an active sensor
   @param {byte} i=Device id
   @returns {byte} addressRegister[i]
*/
boolean isTaken(byte i) {
  i = charToDec(i); // e.g. convert '0' to 0, 'a' to 10, 'Z' to 61.
  byte j = i / 8;   // byte #
  byte k = i % 8;   // bit #
  return addressRegister[j] & (1 << k); // return bit status
}

/**
   This sets the bit in the proper location within the addressRegister
   to record that the sensor is active and the address is taken.
   @param {byte} i=Device id
   @returns {booleanean} initStatus
*/
boolean setTaken(byte i) {
  boolean initStatus = isTaken(i);
  i = charToDec(i); // e.g. convert '0' to 0, 'a' to 10, 'Z' to 61.
  byte j = i / 8;   // byte #
  byte k = i % 8;   // bit #
  addressRegister[j] |= (1 << k);
  return !initStatus; // return false if already taken
}





