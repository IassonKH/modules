/*
  PCINT2

  Pin D0 TX Serial Programming/debug
  Pin D1 RX Serial Programming/debug
  Pin D2 Anemometer Interrupts for wind direction (green wire)
  Pin D3 Anemometer Interrupts for wind speed (yellow wire)
  Pin D4 Stepper Enable for Iridium module
  Pin D5 GPS Vcc, turns on or off
  Pin D6
  Pin D7 SDI-12 Data Pin for soil sensors(blue wire)

  PCINT0

  Pin D8 Higrometer Input Capture Unit
  Pin D9 TX from GPS
  Pin D10 Thermometer Data
  Pin D11 RX Iridium Satellite
  Pin D12 TX Iridium Satellite
  Pin D13

  PCINT1

  Pin A0 Switch Select 1 (14)
  Pin A1 Switch Select 2 (15)
  Pin A2 Push-Button     (16)
  Pin A3 Analog Readings (17)
  Pin A4 SDA from RTC
  Pin A5 SCL from RTC
  Pin A6
  Pin A7

    Switch States
  |A0 | A1 |
  | 0 |  0 | Default Working Mode
  | 0 |  1 | Dev/debug Mode (serial messages printed and reduced run time)
  | 1 |  0 | SDI Address Assign Mode Discovery
  | 1 |  1 |
*/

//------------------------------------Application Notes------------------
/*
  Transistor must be used for powering up GPS with a digital pin, since not enough current can be given by the pins
  Pin D13 is technically free most of the time. GPS cannot recieve data and Iridium is powered down with stepper
  even so, care must be taken when using said pin.

    Digital Pins 0-7  - Port D - PCINT2
    Digital Pins 8-13 - Port B - PCINT0
    Analog Pins A0-A5 - Port C - PCINT1

  SDI and software Serial both try to assign this pins. SDI is in pcint 2(pin 7), sw keeps everyother one
  modify .cpp by commenting out declaration as seen here depending on application:
  https://github.com/EnviroDIY/Arduino-SDI-12/issues/8
  Software Serial.cpp is in your computer at Arduino/hardware/arduino/avr/libraries/SoftwareSerial/src

  DO NOT USE ANY SOFTWARE SERIALS ON DIGITAL PINS 0-7!!!

  SDI12 library taken from here: https://github.com/EnviroDIY/Arduino-SDI-12
  Iridium library taken from here:https://github.com/mikalhart/IridiumSBD
  RTClibrary taken from here: https://github.com/adafruit/RTClib
  SimpleTimer library taken from here: https://github.com/jfturcot/SimpleTimer
*/

//------------------------------------Definitions------------------------
#define DEVMOD
//#define ONE_BUS
#define DS1307_I2C_ADDRESS 0x68


#define vaneInterrupt 0
#define anemometerInterrupt 1

//Pins 4 and 5 for transistor switching, 13 is debug Led
#define vanePin         2
#define anemometerPin   3
#define IridiumControl  4
#define DATAPIN         7
#define LED             13
#define Switch1         14
#define Switch2         15
#define Button          16
#define ADCPin          17

#define thermoError 10 //thermometer has a problem
#define measurementsNumber 1 //times I will take measurements before sending data
#define GPSTries 10 //do a few GPS read tries to try to obtain data


#ifdef DEVMOD
#define SatelliteTries 2
#else
#define SatelliteTries 30
#endif

//bit position of each flag
#define send_Data           1
#define thermoBounce        2
#define newGPSLine          4
#define gpsRoutineDone      8
#define RTCFail             16
#define GPSPresent          32
#define ThermoPresent       64
#define min30               128
#define noGPS_Signal        1
#define success             2
#define noAltitude          4
#define found               8
#define first               16
#define triggered           32
#define interruptAnemometer 1
#define interruptVane       2
#define cycle1              4


//-------------------------------------Macros------------------------------
//Set to 1,0 or read bit at "pos" of byte "by" respectively
#define BOOL_TRUE(by,pos)  by|=pos
#define BOOL_FALSE(by,pos) by&= ~(pos)
#define BOOL_READ(by,pos)  bool(by&pos)

//-------------------------------------Libraries---------------------------
#include "IridiumSBD.h"
#include <OneWire.h>
#include "SoftwareSerial.h"
#include "SimpleTimer.h"
#include "RTClib.h"
#include "avr/power.h"
#include "SDI12.h"

OneWire  ds(10);  // Sensor on DIGITAL pin 10 (4.7K resistor necessary between pin 10 and 3.3V from source)
SoftwareSerial nss(11, 12);
IridiumSBD isbd(nss, 13); //to sleep iridium, not really used
RTC_DS1307 rtc;
SDI12 mySDI12(DATAPIN);

int ADCValue=0;

//--------------------------------------Interrupts---------------------------
//Capture Unit
// timer overflows (every 65536 counts)
ISR(TIMER1_OVF_vect) {
 
}

ISR(TIMER1_CAPT_vect) {
 }

void setup() {
setSerial();
initialize();
}

void loop() {
  
  // read the value from the sensor:
  ADCValue = analogRead(ADCPin);
  float voltage=ADCValue*(5.0/1023.0);
  
  Serial.print(F("ADC "));
  Serial.print(ADCValue);
  Serial.print(F(" V "));
  Serial.println(voltage,2);
 
  digitalWrite(LED, HIGH);
  delay(500);
  digitalWrite(LED, LOW);
  delay(500);
}

//-------------------------Set Serial-----------------------------------------
void setSerial() {
  Serial.begin(115200);
  while (!Serial) {
    //wait for port to open
  }
}

void initialize() {
  Serial.println(F("Initializing"));
  pinMode(LED, OUTPUT);
  pinMode(IridiumControl, OUTPUT);

  pinMode(anemometerPin, INPUT_PULLUP);
  pinMode(vanePin, INPUT_PULLUP);


  mySDI12.begin();
  delay(500);
  //Voltage reference at default is 3.3v for pro mini, 5v for nano
  analogReference(DEFAULT);
  Serial.println(F("Initialisation Complete"));
}
