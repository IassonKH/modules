
//This program takes an int at the start which equals the amount of samples of a RNG
// it will send to a computer to restart it press reset


#define LED             13

//-------------------------------------Macros------------------------------
//Set to 1,0 or read bit at "pos" of byte "by" respectively
#define BOOL_TRUE(by,pos)  by|=pos
#define BOOL_FALSE(by,pos) by&= ~(pos)
#define BOOL_READ(by,pos)  bool(by&pos)

#define baud 115200

#define timeSpace 1

#include "SoftwareSerial.h"
#include "SimpleTimer.h"

long samples = 250;
long counter = 0;
long timeStart;
long timeEnd;
int index = 0;

void setup() {
  // put your setup code here, to run once:
  setSerial();
  randomSeed(1);
  timeStart = millis();
}

void loop() {
  digitalWrite(LED, LOW);
  delay(timeSpace);
  long rnd = 0.0;

  /*char *readData;

  Serial.println(F("Input 4 digits equal to the amount of samples you want."));

  readData = getCharArray(4);

  Serial.println(readData);
  samples = atoi(int(readData));


  if ( (readData != NULL) || (samples!=0) ) {
    Serial.print(F("Total samples in int: "));
    Serial.print(samples);
    Serial.println(F("."));

  } else {
    if (index != 4) {
      Serial.println(F("Wrong index"));
    }
    Serial.println(F("Using 20"));
    samples = 20;
  }
*/

  while (counter < long(samples)) {
    rnd = random(-360.00, 360.00);
    Serial.println(rnd);
    counter++;
    digitalWrite(LED, HIGH);
    delay(timeSpace);
  }

  digitalWrite(LED, LOW);
  timeEnd = millis();
  /*
    Serial.print(F("Done it took: "));
    Serial.print(long(timeEnd - timeStart));
    Serial.println(F(" seconds"));
  */while (true);
}

void setSerial() {
  Serial.begin(long(baud));
  while (!Serial) {
    //wait for port to open
  }
  Serial.println(F("R"));
}





//Reads data untill enter is pressed, no more than 32 chars can be stored
char *getCharArray(int arraySize) {

  char *inData = malloc(arraySize * sizeof(char)); // Allocate some space for the string
  if (!inData) {
    return NULL;
  }
  char inChar; // Where to store the character read
  inChar = Serial.read();
  inChar = '0';

  while (!((inChar == '\n') || (inChar == '\r'))) {
    while (Serial.available() > 0) {
      inChar = Serial.read(); // Read a character

      if (inChar == '\n' || inChar == '\r' ) {
        inData[index] = F("\0");
        break;
      } else {
        if (index < 31) {
          inData[index] = inChar; // Store it
          index++; // Increment where to write next
          delay(2);
        }
      }
    }
  }
  return inData;
}

//Serial.flush clears output buffer
//Input buffer can have 64 bytes, buffer is cleared by reading all bytes.
//DON'T EVER CALL THIS METHOD IF YOU KNOW DATA IS BEING RECEIVED
void clearSerialBuffer() {
  Serial.flush();
  for (int j = 0; j < 65; j++) {
    Serial.read();
  }
}



