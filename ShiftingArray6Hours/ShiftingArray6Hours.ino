float rainAvg[6] = { -1, -1, -1, -1, -1, -1};
float in;
float mm;


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
}

void loop() {

  rainUpdate();
  
  
  Serial.println("Array:");
  for(int i = 0; i < 6; i ++) {
    Serial.print(String(rainAvg[i])+",");
  }
  
  Serial.println();
  Serial.println();
  
  String temporal = String_Rain();
  

  Serial.print(F("String is: "));
  Serial.println(temporal);
  Serial.println();

  delay(5000);
}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          

void rainUpdate() {
  
  shiftRainArray();
}

void shiftRainArray() {
  float shift = rainAvg[0];
  float swap;
  for (int i = 0; i < 6; i ++) {
    swap = rainAvg[i];
    rainAvg[i] = shift;
    shift = swap;
  }
  
  rainAvg[0] = random(100);
  
  in = 0;
  mm = 0;

}



String String_Rain() {

  String rain = "";      //final string
  float averages = 0;  //to temporarily hold numbers
  int cont = 0;          //counts total available numbers for averages

  if (rainAvg[0] == -1) {
    rainAvg[0] = 0;
    rain += F("0,");
  } else {
    averages = rainAvg[0];
    rain += String(averages, 2);
    rain += F(",");
  }

  if (rainAvg[1] == -1) {
    rain += F("0,");
  } else {
    averages = rainAvg[1];
    rain += String(averages, 2);
    rain += F(",");
  }

  if (rainAvg[2] == -1) {
    rain += F("0,");
  } else {
    averages = rainAvg[2];
    rain += String(averages, 2);
    rain += F(",");
  }
  
  averages=0;
  
  for (int j = 3; j < 6; j ++) {
    if (rainAvg[j] == -1) {
      rain += F("0,");
      cont = 0;
      break;
    } else {
      cont = cont + 1;
      Serial.print(F("N added: "));
      Serial.println(rainAvg[j]);
      averages = averages + rainAvg[j];
      Serial.print(F("Avg: "));
      Serial.println(averages);
    }
  }
  if (cont != 0) {
    averages = averages / cont;
    Serial.print(F("Final avg: "));
    Serial.println(averages);
    rain += String(averages, 2);
    rain += F(",");
  }

  return rain;
}


