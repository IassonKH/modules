// WhiteBox Labs -- Tentacle Shield -- Circuit Setup -- I2C only!
// https://www.whiteboxes.ch/tentacle
//
// NOTE: This sketch will work only with circuits in I2C mode, e.g. if you're using the Tentacle Mini
// or the Tentacle on an Arduino without SoftSerial (e.g. Zero)
//
// Tool to help you setup multiple sensor circuits from Atlas Scientific
// It will allow you to control up to 8 Atlas Scientific devices through the I2C bus
//
// THIS IS A TOOL TO SETUP YOUR CIRCUITS INTERACTIVELY. THIS CODE IS NOT INTENDED AS A BOILERPLATE FOR YOUR PROJECT.
//
// This code is intended to work on all Arduinos. If using the Arduino Yun, connect
// to it's serial port. If you want to work with the Yun wirelessly, check out the respective
// Yun version of this example.
//
// USAGE:
//---------------------------------------------------------------------------------------------
// - Set host serial terminal to 9600 baud
// - To open a I2C address, send the number of the address
// - To issue a command, enter it directly to the console.
//
//---------------------------------------------------------------------------------------------
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//---------------------------------------------------------------------------------------------

#include <Wire.h>                          //enable I2C.


#ifdef ARDUINO_SAM_DUE  // Arduino Due  
#define WIRE Wire1
#else
#define WIRE Wire
#endif

char sensorData[30];                     //A 30 byte character array to hold incoming data from the sensors
byte computer_bytes_received = 0;        //We need to know how many characters bytes have been received
byte sensor_bytes_received = 0;          //We need to know how many characters bytes have been received
int channel;                             //INT pointer for channel switching - 0-7 serial, 8-127 I2C addresses
char *cmd;                               //Char pointer used in string parsing


char computerData[20];                   //we make a 20 byte character array to hold incoming data from a pc/mac/other.



void setup() {

  Serial.begin(9600);                    // Set the hardware serial port to 38400
  while (!Serial) ;                      // Leonardo-type arduinos need this to be able to write to the serial port in setup()
  WIRE.begin();                          // enable I2C port.

  intro();         // display startup message
}



void loop() {
  //Serial.println(".");
  if (computer_bytes_received != 0) {            //If input recieved from PC/MAC/other
    cmd = computerData;                          //Set cmd with incoming serial data

    if (String(cmd) == F("scan")) {         // if scan requested
      scan(true);
      computer_bytes_received = 0;               // Reset the var computer_bytes_received to equal 0
      return;
    }
    else if (String(cmd) == F("scani2c")) {
      scan(false);
      computer_bytes_received = 0;               // Reset the var computer_bytes_received to equal 0
      return;
    }
    else {

      // TODO: without loop?
      for (int x = 0; x <= 127; x++) {    //loop through input searching for a channel change request (integer between 0 and 127)
        if (String(cmd) == String(x)) {
          Serial.print(F("channel change"));
          Serial.println( cmd);
          channel = atoi(cmd);      //set channel variable to number 0-127

          if (change_channel()) {   //set MUX switches or I2C address

            Serial.println(F(""));
            Serial.print(F("Channel: "));
            Serial.print(channel);
          }

          computer_bytes_received = 0;                    //Reset the var computer_bytes_received to equal 0
          return;
        }
      }


      if (String(cmd).startsWith(F("i2c,"))) {
        Serial.println(F("type new ID "));
      } else if (String(cmd).startsWith(F("serial,"))) {
        Serial.println(F("No serial"));
      }

    }

    Serial.print("> ");                              // echo to the serial console
    Serial.println(cmd);

    I2C_call();  // send i2c command and wait for answer
    if (sensor_bytes_received > 0) {
      Serial.print("< ");
      Serial.println(sensorData);       //print the data.
    }


    computer_bytes_received = 0;          //Reset the var computer_bytes_received to equal 0
  }

  if (Serial.available() > 0) {                           //If data has been transmitted from an Atlas Scientific device
    computer_bytes_received = Serial.readBytesUntil(13, computerData, 20);   //We read the data sent from the serial monitor(pc/mac/other) until we see a <CR>. We also count how many characters have been received
    computerData[computer_bytes_received] = 0;          //We add a 0 to the spot in the array just after the last character we received.. This will stop us from transmitting incorrect data that may have been left in the buffer
  }
}



boolean change_channel() {                                 //function controls which I2C port is opened. returns true if channel could be changed.

  for (int x = 1; x <= 127; x++) {
    if (channel == x) {
      if ( !check_i2c_connection() ) {                   // check if this I2C port can be opened
        return false;
      }
      return true;
    }
  }
  return false;
}



byte I2C_call() {   
  byte in_char = 0;                        //used as a 1 byte buffer to store in bound bytes from an I2C stamp.
  byte i2c_response_code = 0;              //used to hold the I2C response code.
//function to parse and call I2C commands.
  sensor_bytes_received = 0;                            // reset data counter
  memset(sensorData, 0, sizeof(sensorData));            // clear sensorData array;

  WIRE.beginTransmission(channel);                  //call the circuit by its ID number.
  WIRE.write(cmd);                      //transmit the command that was sent through the serial port.
  WIRE.endTransmission();                           //end the I2C data transmission.

  i2c_response_code = 254;
  while (i2c_response_code == 254) {      // in case the cammand takes longer to process, we keep looping here until we get a success or an error

    if (String(cmd).startsWith("cal") || String(cmd).startsWith("Cal") ) {
      delay(1400);                        // cal-commands take 1300ms or more
    } else if (String(cmd) == "r" || String(cmd) == "R") {
      delay(1000);                        // reading command takes about a second
    }
    else {
      delay(300);                         // all other commands: wait 300ms
    }

    WIRE.requestFrom(channel, 32);    //call the circuit and request 48 bytes (this is more then we need).
    i2c_response_code = WIRE.read();      //the first byte is the response code, we read this separately.

    while (WIRE.available()) {            //are there bytes to receive.
      in_char = WIRE.read();              //receive a byte.

      if (in_char == 0) {                 //if we see that we have been sent a null command.
        while (WIRE.available()) {
          WIRE.read();  // some arduinos (e.g. ZERO) put padding zeroes in the receiving buffer (up to the number of requested bytes)
        }
        break;                            //exit the while loop.
      }
      else {
        sensorData[sensor_bytes_received] = in_char;        //load this byte into our array.
        sensor_bytes_received++;
      }
    }
    /*
        switch (i2c_response_code) {         //switch case based on what the response code is.
          case 1:                          //decimal 1.
            Serial.println( "< success");     //means the command was successful.
            break;                           //exits the switch case.

          case 2:                          //decimal 2.
            Serial.println( "< command failed");      //means the command has failed.
            break;                           //exits the switch case.

          case 254:                        //decimal 254.
            Serial.println( "< command pending");     //means the command has not yet been finished calculating.
            break;                           //exits the switch case.

          case 255:                        //decimal 255.
            Serial.println( "No Data");     //means there is no further data to send.
            break;                           //exits the switch case.
        }*/
  }
}



boolean check_i2c_connection() {                      // check selected i2c channel/address. verify that it's working by requesting info about the stamp

  byte error;
  byte retries = 0;

  while (retries < 3) {
    retries++;
    WIRE.beginTransmission(channel);      // just do a short connection attempt without command to scan i2c for devices
    error = WIRE.endTransmission();

    if (error == 0) {

      int r_retries = 0;
      while (r_retries < 3) {
        r_retries++;

        cmd = "i";                          // set cmd to request info (in I2C_call())
        I2C_call();

        if (parseInfo()) {
          return true;
        }
      }

      return false;
    } else {
      return false;                      // no device at this address
    }
  }
}

// PH EZO  -> '?I,pH,1.1'
// ORP EZO -> '?I,OR,1.0'   (-> wrong in documentation 'OR' instead of 'ORP')
// DO EZO  -> '?I,D.O.,1.0' || '?I,DO,1.7' (-> exists in D.O. and DO form)
// EC EZO  -> '?I,EC,1.0 '
// Legazy PH  -> 'P,V5.0,5/13'
// Legazy ORP -> 'O,V4.4,2/13'
// Legazy DO  -> 'D,V5.0,1/13'
// Legazy EC  -> 'E,V3.1,5/13'

boolean parseInfo() {                  // parses the answer to a "i" command. returns true if answer was parseable, false if not.

  if (sensorData[0] == '?' && sensorData[1] == 'I') {          // seems to be an EZO stamp

    // PH EZO
    if (sensorData[3] == 'p' && sensorData[4] == 'H') {
      Serial.println(F("pH EZO"));
      return true;
      // ORP EZO
    }
    else if (sensorData[3] == 'O' && sensorData[4] == 'R') {
      Serial.println(F("ORP EZO"));
      return true;
      // DO EZO
    }
    else if (sensorData[3] == 'D' && sensorData[4] == 'O') {
      Serial.println(F("DO EZO"));
      return true;

      // D.O. EZO
    }
    else if (sensorData[3] == 'D' && sensorData[4] == '.' && sensorData[5] == 'O' && sensorData[6] == '.') {
      Serial.println(F("DO EZO"));
      return true;

      // EC EZO
    }
    else if (sensorData[3] == 'E' && sensorData[4] == 'C') {
      Serial.println(F("EC EZO"));
      return true;

    } else {
      Serial.println(F("Unknown Stamp"));
      return false;
    }

  } else {
    if ( sensorData[0] == 'P') {
      Serial.println(F("pH legacy"));
      return true;

    } else if ( sensorData[0] == 'O') {
      Serial.println(F("ORP legacy"));

      return true;

    } else if ( sensorData[0] == 'D') {
      Serial.println(F("DO legacy"));

      return true;

    } else if ( sensorData[0] == 'E') {
      Serial.println(F("EC legacy"));
      return true;
    }
  }

  return false;        // can not parse this info-string
}



void scan(boolean scanserial) {                      // Scan for all devices.
  int stamp_amount = 0;

  for (channel = 1; channel < 127; channel++ ) {
    if (change_channel()) {
      stamp_amount++;
      Serial.print(F("I2C "));
      Serial.print(channel);;
      Serial.println();
    }
  }
  Serial.println(F(""));
  Serial.print(stamp_amount);
  Serial.println(F(" devices found"));
}

void intro() {
  Serial.flush();
  Serial.println(F("Make sure devices are in I2C (blue lights)"));
  Serial.println(F("Type channel 0-127 or scan "));
}
