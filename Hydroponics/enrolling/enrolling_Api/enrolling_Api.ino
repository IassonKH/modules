#define secondsDelay 30000
/*^^^^^^^^^^^^^^^^^^^^^^
  secondsDelay is delay in miliseconds between each read/send attempt
  Ex= 30,000 is 30 seconds wait time
*/

#include <Wire.h>                          //enable I2C.

#define deviceNumber 4
#define WIRE Wire

#define APISize 32

#define normal    0 //00000000 means we are in normal operating mode
#define discovery 1 //000000001 means we are in search mode 
#define debug     2 //000000002 means we are in search mode 
#define extra     4 //000000004 means we are in search mode
#define Switch3         13
#define Switch1         14
#define Switch2         15
#define Button          16


char sensorData[30];                     //A 30 byte character array to hold incoming data from the sensors
byte computer_bytes_received = 0;        //We need to know how many characters bytes have been received
byte sensor_bytes_received = 0;          //We need to know how many characters bytes have been received
int channel;                             //INT pointer for channel switching - 0-7 serial, 8-127 I2C addresses
char *cmd;                               //Char pointer used in string parsin

char computerdata[20];                   //we make a 20 byte character array to hold incoming data from a pc/mac/other.
byte index = 0; // Index into array; where the last char was stored


struct atlasDevice {
  int id;
  char apiKey[8];
};

atlasDevice atlas_Array[deviceNumber];


//-------------------------------------Macros------------------------------
//Set to 1,0 or read bit at "pos" of byte "by" respectively
#define BOOL_TRUE(by,pos)  by|=pos
#define BOOL_FALSE(by,pos) by&= ~(pos)
#define BOOL_READ(by,pos)  bool(by&pos)


void setup() {

  initialize();

  atlasRoutine();   //this methods finds connected hydroponics devices and asks an id for each one

  ////Serial.println(F("Program will continously print the found devices info"));
  //Serial.println("");
}

void loop() {
  atlasPrint();

  delay(secondsDelay);

}

void initialize() {

  Serial.begin(9600);                    // Set the hardware serial port to 9600
  while (!Serial) ;
  WIRE.begin();
  //Serial.println(F("Finding devices, will print their type and ids if any."));
  //Serial.println(F("Please follow instructions to register their ApiKeys."));
  //Serial.println("");
  Serial.flush();
}

void atlasRoutine() {

  scan();

}


void atlasPrint() {

  String temporal;
  String stringFrame;
  if (!(stringFrame.reserve(450))) {
    Serial.println(F("Not enough space"));
    return;
  }
  stringFrame = F("GX8K2F3T,");
  byte code = 0;
  byte in_char;
  // put your main code here, to run repeatedly:
  for (int channel = 0; channel < 4; channel++) {

    Wire.beginTransmission(atlas_Array[channel].id);     // call the circuit by its ID number.
    Wire.write('r');                          // request a reading by sending 'r'
    Wire.endTransmission();                         // end the I2C data transmission.

    delay(1000);  // AS circuits need a 1 second before the reading is ready


    sensor_bytes_received = 0;                        // reset data counter
    memset(sensorData, 0, sizeof(sensorData));        // clear sensordata array;

    Wire.requestFrom(atlas_Array[channel].id, 48, 1);    // call the circuit and request 48 bytes (this is more then we need).
    code = Wire.read();

    while (Wire.available()) {          // are there bytes to receive?
      in_char = Wire.read();            // receive a byte.

      if (in_char == 0) {               // null character indicates end of command
        Wire.endTransmission();         // end the I2C data transmission.
        break;                          // exit the while loop, we're done here
      }
      else {
        sensorData[sensor_bytes_received] = in_char;      // append this byte to the sensor data array.
        sensor_bytes_received++;
      }
    }

    //Serial.print(atlas_Array[channel].id);   // print channel name
    //Serial.print(':');
    switch (atlas_Array[channel].id) {
      case 1:
        stringFrame += F("phHydro:");
        break;
      case 2:
        stringFrame += F("orpHydro:");
        break;
      case 3:
        stringFrame += F("doHydro:");
        break;
      case 4:
        stringFrame += ecSeparate(String(sensorData));
        break;
      default:
        break;
    }

    switch (code) {                       // switch case based on what the response code is.
      case 1:                             // decimal 1  means the command was successful.

        switch (atlas_Array[channel].id) {
          case 1:
            stringFrame += sensorData;
            stringFrame += F("@oKRv6cTk");
            break;
          case 2:
            stringFrame += sensorData;
            stringFrame += F("@Tj8sGxjD");
            break;
          case 3:
            stringFrame += sensorData;
            stringFrame += F("@WN3NUCoE");
            break;
          case 4:

            break;
          default:
            break;
        }

        if (atlas_Array[channel].id != deviceNumber) {
          stringFrame += F(",");
        }
        //Serial.print(sensorData);       // print the actual reading
        /*//Serial.print(F(",@"));
          for (int i = 0; i < 8; i++) {
          //Serial.print(atlas_Array[channel].apiKey[i]);
          }
        *///Serial.println("");
        break;                              // exits the switch case.

      case 2:                             // decimal 2 means the command has failed.
        //Serial.println(F("Command failed"));   // print the error
        break;                              // exits the switch case.

      case 254:                           // decimal 254  means the command has not yet been finished calculating.
        //Serial.println(F("Circuit not ready")); // print the error
        break;                              // exits the switch case.

      case 255:                           // decimal 255 means there is no further data to send.

        break;                              // exits the switch case.
    }

  }
  //Serial.println();
  temporal = String_CRC(stringFrame);
  stringFrame = String(F("#")) + stringFrame + temporal;
  Serial.print(stringFrame);
  Serial.print("\r");
  // for loop
  //Serial.println("");
  Serial.flush();
}

bool checkAPIKey(char *API) {
  bool check = false;
  //Serial.print(F("Total characters: "));
  //Serial.println(index);
  if (index == 8) {
    check = true;
  } else {
    //Serial.println(F("Wrong size"));
  }
  for (int i = 0; i < index; i++) {
    if ( (!isdigit(API[i])) && (!isalpha(API[i]))) {
      //Serial.print(F("Non permitted: "));
      //Serial.println(API[i]);
      check = false;
    }
  }

  if (check) {
    //Serial.println(F("API ok"));
  } else {

    //Serial.println(F("Reenter 8 alphanumeric characters"));
  }
  //Serial.println(F(""));;

  return check;
}




//Reads data untill enter is pressed, no more than 32 chars can be stored
char *getCharArray(int arraySize) {
  char *inData = malloc(arraySize * sizeof(char)); // Allocate some space for the string
  if (!inData) {
    return NULL;
  }
  char inChar; // Where to store the character read
  inChar = Serial.read();
  inChar = '0';

  while (!((inChar == '\n') || (inChar == '\r'))) {
    while (Serial.available() > 0) {
      inChar = Serial.read(); // Read a character

      if (inChar == '\n' || inChar == '\r' ) {
        inData[index] = F("\0");
        break;
      } else {
        if (index < 31) {
          inData[index] = inChar; // Store it
          index++; // Increment where to write next
          delay(2);
        }
      }
    }
  }
  return inData;
}

//Serial.flush clears output buffer
//Input buffer can have 64 bytes, buffer is cleared by reading all bytes.
//DON'T EVER CALL THIS METHOD IF YOU KNOW DATA IS BEING RECEIVED
void clearSerialBuffer() {
  Serial.flush();
  for (int j = 0; j < 65; j++) {
    Serial.read();
  }
}


void scan() {
  // Scan for all devices.
  int stamp_Amount = 0;
  byte state;
  int devices = 0;

  while (devices < 4) {
    for (channel = 1; channel < 127; channel++ ) {
      if (change_channel()) {

        //this part of the code automatically assigns the next available ID to the sensor
        //available ranges 1-127
        stamp_Amount++;
        computerdata[0] = 'I';
        computerdata[1] = '2';
        computerdata[2] = 'C';
        computerdata[3] = ',';

        if (stamp_Amount > 9) {
          computerdata[4] = (stamp_Amount / 10) + '0';
          computerdata[5] = (stamp_Amount % 10) + '0';
        } else {
          computerdata[4] = '0';
          computerdata[5] = stamp_Amount + '0';
        }

        computerdata[6] = 0;
        cmd = computerdata;

        I2C_call();

        atlas_Array[stamp_Amount - 1].id = stamp_Amount;

        /*  //Serial.print(F("I2C "));
          //Serial.println(atlas_Array[stamp_Amount - 1].id);
          addressAssign(stamp_Amount - 1);
        */
        devices++;
      }
    }
  }
  //Serial.println(F(""));
  //Serial.print(stamp_Amount);
  //Serial.println(F(" devices found"));

}

void addressAssign(int devicePos) {

  bool check = false;
  int arraySize = int(APISize) - 1;
  char pos;
  char *readData;

  //Serial.println(F(""));

  //Serial.println(F("Enter 8 alphanumerical chars then press enter"));
  Serial.flush();
  while (!check) {
    readData = getCharArray(arraySize);
    if (readData != NULL) {
      //Serial.print(F("Entered: "));
      for (int i = 0; i < index; i++) {
        //Serial.print(readData[i]);
      }
      //Serial.println(F(""));
      //Serial.println(F(""));
      //Serial.println(F(""));
      check = checkAPIKey(readData);
      index = 0;
    }
  }
  clearSerialBuffer();
  check = false;

  for (int i = 0; i < 8; i++) {
    atlas_Array[devicePos].apiKey[i] = readData[i];
  }
}


boolean change_channel() {                                 //function controls which I2C port is opened. returns true if channel could be changed.

  for (int x = 1; x <= 127; x++) {
    if (channel == x) {
      if ( !check_i2c_connection() ) {                   // check if this I2C port can be opened
        return false;
      }
      return true;
    }
  }
  return false;
}




boolean check_i2c_connection() {                      // check selected i2c channel/address. verify that it's working by requesting info about the stamp

  byte error;
  byte retries = 0;

  while (retries < 3) {
    retries++;
    WIRE.beginTransmission(channel);      // just do a short connection attempt without command to scan i2c for devices
    error = WIRE.endTransmission();

    if (error == 0) {

      int r_retries = 0;
      while (r_retries < 3) {
        r_retries++;

        cmd = "i";                          // set cmd to request info (in I2C_call())
        I2C_call();

        if (parseInfo()) {
          return true;
        }
      }

      return false;
    } else {
      return false;                      // no device at this address
    }
  }
}




byte I2C_call() {
  byte in_char = 0;                        //used as a 1 byte buffer to store in bound bytes from an I2C stamp.
  byte i2c_response_code = 0;              //used to hold the I2C response code.
  //function to parse and call I2C commands.
  sensor_bytes_received = 0;                            // reset data counter
  memset(sensorData, 0, sizeof(sensorData));            // clear sensorData array;



  WIRE.beginTransmission(channel);                  //call the circuit by its ID number.
  WIRE.write(cmd);                      //transmit the command that was sent through the serial port.
  WIRE.endTransmission();                           //end the I2C data transmission.

  i2c_response_code = 254;
  while (i2c_response_code == 254) {      // in case the cammand takes longer to process, we keep looping here until we get a success or an error

    if (String(cmd).startsWith(F("cal")) || String(cmd).startsWith(F("Cal")) ) {
      delay(1400);                        // cal-commands take 1300ms or more
    } else if (String(cmd) == "r" || String(cmd) == "R") {
      delay(1000);                        // reading command takes about a second
    }
    else {
      delay(300);                         // all other commands: wait 300ms
    }

    WIRE.requestFrom(channel, 32);    //call the circuit and request 48 bytes (this is more then we need).
    i2c_response_code = WIRE.read();      //the first byte is the response code, we read this separately.

    while (WIRE.available()) {            //are there bytes to receive.
      in_char = WIRE.read();              //receive a byte.

      if (in_char == 0) {                 //if we see that we have been sent a null command.
        while (WIRE.available()) {
          WIRE.read();  // some arduinos (e.g. ZERO) put padding zeroes in the receiving buffer (up to the number of requested bytes)
        }
        break;                            //exit the while loop.
      } else {
        sensorData[sensor_bytes_received] = in_char;        //load this byte into our array.
        sensor_bytes_received++;
      }
    }
  }
}




boolean parseInfo() {                  // parses the answer to a "i" command. returns true if answer was parseable, false if not.

  if (sensorData[0] == '?' && sensorData[1] == 'I') {          // seems to be an EZO stamp

    // PH EZO
    if (sensorData[3] == 'p' && sensorData[4] == 'H') {
      //Serial.print(F("pH EZO "));
      return true;
      // ORP EZO
    }
    else if (sensorData[3] == 'O' && sensorData[4] == 'R') {
      //Serial.print(F("ORP EZO "));
      return true;
      // DO EZO
    }
    else if (sensorData[3] == 'D' && sensorData[4] == 'O') {
      //Serial.print(F("DO EZO "));
      return true;

      // D.O. EZO
    }
    else if (sensorData[3] == 'D' && sensorData[4] == '.' && sensorData[5] == 'O' && sensorData[6] == '.') {
      //Serial.print(F("DO EZO "));
      return true;

      // EC EZO
    }
    else if (sensorData[3] == 'E' && sensorData[4] == 'C') {
      //Serial.print(F("EC EZO "));
      return true;

    } else {
      //Serial.print(F("Unknown Stamp "));
      return false;
    }

  } else {
    if ( sensorData[0] == 'P') {
      //Serial.print(F("pH legacy "));
      return true;

    } else if ( sensorData[0] == 'O') {
      //Serial.print(F("ORP legacy "));

      return true;

    } else if ( sensorData[0] == 'D') {
      //Serial.print(F("DO legacy "));

      return true;

    } else if ( sensorData[0] == 'E') {
      //Serial.print(F("EC legacy "));
      return true;
    }
  }

  return false;        // can not parse this info-string
}


//Generic Polling Button Routine with Debouncing
void buttonWait() {
  //Serial.println(F("Push button"));
  while (1) {
    if (digitalRead(Button) == LOW) {
      delay(45);
      if (digitalRead(Button) == LOW) {
        break;
      }
    }
  }
}


//reads current state of dipswitch and returns result as a byte.
byte getState() {
  byte state = 0;

  if (digitalRead(Switch2)) {
    BOOL_TRUE(state, 2);
  } else {
    BOOL_FALSE(state, 2);
  }

  if (digitalRead(Switch1)) {
    BOOL_TRUE(state, 1);
  } else {
    BOOL_FALSE(state, 1);
  }

  switch (state) {
    case 0:
      //normal working mode
      //Serial.println(F("Normal"));
      break;
    case 1:
      //Discovery SDI-12 mode
      //Serial.println(F("Discovery"));
      break;
    case 2:
      //Debug mode
      //Serial.println(F("Debug"));
      break;

    case 4:
      //Serial.println(F("Extra"));
      break;

    default:
      //Serial.println(F("Default Err"));
      break;
  }
  return state;
}


String String_CRC(String stringFrame) {
  String CRC_String = "";
  String crc;

  byte bytes[stringFrame.length() + 1];
  stringFrame.getBytes(bytes, stringFrame.length() + 1);
  byte crc8 = CRC8(bytes, stringFrame.length());


  if (String(crc8, HEX).length() == 1) {
    crc = "0" + String(crc8, HEX);
  } else {
    crc = String(crc8, HEX);
  }

  CRC_String = "*" + crc ;

  return CRC_String;
}

byte CRC8(const byte * data, int len) {
  byte crc = 0x00;
  while (len--) {
    crc ^= *data++;
    for (byte i = 0; i < 8; i++) {
      if (crc & 0x80) crc = (crc << 1) ^ 0x07;
      else crc <<= 1;
    }
  }
  return crc;
}

//this method separates the EC string and gives it format, it counts commas
String ecSeparate(String ec) {
  int commas = 0; //number of commas to read I will separate
  String result = F("ecHydro:"); //to store the result
  int stringSize = ec.length(); //to read all chars
  int count = 0; //start at pos 0 of string
  char temp; //temporal char to read

  while (count != stringSize) {
    temp = ec.charAt(count);
    if (temp == ',') {
      switch (commas) {
        case 0:
          result += F("@7QaeaRBE,ppmHydro:");
          commas++;
          break;
        case 1:
          result += F("@7QaeaRBE,psuHydro:");
          commas++;
          break;
        case 2:
          result += F("@7QaeaRBE,gravHydro:");
          commas++;
          break;
        default:
          break;
      }
    } else {
      result += temp;
    }
    count++;
  }
  result += F("@7QaeaRBE");

  return result;
}


