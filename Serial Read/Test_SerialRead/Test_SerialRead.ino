#define APISize 32

byte index = 0; // Index into array; where the last char was stored


void setup() {
  // put your setup code here, to run once:
  setSerial();
  Serial.println(F("Ready"));

}

void loop() {
  // put your main code here, to run repeatedly:
  bool check = false;
  int arraySize = int(APISize) - 1;
  char *readData;

  Serial.println(F("Enter 8 alphanumerical chars then press enter"));
  while (!check) {
    readData = getCharArray(arraySize);
    if (readData != NULL) {
      Serial.print(F("Entered: "));
      for (int i = 0; i <= index; i++) {
        Serial.print(readData[i]);
      }
      Serial.println("");
      check = checkAPIKey(readData);
      index = 0;
    }
  }
  clearSerialBuffer();
  check = false;
  free(readData);
}

bool checkAPIKey(char *API) {
  bool check = false;
  Serial.print(F("Total characters: "));
  Serial.println(index);
  if (index == 8) {
    check = true;
  } else {
    Serial.println(F("Wrong size"));
  }
  for (int i = 0; i < index; i++) {
    if ( (!isdigit(API[i])) && (!isalpha(API[i]))) {
      Serial.print(F("Non permitted: "));
      Serial.println(API[i]);
      check = false;
    }
  }

  if (check) {
    Serial.println(F("API ok"));
  } else {
    Serial.println(F("Reenter 8 alphanumeric characters"));
  }
  Serial.println("");

  return check;
}
//-------------------------Set Serial-----------------------------------------

void setSerial() {
  Serial.begin(115200);
  while (!Serial) {
    //wait for port to open
  }
}


char *getCharArray(int arraySize) {

  char *inData = malloc(arraySize * sizeof(char)); // Allocate some space for the string
  if (!inData) {
    return NULL;
  }
  char inChar; // Where to store the character read
  inChar = Serial.read();
  inChar = '0';

  while (!((inChar == '\n') || (inChar == '\r'))) {

    while (Serial.available() > 0) {
      inChar = Serial.read(); // Read a character

      if (inChar == '\n' || inChar == '\r' ) {
        inData[index] = "\0";
        break;

      } else {
        inData[index] = inChar; // Store it
        index++; // Increment where to write next
        delay(2);
      }
    }
  }
  return inData;
}

//Serial.flush clears output buffer
//Input buffer can have 64 bytes, buffer is cleared by reading all bytes.
//DON'T EVER CALL THIS METHOD IF YOU KNOW DATA IS BEING RECEIVED

void clearSerialBuffer() {

  Serial.flush();

  for (int j = 0; j < 65; j++) {
    Serial.read();
  }

}


