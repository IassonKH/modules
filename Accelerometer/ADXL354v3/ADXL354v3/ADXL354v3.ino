#include "Wire.h"
#include "I2Cdev.h"
#include "ADXL345.h"

#define totalAccelerometers 1   //Modify this variable to change the amount of accelerometers, recommended max=3
#define samples 1000
#define ALPHA 0.20              //the Lower the alpha the lower frecuencies are filtered

/*This program reads up to 2 accelerometers connected and gives their readings for a while
 samples modifies how many readings are done. Alhpa is the constant for a LPF
*/

// class default I2C address is 0x53
// specific I2C addresses may be passed as a parameter here
// ALT low = 0x53
// ALT high = 0x1D
ADXL345   accel(ADXL345_ADDRESS_ALT_LOW);
ADXL345 accel2(ADXL345_ADDRESS_ALT_HIGH);

//data for the instantenous state of an accelerometer
struct accelProfile {
  int16_t x, y, z;  //position info for axis
  int16_t filterX, filterY, filterZ; //aceleration info for axis
};
//create an acceleration profile for totalAccelerometers amount of sensors
accelProfile accelProfileNow[(int)totalAccelerometers];

//Initializes accelerometers and gives warnings if they can't be found
void setAccelerometers() {
  // join I2C bus (I2Cdev library doesn't do this automatically)
  Wire.begin();

  Serial.println(F("Initializing I2C devices"));

  accel.initialize();
  // verify connection
  Serial.println(F("Testing device 1"));
  if (accel.testConnection()) {
    Serial.println(F("ADXL345 1 connection successful"));
    accel.setRange(0x3); //0x0=2g range, max sensitivity
    //   accel.setRate(0xD);  //800Hz sampling rate. each  +1 doubles frec up to 0xF
  } else {
    Serial.println( F("ADXL345 1 connection failed"));
    //while (1);
  }

  if (totalAccelerometers > 1) {
    accel2.initialize();
    // verify connection
    if (accel2.testConnection()) {
      Serial.println( F("ADXL345 2 connection successful"));
      accel2.setRange(0x3); //0x0= 2g range, max sensitivity
      //   accel2.setRate(0xD);  //800Hz sampling rate. each  +1 doubles frec up to 0xF
    } else {
      Serial.println( F("ADXL345 2 connection fzzailed"));
      //while (1);
    }
  }
}


//setup  method runs once at start up
void setup() {

  pinMode(A0, OUTPUT); //will be used as CS, have to stay high for I2c
  pinMode(9, OUTPUT);  //will be used as CS, have to stay high for I2c
  pinMode(2, OUTPUT);  //HIGH will be used to define address for I2C = 0x1D. 0x3A write 0x3B read.
  pinMode(3, OUTPUT);  //LOW will  be used to define address for I2C = 0x53. 0xA6 write 0xA7 read.

  digitalWrite(A0, HIGH);
  digitalWrite(9, HIGH);
  digitalWrite(3, LOW);
  digitalWrite(2, HIGH);


  Serial.begin(115200);

  Serial.print(F("Start: "));
  Serial.println(millis());

  setAccelerometers();

  Serial.println(F("R"));
}

//Method is main() with a while(1). We will select and read each accelerometer every 6ms
void loop() {

  for (int j = 0; j < (long)samples; j++) {
    for (int i = 0; i < (int)totalAccelerometers; i++) {
      delay(6);
      updateAccelProfile(i); //updates last acceleration profile of device i
      printAccelInfo(i);     //print lastest acceleration profile of device i
    }
  }

  Serial.print(F("End: "));
  Serial.println(millis());

  while (true);
}

//Input variables is internal int id of accelerometer
//method will update position and instantaneous acceleration profile of device

//Add here low pass filter and correct data read.

void updateAccelProfile(int deviceID) {

  int16_t ax, ay, az;

  if (deviceID >= 0) {

    if (deviceID == 0) {
      accel.getAcceleration(&ax, &ay, &az);
    } else {
      accel2.getAcceleration(&ax, &ay, &az);
    }
    /* Display the results (acceleration is measured in m/s^2 and translated to Gs) */
    accelProfileNow[deviceID].x = ax;// * (float(0.00390625));
    accelProfileNow[deviceID].y = ay;// * (float(0.00390625));
    accelProfileNow[deviceID].z = az;// * (float(0.00390625));

    //low pass filter
    accelProfileNow[deviceID].filterX = (accelProfileNow[deviceID].x * ALPHA) + (accelProfileNow[deviceID].filterX * (1.0 - ALPHA));
    accelProfileNow[deviceID].filterY = (accelProfileNow[deviceID].y * ALPHA) + (accelProfileNow[deviceID].filterY * (1.0 - ALPHA));
    accelProfileNow[deviceID].filterZ = (accelProfileNow[deviceID].z * ALPHA) + (accelProfileNow[deviceID].filterZ * (1.0 - ALPHA));
  } else {
    Serial.println(F("Error, accessing negative ID accelerometer"));
  }
}

//Prints profile of accelerometer i at serial console
void printAccelInfo(int i) {

  if (i == 0) {
    Serial.print(F("f"));
  } else {
    Serial.print(F("s"));
  }

  if (i >= 0) {
    // Output x,y,z values
    Serial.println(accelProfileNow[i].x);
    Serial.println(accelProfileNow[i].y);
    Serial.println(accelProfileNow[i].z);

    Serial.println(accelProfileNow[i].filterX);
    Serial.println(accelProfileNow[i].filterY);
    Serial.println(accelProfileNow[i].filterZ);
  } else {
    Serial.println(F("Error, accessing negative ID accelerometer"));
  }
}



