#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_ADXL345_U.h>

/* Assign a unique ID to this sensor at the same time */
Adafruit_ADXL345_Unified accel = Adafruit_ADXL345_Unified(12345);

#define totalAccelerometers 1   //Modify this variable to change the amount of accelerometers, recommended max=3
#define samples 5

//data for the instantenous state of an accelerometer
struct accelProfile {
  int x, y, z;  //position info for axis
  double xyz[3];//aceleration info for axis
};

//create an acceleration profile for totalAccelerometers amount of sensors
accelProfile accelProfileNow[(int)totalAccelerometers];

void displaySensorDetails(int i) {
  sensor_t sensor;
  if (i == 0) {
    accel.getSensor(&sensor);
  } else {
    accel.getSensor(&sensor);
  }
  Serial.println("------------------------------------");
  Serial.print  ("Sensor:       "); Serial.println(sensor.name);
  Serial.print  ("Driver Ver:   "); Serial.println(sensor.version);
  Serial.print  ("Unique ID:    "); Serial.println(sensor.sensor_id);
  Serial.print  ("Max Value:    "); Serial.print(sensor.max_value); Serial.println(" m/s^2");
  Serial.print  ("Min Value:    "); Serial.print(sensor.min_value); Serial.println(" m/s^2");
  Serial.print  ("Resolution:   "); Serial.print(sensor.resolution); Serial.println(" m/s^2");
  Serial.println("------------------------------------");
  Serial.println("");
  delay(500);
}

//Method runs once at start up. we start serial comms, set ports and initialize accelerometers
void setup() {

  pinMode(A0, OUTPUT); //pin 2 is connected to chip 1, 3 to 2, 4 to 3. Hence the +1
  pinMode(9, OUTPUT);

  Serial.begin(115200);

  Serial.print(F("Start: "));
  Serial.println(millis());
  Serial.println(F("R"));
  setAccelerometers();

}

//Method is main() with a while(1). We will select and read each accelerometer every half second
void loop() {

  while (true) {
  
    delay(1000);
    
    for (int i = 0; i < (int)totalAccelerometers; i++) {
      selectAccel(i);        //selects Slave device with internal id i
      updateAccelProfile(i); //updates last acceleration profile of device i
      printAccelInfo(i);     //print lastest acceleration profile of device i
      Serial.println();
    }
  
  }
}


//method will initialize all acelerometers using their CS
void setAccelerometers() {

  for (int i = 0; i < (int)totalAccelerometers; i++) {
    //turn off all chips with their cs
    switch (i) {
      case 0:
        digitalWrite(A0, LOW);
        digitalWrite(9, LOW);

        if (!accel.begin())
        {
          /* There was a problem detecting the ADXL345 ... check your connections */
          Serial.println("Ooops, no ADXL345 detected ... Check your wiring!");
          while (1);
        }
        /* Set the range to whatever is appropriate for your project */
        accel.setRange(ADXL345_RANGE_16_G);
        // displaySetRange(ADXL345_RANGE_8_G);
        // displaySetRange(ADXL345_RANGE_4_G);
        // displaySetRange(ADXL345_RANGE_2_G);

        /* Display some basic information on this sensor */
        displaySensorDetails(i);
        Serial.println("");

        break;

      case 1:
        digitalWrite(A0, HIGH);
        digitalWrite(9, LOW);
        if (!accel.begin())
        {
          /* There was a problem detecting the ADXL345 ... check your connections */
          Serial.println("Ooops, no ADXL345 detected ... Check your wiring!");
          while (1);
        }

        /* Set the range to whatever is appropriate for your project */
        accel.setRange(ADXL345_RANGE_16_G);
        // displaySetRange(ADXL345_RANGE_8_G);
        // displaySetRange(ADXL345_RANGE_4_G);
        // displaySetRange(ADXL345_RANGE_2_G);

        /* Display some basic information on this sensor */
        displaySensorDetails(i);

        break;

      default:
        Serial.println(F("Out of bounds case statement"));
        break;

    }
  }
}


//turns on and off chips according to which one will be read
void selectAccel(int deviceID) {

  if (deviceID >= 0) {

    switch (deviceID) {
      case 0:
        digitalWrite(2, LOW);
        digitalWrite(3, HIGH);
        break;

      case 1:
        digitalWrite(2, HIGH);
        digitalWrite(3, LOW);
        break;

      default:
        Serial.println(F("Out of bounds case statement"));
        break;

    }
  } else {
    Serial.println(F("Error, accessing negative ID accelerometer"));
  }
}


//Input variables is internal int id of accelerometer
void updateAccelProfile(int deviceID) {

  sensors_event_t event;
  if (deviceID >= 0) {

    if (deviceID == 0) {
      accel.getEvent(&event);
    } else {
      accel.getEvent(&event);
    }
    /* Display the results (acceleration is measured in m/s^2) */
    accelProfileNow[deviceID].x = event.acceleration.x / (float(0.0392266));
    accelProfileNow[deviceID].y = event.acceleration.y / (float(0.0392266));
    accelProfileNow[deviceID].z = event.acceleration.z / (float(0.0392266));
    accelProfileNow[deviceID].xyz[0] = event.acceleration.x;
    accelProfileNow[deviceID].xyz[1] = event.acceleration.y;
    accelProfileNow[deviceID].xyz[2] = event.acceleration.z;

  } else {

    Serial.println(F("Error, accessing negative ID accelerometer"));
  }
}

//Prints profile of accelerometer i at serial console
void printAccelInfo(int i) {

  if (i >= 0) {
    // Output x,y,z values
    Serial.println(accelProfileNow[i].x);
    Serial.println(accelProfileNow[i].y);
    Serial.println(accelProfileNow[i].z);

    // Output acceleration info
    Serial.println(accelProfileNow[i].xyz[0]);
    Serial.println(accelProfileNow[i].xyz[1]);
    Serial.println(accelProfileNow[i].xyz[2]);
  } else {
    Serial.println(F("Error, accessing negative ID accelerometer"));
  }

}



