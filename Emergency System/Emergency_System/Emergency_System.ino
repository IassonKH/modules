/*
  PCINT2

  Pin D0 TX Serial Programming/debug
  Pin D1 RX Serial Programming/debug
  Pin D2 Push button
  Pin D3
  Pin D4 Enable 1
  Pin D5 Enable 2
  Pin D6
  Pin D7

  PCINT0

  Pin D8
  Pin D9  TX from GPS
  Pin D10
  Pin D11 RX Iridium Satellite
  Pin D12 TX Iridium Satellite
  Pin D13 Led

  PCINT1

  Pin A0 Rain Polling
  Pin A1
  Pin A2
  Pin A3 Temperature + humidity sensor Vin
  Pin A4 SDA from RTC
  Pin A5 SCL from RTC
  Pin A6 Analog only
  Pin A7 Analog only
*/

//------------------------------------Application Notes------------------
/*
  Transistor must be used for powering up GPS, Iridium via a digital pin. GPS cannot receive while comms with Iridium are going and vice-versa.

    Digital Pins 0-7  - Port D - PCINT2
    Digital Pins 8-13 - Port B - PCINT0
    Analog Pins A0-A5 - Port C - PCINT1

    DO NOT USE ANY SOFTWARE SERIALS ON DIGITAL PINS 0-7!!!
  Iridium library taken from here:https://github.com/mikalhart/IridiumSBD
  RTClibrary taken from here: https://github.com/adafruit/RTClib
  SimpleTimer library taken from here: https://github.com/jfturcot/SimpleTimer

  This program uses an Iridium and a gps device to keep sending messages to server to give location.
  In case button is pressed once emergency mode is activated and messages are sent with a higher frecuency
  Another button press will change mode and return to normality
*/
//------------------------------------Definitions------------------------

#define DS1307_I2C_ADDRESS 0x68


#define buttonInterrupt 0
//Pins 4 and 5 for transistor switching, 13 is debug Led
#define button          2
#define IridiumControl  4
#define GPSControl      5
#define LED             13

//Number tries I will make to send Data through satellite link
#define SatelliteTries 10
#define GPSTries 10 //do a few GPS read tries to try to obtain data

//bit position of each flag
#define send_Data           1
#define thermoBounce        2
#define newGPSLine          4
#define gpsRoutineDone      8
#define RTCFail             16
#define GPSPresent          32
#define ThermoPresent       64
#define min30               128
#define noGPS_Signal        1
#define success             2
#define noAltitude          4
#define found               8
#define first               16
#define triggered           32
#define interruptAnemometer 1
#define interruptVane       2
#define cycle1              4
#define cycle2              8
#define seconds5            16
#define windGust            32
#define debugFlag           64


//-------------------------------------Macros------------------------------
//Set to 1,0 or read bit at "pos" of byte "by" respectively
#define BOOL_TRUE(by,pos)  by|=pos
#define BOOL_FALSE(by,pos) by&= ~(pos)
#define BOOL_READ(by,pos)  bool(by&pos)

//-------------------------------------Libraries---------------------------
#include "IridiumSBD.h"
#include "SoftwareSerial.h"
#include "SimpleTimer.h"
#include "RTClib.h"
#include "avr/power.h"
#include<Wire.h>

//----------------------------------Modify time intervals-----------------

#define measurementsNumber 1 //times I will take readings before sending data

//----------------------------------Constants & Global variables----------

bool firstRun = true;          //flag for sending first time at power on
bool emergencyButton = false;  //flag for changing sending rate. changes with every button push
bool normalButtonState;       //this Bool will be defined at start up by actually reading the state of the button

byte flags1 = 0;
byte flags2 = 0;
byte flags3 = 0;


short takeReading = 300; //value in seconds between each data acquisition

SimpleTimer timerSeconds; // timer for seconds declared with SimpleTimer Library

byte readTries = 0; //Count how many times I've tried to read data

unsigned long referenceTime = 0; //will reset on year 2038
//days counter
long daysCount = 0;

//receive buffer for serial comms
char rc;

//Variables used for GPS
static const char dataGPS[6] = "$GPGGA"; //Line of data we'll search for + null char

char longitudeA[12];
char latitudeA[11];
char altitudeA[7];

float latitude  = 0.0;
float longitude = 0.0;
float altitude  = 0.0;
float average   = 0.0;

SoftwareSerial GPS_Serial(9, 13); //TX from GPS to Arduino Pin 9
SoftwareSerial nss(11, 12);  //Iridium object created
IridiumSBD isbd(nss, 13); //to sleep iridium, not really used
RTC_DS1307 rtc;  //instance rtc object


//---------------------------------------External Interrupts-------------------------------------------
/*Anemometer Interrupt Routine
  Everytime an interrupt is fired in anemometerPin it will update the new time in case there isn't an overflow
  then it will check if we got interrupts in correct order and update values right away
*/
void button_Polling() {
  noInterrupts();
  //toggle state of device when button is pressed
  detachInterrupt(digitalPinToInterrupt(button));
  emergencyButton = !emergencyButton;

  while (digitalRead(button) != normalButtonState) {

  }
  Serial.println("change");

  //Wait until button is let go. Program can freeze here.
  interrupts();
}



//-----------------------------------------------------MAIN------------------------------------------
/*
  Method runs once at start up, initializes everything
*/
void setup() {
  //--------------------------Serial Debug-----------------------------------
  //sets serial comms
  setSerial();
  //initialize sensors and environment
  initialize();

  //--------------------------RTC Module-------------------------------------
  setRTC();

  //--------------------------First Run--------------------------------------
  //Gets reference time
  readDS1307time();

  readData();
}
/*
  Equivalent of main with a while(1){}
  updates time, then checks if enough time passed to send info
*/
void loop() {
  timerSeconds.run();
  //----------------Data Read----------------------------------------
  //readData();
  //----------------Data Send----------------------------------------
  //sendData();

}

//-------------------------Serial Comms-----------------------------------------------
/*
  Waits for serial comm to start, this depends on the module on the arduino and not on the actual connection with another device
*/
void setSerial() {
  Serial.begin(115200);
  //while (!Serial) {
  //wait for port to open
  //}
}

/*
  Sets pins, flags, and routines
*/
void initialize() {

  Serial.println(F("Boot"));
  //define pins as outputs and inputs
  pinMode(LED, OUTPUT);  //debug on board led

  pinMode(button, INPUT);
  normalButtonState = digitalRead(button);

  pinMode(IridiumControl, OUTPUT); //transistor to Satellite
  digitalWrite(IridiumControl, LOW); //turn off Iridium

  pinMode(GPSControl, OUTPUT); //transistor to GPS
  digitalWrite(GPSControl, LOW);    //turn off GPS

  //library is in ms, will enter "secondsCount" every second approx
  timerSeconds.setInterval(1000, secondsCount);

  //flags for system
  BOOL_TRUE(flags1, send_Data);
  BOOL_FALSE(flags1, thermoBounce);
  BOOL_TRUE(flags1, newGPSLine);
  BOOL_FALSE(flags1, gpsRoutineDone);
  BOOL_FALSE(flags1, RTCFail);
  BOOL_TRUE(flags1, GPSPresent);
  BOOL_TRUE(flags1, ThermoPresent);
  BOOL_TRUE(flags1, min30);
  BOOL_FALSE(flags2, noGPS_Signal);
  BOOL_FALSE(flags2, success);
  BOOL_FALSE(flags2, noAltitude);
  BOOL_FALSE(flags3, interruptAnemometer);
  BOOL_FALSE(flags3, interruptVane);
  BOOL_FALSE(flags3, cycle1);
  BOOL_TRUE(flags3, debugFlag);

  attachInterrupt(digitalPinToInterrupt(button), button_Polling, CHANGE);

  interrupts();
  Serial.println(F("R"));
}
//-------------------------------Data Read---------------------------
//reads thermometer, humidity, RTC
void readData() {

  //hard stop to make sure button isn't pressed
  while (!(digitalRead(button))) {

  }

  Serial.println(F("b"));

  //get time if possible from RTC
  readDS1307time();
  Serial.println(F("c"));

  readTries++;

  sendData();

}

//------------------------------------Data Send--------------------------
/*
  Send data through satellite uplink
*/
void sendData() {

  Serial.println(F("Sending data"));
  //get data from GPS
  gpsRoutine();
  Serial.println(F("1"));

  //try to send Data. If no satellite is connected data will be sent via serial comms
  satelliteRoutine();
  Serial.println(F("2"));

  //enter low power consumption mode
  firstRun = false;
  //reset amount of times I've read sucessfully
  readTries = 0;

  attachInterrupt(digitalPinToInterrupt(button), button_Polling, CHANGE);
}



//-----------------------------------------------Extra Timer------------------------------

//Timer for time period, executed very second
void secondsCount() {
  //Serial.println(seconds);
  //flash led
  digitalWrite(LED, !digitalRead(LED));
  //seconds counts days, takeReading data acquisition period, 5 count and 60 count are for windVane

  takeReading++;

  //if we are not in an emergency, send data every 5 minutes.
  if (emergencyButton == false) {
    if (takeReading >= 300) {
      //Serial.println(F("5 min passed"));
      takeReading = 0;
      Serial.println(F("Normality"));
      readData();
    }
  } else { //else panic and send data constantly
    Serial.println(F("Pressure!"));
    readData();
  }
  /*If RTC died, count extra days every 8 times we read data. 8*3 hour period=24 hours
  */
}


//-----------------------------------------------------------GPS Methods------------------------------------
void gpsRoutine() {
  //Turn GPS on. Wait for start up, being serial comm, wait a bit and start reading.
  digitalWrite(GPSControl, HIGH);

  if (!emergencyButton) {
    delay(32000);
  }
  GPS_Serial.begin(9600);

  //Serial.println("Entering GPS Routine");
  latitude = 0.0;
  longitude = 0.0;
  altitude = 0.0;

  //check if GPS data is available, then try to get data GPSTries times
  for (byte i = 0; i < GPSTries; i++) {
    BOOL_TRUE(flags2, noGPS_Signal);
    BOOL_FALSE(flags2, noAltitude);
    do {
      checkHeader();
      commaSkip(2);
      getLatitude();
      commaSkip(1);
      getLongitude();
      commaSkip(4);
      getAltitude();
      newLineWait();
    } while ( BOOL_READ(flags1, gpsRoutineDone) != 1);
    BOOL_FALSE(flags1, gpsRoutineDone);
#ifndef DEVMOD
    delay(5000);
#endif
    //Serial.print("Tries GPS: ");
    //Serial.println(i);
    if (BOOL_READ(flags2, noGPS_Signal) != 0) {
      break;
    }
  }

  //Turn GPS Off After Routine is Done
  GPS_Serial.end();

  if (!emergencyButton) {
    digitalWrite(GPSControl, LOW);
  }
}




//-----------------------------------------GPS Management----------------------------
//wait until there's data. Emergency error break in while loop
char readGPS() {
  byte emergency = 0;
  char rc;
  BOOL_TRUE(flags1, GPSPresent);
  while (!GPS_Serial.available()) {
    emergency++;
    delay(10);
    //if read fails 100 times stop trying to read and send warning, stops gps routine
    if (emergency > 100) {
      //Serial.println(F("GPS err"));
      BOOL_TRUE(flags1, gpsRoutineDone);
      BOOL_FALSE(flags1, GPSPresent);
      BOOL_FALSE(flags2, noGPS_Signal);

      break;
    }
  }
  if ( BOOL_READ(flags1, GPSPresent) != 0) {
    rc = GPS_Serial.read();
  }
  return rc;
}



//------------------------------GPS String Analysis----------------------------------


//Checks for header stored in data[] array
void checkHeader() {
  if ( BOOL_READ(flags1, newGPSLine) ) {
    for (byte i = 1; i < 6; i++) {
      rc = readGPS();
      if (rc != dataGPS[i]) {
        BOOL_FALSE(flags1, newGPSLine);
      } else {
        //Serial.print(rc);
      }
    }
  }
}

//Read n=number of commas, next data char will be either a comma or a number
void commaSkip(int skippedCommas) {
  if ( BOOL_READ(flags1, newGPSLine) ) {
    for (byte i = 0; i < skippedCommas; i++) {
      rc = '0';
      while (rc != ',') {
        rc = readGPS();
      }
    }
    //Serial.println(" ");
  }
}
//Gets the corresponsing altitude only if header was found
void getAltitude() {
  if ( BOOL_READ(flags1, newGPSLine) && BOOL_READ(flags1, GPSPresent)) {
    //Serial.println(F(""));
    for (byte i = 0; i <= 5; i++) {
      rc = readGPS();
      altitudeA[i] = rc;
      //Serial.print(rc);
      if ((rc != '.') && (rc > '9' || rc < '0')) {
        BOOL_TRUE(flags2, noAltitude);
        BOOL_TRUE(flags1, gpsRoutineDone);
        return 0;
      }
    }
    //Serial.println(F(""));
    altitudeA[7] = '\0';
    altitude = (float)atof(altitudeA);

    //Serial.println(F("Alti "));
    //Serial.println(altitude);
    BOOL_TRUE(flags1, gpsRoutineDone);
  }
}

//Gets the corresponding latitude only if header was found
void getLatitude() {

  if ( BOOL_READ(flags1, newGPSLine) ) {
    //Serial.println(F(""));

    for (byte i = 0; i <= 9; i++) {
      rc = readGPS();
      latitudeA[i] = rc;
      //Serial.print(rc);
      if ((rc > '9' || rc < '0') && (rc != '.')) {

        BOOL_FALSE(flags1, GPSPresent);
        BOOL_FALSE(flags2, noGPS_Signal);
        return 0;
      }
    }
    //Serial.println("");
    latitudeA[11] = '\0';
    latitude = (float)atof(latitudeA);
    latitude = latitude / 100;
    //Determine sign for latitude

    commaSkip(1);

    rc = readGPS();
    if (rc == 'S') {
      latitude = latitude * (-1.0);
    }
    //Serial.print(F("Lat "));
    //Serial.println(latitude, 7);
  }
}


//Gets the corresponsing longitude only if header was found
void getLongitude() {
  if ( BOOL_READ(flags1, newGPSLine) && BOOL_READ(flags1, GPSPresent)) {
    //Serial.println(F(""));
    for (byte i = 0; i <= 10; i++) {
      rc = readGPS();
      longitudeA[i] = rc;
      //Serial.print(rc);
      if ((rc > '9' || rc < '0') && (rc != '.')) {
        BOOL_FALSE(flags1, GPSPresent);
        BOOL_FALSE(flags2, noGPS_Signal);
        return 0;
      }
    }

    //Serial.println(F(""));


    longitudeA[12] = '\0';
    longitude = (float)atof(longitudeA);
    longitude = longitude / 100;

    commaSkip(1);

    rc = readGPS();
    if (rc == 'W') {
      longitude = longitude * (-1.0);
    }

    //Serial.println(F("Long "));
    //Serial.println(longitude, 7);

  }
}

//Reads bits from GPS until new line is found. New lines all start with '$'
void newLineWait() {
  while ( BOOL_READ(flags1, newGPSLine) != 1) {
    rc = readGPS();
    //Serial.print(rc);
    if (rc == '$') {
      BOOL_TRUE(flags1, newGPSLine);
    }
  }
}

//-----------------------------------------------RTC Management--------------------------------------
/*
  Starts RTC
*/
void setRTC() {
  //RTCFail =1 in case we can't connect to it
  if (! rtc.begin()) {
    BOOL_TRUE(flags1, RTCFail);
  } else {
    BOOL_FALSE(flags1, RTCFail);
  }
}

/*
  Reads RTC and updates its state if this wasn't possible
*/
void readDS1307time() {
  DateTime now = rtc.now();

  //checks if RTC is not present
  if ((now.year() == 2165 || now.month() == 165 || now.year() == 165) && (now.unixtime() != 0) ) {
    BOOL_TRUE(flags1, RTCFail);
    //Serial.println(F("RTC dead"));
  } else {
    BOOL_FALSE(flags1, RTCFail);
    //Serial.println(now.unixtime());
  }

  //update todays reference time only if RTC's working and readtries has reset
  if ( (readTries == 0 && !(BOOL_READ(flags1, RTCFail))) || firstRun ) {
    referenceTime = now.unixtime();
  }

}


//---------------------------------------------------------------Arithmetic Methods---------------------------

//this methods recevie an array and a size, then they find the max or minx value in given array inside the first SIZE spaces
float findMaximum(int SIZE, float avgArray[]) {
  float highest = avgArray[0];
  for (int i = 0; i < SIZE; i++) {
    if (highest < avgArray[i]) {
      highest = avgArray[i];
    }
  }
  return highest;
}

float findMinimum(int SIZE, float avgArray[]) {
  float lowest = avgArray[0];
  for (int i = 0; i < SIZE; i++) {
    if (lowest > avgArray[i]) {
      lowest = avgArray[i];
    }
  }
  return lowest;
}

//this method recevies an array and a size, then it calculates the average value in given array inside the first SIZE spaces
float getAverage(int SIZE, float movingAvg[]) {

  float average = 0;
  for (byte i = 0; i < SIZE; i++) {
    average = average + movingAvg[i];
  }
  average = average / SIZE;
  return average;
}

//----------------------------------------------------------Iridium Management------------------------------

//blink lead while sending data with iridium
boolean ISBDCallback() {
  digitalWrite(LED_BUILTIN, (millis() / 1000) % 2 == 1 ? HIGH : LOW);
  return true;
}

/*
  Starts Iridium turns it on, waits a few seconds, builds frame and sends it
*/
void satelliteRoutine() {

  byte tries = 0; //time out fatal error variable for satellite comm
  //Turn Iridium on. Wait for start up, being serial comm, wait a bit and start reading.
  digitalWrite(IridiumControl, HIGH);

  digitalWrite(GPSControl, HIGH); //turn on Iridium

  //150 sec for start up time, 120 sec minimum
  if (!emergencyButton) {
    delay(60000);
    delay(60000);
    delay(30000);
  }


  nss.begin(19200);

  isbd.attachConsole(Serial);
  isbd.setPowerProfile(1);
  isbd.begin();
  frameRoutine();

  //reset variables for Iridium, turn off module
  BOOL_FALSE(flags2, success);

  isbd.sleep();
  nss.end();


  //150 sec for start up time, 120 sec minimum
  if (!emergencyButton) {
    digitalWrite(IridiumControl, LOW);
    digitalWrite(GPSControl, LOW); //turn off Iridium
  }
}

//Build frame depending on what Data was available/is requiered
void frameRoutine() {

  int signalQuality = -1; //default value for iridium modem minimum signal quality
  //possible to prealocate char array with malloc and keep track of current pos with counter, more efficient?

  //reserve() method makes an "array" of the string and instead of causing memory fragmentation, makes it so
  //the string is manipulated inside it. once the method ends space is freed
  String stringFrame;
  String temporal;


  //this pre-allocates a 270 char buffer in memory to work with.
  //no memory fragmentation will happen as String will be rewritten only inside the 270 char space
  if (!(stringFrame.reserve(270))) {
    //Serial.println(F("No space"));
    return;
  }

  stringFrame = F("10,Emergency,");
  byte tries = 0;

  Serial.print(F("GPS:"));
  temporal = String_GPS();
  stringFrame += temporal;
  //Serial.println(temporal);

  Serial.print(F("RTC:"));
  temporal = String_RTC();
  stringFrame += temporal;
  //Serial.println(temporal);

  temporal = String_CRC(stringFrame);
  stringFrame = String(F("&")) + stringFrame + temporal;
  char *chars = const_cast<char*>(stringFrame.c_str());

  Serial.println(F(""));
  Serial.println(chars);
  Serial.flush();

  int err;
  //tries to get signal quality then decides if it's a good idea to send data or try again
  while (BOOL_READ(flags2, success) == 0) {
    if (tries < SatelliteTries) {
      err = isbd.getSignalQuality(signalQuality);

      if (err != 0) {
        //Serial.print(F("Sig Quality fail: err "));
        //Serial.println(err);
        tries++;
      }

      //Serial.print(F("Quality "));
      //Serial.println(signalQuality);
      if (signalQuality < 1) {
        tries++;
        err = 11;
      } else {
        err = isbd.sendSBDText(chars);
      }
      if (err != 0) {
        //Serial.print(F("sendText err "));
        //Serial.println(err);
        tries++;
      } else {
        BOOL_TRUE(flags2, success);
      }
    } else {
      BOOL_TRUE(flags2, success);
      //Serial.println(F("Error with SDB module"));
    }
    delay(6000);
  }

  //Serial.println(F("Routine Done"));
}

/* Gets data if RTC is present
*/
String String_RTC() {

  if (referenceTime == 0) {
    BOOL_TRUE(flags1, RTCFail);
  }

  String RTC_String = F("");
  //timestamp
  if (!(BOOL_READ(flags1, RTCFail))) {
    //Serial.println(F("RTC present"));
    RTC_String += String(referenceTime, DEC);
    RTC_String += F(",");
  } else {
    RTC_String += F(",");
  }
  return RTC_String;
}


/* Gets data if GPS is present. Data will be hard coded server side this time
*/

String String_GPS() {
  String GPS_String = "";
  String temporal;

  if (BOOL_READ(flags2, noGPS_Signal)) {
    //Serial.println("GPS present");
    temporal = String(latitude, 6);
    GPS_String = temporal + ',';

    temporal = String(longitude, 6);
    GPS_String = GPS_String + temporal + ',';

    if (!BOOL_READ(flags2, noAltitude)) {
      temporal = String(altitudeA);
      GPS_String = GPS_String + temporal + ',';
    } else {
      GPS_String = GPS_String + ',';
    }

  } else {
    //Serial.println("GPS not present");
    GPS_String = ",,,";
  }
  return GPS_String;
}
/*
  Calculates CRC from String
*/
String String_CRC(String stringFrame) {
  String CRC_String = "";
  String crc;

  byte bytes[stringFrame.length() + 1];
  stringFrame.getBytes(bytes, stringFrame.length() + 1);
  byte crc8 = CRC8(bytes, stringFrame.length());


  if (String(crc8, HEX).length() == 1) {
    crc = F("0");
    crc += String(crc8, HEX);
  } else {
    crc = String(crc8, HEX);
  }

  CRC_String = F("*");
  CRC_String +=   crc;

  return CRC_String;
}

byte CRC8(const byte * data, byte len) {
  byte crc = 0x00;
  while (len--) {
    crc ^= *data++;
    for (byte i = 0; i < 8; i++) {
      if (crc & 0x80) crc = (crc << 1) ^ 0x07;
      else crc <<= 1;
    }
  }
  return crc;
}


//---------------------------------------------------------Button Polling----------------------------
/*
  void button_Polling() {
  //toggle state of device when button is pressed
  if (digitalRead(button) != normalButtonState) {
    delay(500); //debounce routine
    if (digitalRead(button) != normalButtonState) {

      Serial.println(digitalRead(button));
      emergencyButton = !emergencyButton;
      //Wait until button is let go. Program can freeze here.
      while (emergencyButton == digitalRead(button)) {

      }
    }
  }
  }*/


