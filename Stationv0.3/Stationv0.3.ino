/*
  PCINT2

  Pin D0 TX Serial Programming/debug
  Pin D1 RX Serial Programming/debug
  Pin D2 Anemometer Interrupts for wind direction (green wire)
  Pin D3 Anemometer Interrupts for wind speed (yellow wire)
  Pin D4 Enable 1
  Pin D5 Enable 2
  Pin D6 Enable 3
  Pin D7 SDI-12 Data Pin for soil sensors(blue wire)

  PCINT0

  Pin D8  Higrometer Input Capture Unit
  Pin D9  TX from GPS
  Pin D10 Thermometer Data
  Pin D11 RX Iridium Satellite
  Pin D12 TX Iridium Satellite
  Pin D13 

  PCINT1

  Pin A0 
  Pin A1 
  Pin A2 Possible Rain Interrupt
  Pin A3 Analog Readings (17)
  Pin A4 SDA from RTC
  Pin A5 SCL from RTC
  Pin A6 Analog only
  Pin A7 Analog only

  Switch States
  |A0 | A1 |
  | 0 |  0 | Default Working Mode
  | 0 |  1 | Dev/debug Mode (serial messages printed and reduced run time)
  | 1 |  0 | SDI Address Assign Mode Discovery
  | 1 |  1 |
*/

//------------------------------------Application Notes------------------
/*
  Transistor must be used for powering up GPS,SDI,Higrometer with a digital pin, since not enough current can be given by the pins
  Pin D13 is technically free most of the time. GPS cannot recieve data and Iridium is powered down with stepper
  even so, care must be taken when using said pin.

    Digital Pins 0-7  - Port D - PCINT2
    Digital Pins 8-13 - Port B - PCINT0
    Analog Pins A0-A5 - Port C - PCINT1

  SDI and software Serial both try to assign this pins. SDI is in pcint 2(pin 7), sw keeps everyother one
  modify .cpp by commenting out declaration as seen here depending on application:
  https://github.com/EnviroDIY/Arduino-SDI-12/issues/8
  Software Serial.cpp is in your computer at Arduino/hardware/arduino/avr/libraries/SoftwareSerial/src

  DO NOT USE ANY SOFTWARE SERIALS ON DIGITAL PINS 0-7!!!

  SDI12 library taken from here: https://github.com/EnviroDIY/Arduino-SDI-12
  Iridium library taken from here:https://github.com/mikalhart/IridiumSBD
  RTClibrary taken from here: https://github.com/adafruit/RTClib
  SimpleTimer library taken from here: https://github.com/jfturcot/SimpleTimer
*/
//------------------------------------Definitions------------------------
#define DEVMOD

#define DS1307_I2C_ADDRESS 0x68

#define normal    0 //00000000 means we are in normal operating mode
#define discovery 1 //000000001 means we are in search mode for the SDI sensors
#define satOut 4 //0000001xx means we want to use iridium module too 


#define vaneInterrupt 0
#define anemometerInterrupt 1

//Pins 4 and 5 for transistor switching, 13 is debug Led
#define vanePin         2
#define anemometerPin   3
#define IridiumControl  4
#define GPSControl      5
#define DATAPIN         7
#define Thermos         10
#define LED             13
#define Switch3         13
#define Switch1         14
#define Switch2         15
#define Button          16
#define ADCPin          17

#define thermoError 10 //thermometer has a problem
#define measurementsNumber 1 //times I will take measurements before sending data
#define GPSTries 2 //do a few GPS read tries to try to obtain data

#define APISize 32

#ifdef DEVMOD
#define SatelliteTries 2
#else
#define SatelliteTries 30
#endif

//bit position of each flag
#define send_Data           1
#define thermoBounce        2
#define newGPSLine          4
#define gpsRoutineDone      8
#define RTCFail             16
#define GPSPresent          32
#define ThermoPresent       64
#define min30               128
#define noGPS_Signal        1
#define success             2
#define noAltitude          4
#define found               8
#define first               16
#define triggered           32
#define interruptAnemometer 1
#define interruptVane       2
#define cycle1              4
#define cycle2              8
#define seconds5            16
#define anemometerP         32
#define windGust            64
#define debugFlag           128


//-------------------------------------Macros------------------------------
//Set to 1,0 or read bit at "pos" of byte "by" respectively
#define BOOL_TRUE(by,pos)  by|=pos
#define BOOL_FALSE(by,pos) by&= ~(pos)
#define BOOL_READ(by,pos)  bool(by&pos)

//-------------------------------------Libraries---------------------------
#include "IridiumSBD.h"
#include <OneWire.h>
#include "SoftwareSerial.h"
#include "SimpleTimer.h"
#include "RTClib.h"
#include "avr/power.h"
#include "SDI12.h"

//----------------------------------Constants & Global variables----------

byte flags1 = 0;
byte flags2 = 0;
byte flags3 = 0;

//addressRegister for SDI-12
byte addressRegister[8] = {
  0B00000000,
  0B00000000,
  0B00000000,
  0B00000000,
  0B00000000,
  0B00000000,
  0B00000000,
  0B00000000
};



byte addrDirectory[3][8];   //maximum amount of thermometers permitted
char apiKeyThermos[3][8];   //arrays for assigned Apikeys to thermos
byte thermosFound = 0;      //counter for total amount of thermometers found


short takeReading = 0; //value in seconds between each data acquisition

SimpleTimer timerSeconds; // timer for seconds declared with SimpleTimer Library

byte readTries = 0; //Count how many times I've tried to read data
byte thermoCounter = 0; //times thermometer wasn't found
byte second5Count = 0;
byte second60Count = 0;
byte pos10Array = 0; //Position in 10 wind average I'm currently at

volatile unsigned long overflowCount;  //variables used in interrupts for the capture unit to count time
volatile unsigned long startTime;
volatile unsigned long finishTime;


//variables changed within interrupts for the vane. anemometerDifference
//returned to 0 when no interrupts have happened in the last 5 seconds

volatile unsigned long previousAnemometer = 0;
volatile unsigned long newAnemometer = 0;
volatile unsigned long anemometerDifference = 0;

volatile unsigned long newVane = 0;
volatile unsigned long vaneDifference = 0;

unsigned long seconds = 0; // seconds counter
unsigned long referenceTime = 0; //will reset on year 2038

float vanePos   = 0.0;   //last angle° from reference vane has

float movingAvg[12];  //avg of wind Speeds of last 60 seconds, each value represents last wind speed of last 5 seconds
float movingMax[10];  //maximum wind Speeds of the last 10 minutes, each position is the max wind speed of a whole minute
float movingMin[10];  //minimum wind Speeds of the last 10 minutes, each position is the min wind speed of a whole minute

float movingAvg2min = 0;  //avg of moving avg of the last 2 min
float avgLastMinute = 0;  //to save last minute avg
float avgThisMinute = 0;  //to save current last minute avg

float latitude  = 0.0;  //gps data
float longitude = 0.0;
float altitude  = 0.0;

float mm1 = 0; // inches of first minute converted to millimeters by the rain gauge
float mm2 = 0; // inches of second minute to give average of 2 minutes

//arrays to convert gps data to chars
char longitudeA[12];
char latitudeA[11];
char altitudeA[7];
//receive buffer for serial comms
char rc;

//---------------------------------Virtual Serial----------------------------------

OneWire  ds(Thermos);  // Sensor on DIGITAL pin 10 (4.7K resistor necessary between pin 10 and 3.3V from source)
SoftwareSerial nss(11, 12);  //Iridium object created
SoftwareSerial GPS_Serial(9, 13); //TX from GPS to Arduino Pin 9
IridiumSBD isbd(nss, 13); //to sleep iridium, not really used
RTC_DS1307 rtc;  //instance rtc object
SDI12 mySDI12(DATAPIN);  //instance SDI12 object


//Rain Averages
float rainAvg;

//Thermometer management variables
float celsius[3];

//ID count of soil sensor
char SDIcount = '1';

//Array to store data of every soil sensor
struct SDI_info {
  char apikey[8];
  char id;
};
//save space for three sensors
struct SDI_info SDI_array[3];

//---------------------------------------------------------Interrupts---------------------------
//---------------------------------------Capture Unit-------------------------------------------
// timer overflows (every 65536 counts)
ISR(TIMER1_OVF_vect) {
  overflowCount++;
}
//in case a change in the pin is detected
ISR(TIMER1_CAPT_vect) {
  //save counter inmediately
  unsigned int timer1CounterValue = ICR1;
  unsigned int overflowCopy = overflowCount;

  // if just missed an overflow
  if ((TIFR1 & bit(TOV1)) && timer1CounterValue < 0x7FFF) {
    overflowCopy++;
  }

  // wait until we notice last one
  if (BOOL_READ(flags2, triggered)) {
    return;
  }

  if (BOOL_READ(flags2, first)) {
    startTime = (overflowCopy << 16) + timer1CounterValue;
    BOOL_FALSE(flags2, first);
    return;
  }

  finishTime = (overflowCopy << 16) + timer1CounterValue;
  BOOL_TRUE(flags2, triggered);
  TIMSK1 = 0;    // no more interrupts for now
}

//---------------------------------------External Interrupts-------------------------------------------
//Anemometer Interrupt Routine
void anemometer() {
  //if we choose anemometer routines over pluviometers, anemometerP=1
  if (BOOL_READ(flags3, anemometerP)) {
    //save time value and detach interrupt to avoid multiple firing of routine
    detachInterrupt(digitalPinToInterrupt(anemometerPin));
    newAnemometer = millis();

    //incase of overflow skip reading
    if (newAnemometer - previousAnemometer > newAnemometer + previousAnemometer) {
      previousAnemometer = newAnemometer;
      attachInterrupt(digitalPinToInterrupt(anemometerPin), anemometer, RISING);
      return;
    }
    //if we have 2 anemometers interrupts between one vane interrupt
    if ((newVane >= previousAnemometer) && (newVane <= newAnemometer)) {
      BOOL_TRUE(flags3, cycle1); //prepare cycle to calculate needed data
      anemometerDifference = newAnemometer - previousAnemometer; //save values before updating data
      vaneDifference = (newAnemometer - previousAnemometer) - (newAnemometer - newVane);
    }
    previousAnemometer = newAnemometer; //update data for next interrupt and re-arm it
    attachInterrupt(digitalPinToInterrupt(anemometerPin), anemometer, RISING);

  } else {
    //in case we are using pluviometer update rain total bit, anemometerP=0
    mm1 = (mm1 + 0.01) * 25.4;
  }
}


//Vane Interrupt Routine, save time data, un-arm and re-arm interrupt to avoid multiple firings
void vane() {
  detachInterrupt(digitalPinToInterrupt(vanePin));
  newVane = millis();
  attachInterrupt(digitalPinToInterrupt(vanePin), vane, RISING);
}



//-------------------------------------Setup---------------------------------
//this code runs once
void setup() {
  //manually choose anemometer for now
  BOOL_TRUE(flags3, anemometerP);

  //--------------------------Serial Debug-----------------------------------

  //sets serial comms
  setSerial();
  //initialize sensors and environment
  initialize();
  //get state of dip switch to choose mode
  //if this mode is accessed we will stay here until switches are changed
  //necessary to assign APIKeys to every SDI
  if (getState() == byte(discovery)) {
    addressAssign();
  }
  //--------------------------RTC Module-------------------------------------
  setRTC();
  //--------------------------First Run--------------------------------------
  //Gets reference time
  readDS1307time();

  discoverOneWireDevices();
  //Search for sdi12 devices on bus, even if we already assigned apikeys to each
  discoverSDI12();
  //enable external interrupts on respective Pins
  attachInterrupt(digitalPinToInterrupt(anemometerPin), anemometer, RISING);
  attachInterrupt(digitalPinToInterrupt(vanePin), vane, RISING);
}

//-----------------------------------------Loop------------------------------
//this code is the analog of a void main() with a while(1)
void loop() {
  //this method updates everytime a second passes
  timerSeconds.run();
  //----------------Data Read----------------------------------------

  windRoutine();
  readData();

  //----------------Data Send----------------------------------------
  sendData();
}


void initialize() {

  Serial.println(F("Initializing"));
  //define pins as outputs and inputs
  pinMode(LED, OUTPUT);
  pinMode(IridiumControl, OUTPUT);
  pinMode(GPSControl, OUTPUT);

  pinMode(Switch3, INPUT);
  pinMode(Switch1, INPUT);
  pinMode(Switch2, INPUT);
  pinMode(Button, INPUT);

  pinMode(anemometerPin, INPUT_PULLUP);
  pinMode(vanePin, INPUT_PULLUP);
  //define voltage reference for analog inputs. default=5v internal in nano
  analogReference(DEFAULT);

  //start SDI bus and wait a little bit
  mySDI12.begin();
  delay(500);

  //turn Iridium supply off, not connected right now
  digitalWrite(IridiumControl, LOW);
  digitalWrite(GPSControl, LOW);

  //library is in ms, will enter "secondsCount" every second approx
  timerSeconds.setInterval(1000, secondsCount);
  //flags for system
  BOOL_TRUE(flags1, send_Data);
  BOOL_FALSE(flags1, thermoBounce);
  BOOL_TRUE(flags1, newGPSLine);
  BOOL_FALSE(flags1, gpsRoutineDone);
  BOOL_FALSE(flags1, RTCFail);
  BOOL_TRUE(flags1, GPSPresent);
  BOOL_TRUE(flags1, ThermoPresent);
  BOOL_TRUE(flags1, min30);
  BOOL_FALSE(flags2, noGPS_Signal);
  BOOL_FALSE(flags2, success);
  BOOL_FALSE(flags2, noAltitude);
  BOOL_FALSE(flags3, interruptAnemometer);
  BOOL_FALSE(flags3, interruptVane);
  BOOL_FALSE(flags3, cycle1);
  BOOL_TRUE(flags3, debugFlag);

  interrupts();
  Serial.println(F("Initialisation Complete"));
}
//-------------------------------Data Read---------------------------
//reads thermometer every hour
void readData() {

  //min30 is a flag to define data has to be read, time between data acquisition can be changed in secondsCount()
  if (/*BOOL_READ(flags1, min30)*/ BOOL_READ(flags3, debugFlag)) {

    BOOL_FALSE(flags1, min30);
    BOOL_FALSE(flags3, debugFlag);
    //power up internal modules of arduino
    powerUpRoutine();
    //get time if possible from RTC
    readDS1307time();

    for (int i = 0; i < thermosFound; i++) {
      //Try to read thermometer 10 times
      BOOL_FALSE(flags1, thermoBounce);

      while (!(BOOL_READ(flags1, thermoBounce))) {
        //If temperature is read, thermoBounce=1
        registerTemperature(i);

        if (thermoCounter == thermoError) {
          //if all 10 reads fail, send a warning
          BOOL_TRUE(flags1, thermoBounce);
          BOOL_FALSE(flags1, ThermoPresent);
        }
      }
      thermoCounter = 0;
    }
    /*augment times we have gotten data. Increase measurmentsNumber if you want more
      to read more times before sending data.
    */
    readTries++;
    if (readTries >= measurementsNumber) {
      BOOL_TRUE(flags1, send_Data);
    } else {
      //Power down internal modules to save power
      lowPowerRoutine();
    }

  }
}

//------------------------------------Data Send--------------------------
void sendData() {

  if (BOOL_READ(flags1, send_Data)) {


    BOOL_FALSE(flags1, send_Data);

    //get data from GPS
    gpsRoutine();
    //get data from Higrometer
    higrometerRoutine();

    //try to send Data. If no satellite is connected data will be sent via serial comms
    satelliteRoutine();

    //reset amount of times I've read sucessfully
    readTries = 0;
    lowPowerRoutine();
    //reset period time counter
    seconds = 0;
  }
}

//--------------------------------------------Switch Case Controlling--------------------------------

//reads current state of dipswitch and returns result as a byte.
byte getState() {
  byte state = 0;

  if (digitalRead(Switch2)) {
    BOOL_TRUE(state, 2);
  } else {
    BOOL_FALSE(state, 2);
  }

  if (digitalRead(Switch1)) {
    BOOL_TRUE(state, 1);
  } else {
    BOOL_FALSE(state, 1);
  }

  if (digitalRead(Switch3)) {
    BOOL_TRUE(state, 3);
  } else {
    BOOL_FALSE(state, 3);
  }



  switch (state) {
    case 0:
      //normal working mode
      Serial.println(F("Normal"));
      break;
    case 1:
      //Discovery SDI-12 mode
      Serial.println(F("Discovery"));
      break;
    case 2:
      //Debug mode
      Serial.println(F("Debug"));
      break;

    case 4:
      Serial.println(F("Satellite"));
      break;

    default:
      Serial.println(F("Other"));
      break;
  }
  return state;
}

//-----------------------------------------------Extra Timer------------------------------

/*
   Timer for time period, executed every second
*/
void secondsCount() {
  //flash led
  digitalWrite(LED, !digitalRead(LED));
  //seconds counts days, takeReading data acquisition period, 5 count and 60 count are for windVane
  seconds++;
  takeReading++;
  second5Count++;
  second60Count++;

  if (second5Count >= 5) {
    //flag to register last read speed and vanePos
    BOOL_TRUE(flags3, seconds5);
    second5Count = 0;
  }
  //reading every 60 seconds
  if (takeReading >= 60) {
    //Serial.println(F("1 min passed"));
    BOOL_TRUE(flags1, min30);
    BOOL_TRUE(flags3, debugFlag);
    takeReading = 0;
  }
  if (seconds >= 86304) {
    /*If RTC died, count extra days every 86400 seconds
      value was experimentally compensated to 86304 to account
      for drift due to temperature in device*/
    if (BOOL_READ(flags1, RTCFail)) {
      Serial.println(F("RTC dead"));
      referenceTime = referenceTime + seconds;
      BOOL_TRUE(flags1, min30);
      BOOL_TRUE(flags3, debugFlag);
    }
    seconds = 0;
  }
}

//---------------------------------------------------------Low Power------------------------------
void lowPowerRoutine() {
  /*
    ADCSRA = 0;
    //turn off unused modules until time to read comes
    power_adc_disable(); // ADC converter
    power_spi_disable(); // SPI
    power_usart0_disable();// Serial (USART)
    //power_timer0_disable();// Timer 0 not turned off to keep millis() running
    power_timer1_disable();// Timer 1
    power_timer2_disable();// Timer 2
    power_twi_disable(); // TWI (I2C)
  */
}

void powerUpRoutine() {
  /*power_adc_enable();
    power_spi_enable(); // SPI
    power_usart0_enable(); // Serial (USART)
    //power_timer0_enable(); // Timer 0 never turned off, cause millis() resets
    power_timer1_enable(); // Timer 1
    power_timer2_enable(); // Timer 2
    power_twi_enable(); // TWI (I2C)
    delay(10000);
  */
}




//-------------------------Serial Comms-----------------------------------------------

void setSerial() {
  Serial.begin(115200);
  while (!Serial) {
    //wait for port to open
  }
}


//Reads data untill enter is pressed, no more than 32 chars can be stored
char *getCharArray(int arraySize, int *index) {
  *index = 0;
  char inChar; // Where to store the character read
  char *inData = malloc(arraySize * sizeof(char)); // Allocate some space for the string of size arraySize

  if (!inData) {
    return NULL;
  }
  //read one byte/char to clear buffer from noise or leftovers
  inChar = Serial.read();
  inChar = F("0");

  while (!((inChar == '\n') || (inChar == '\r'))) {
    while (Serial.available() > 0) {
      inChar = Serial.read(); // Read a character
      if (inChar == '\n' || inChar == '\r' ) {
        //if enter was read insert EOF
        inData[*index] = F("\0");
        break;
      } else {
        //in case more than 32 bytes try to be stored only overwrite last received
        if (*index < 31) {
          inData[*index] = inChar; // Store it
          Serial.print(inData[*index]);
          Serial.print(F(" "));
          Serial.println(*index);
          *index = *index + 1; // Increment where to write next
        }
      }
    }
    //small delay before reading next char
    delay(2);
  }
  return inData;
}

//Serial.flush clears output buffer
//Input buffer can have 64 bytes, buffer is cleared by reading all bytes.
//DON'T EVER CALL THIS METHOD IF YOU KNOW DATA IS BEING RECEIVED
void clearSerialBuffer() {
  Serial.flush();
  for (int j = 0; j < 65; j++) {
    Serial.read();
  }
}
//----------------------------------------------------------Temperature Management-------------------------------

void discoverOneWireDevices() {
  bool check = false;
  byte i;
  byte present = 0;
  byte data[12];
  byte addr[8];
  int index = 0;
  int arraySize = int(APISize) - 1;
  char *readData;

  while (ds.search(addr)) {
    //Serial.print(F("\n\rFound \'1-Wire\' with address:\n\r"));
    for ( i = 0; i < 8; i++) {
      //Serial.print("0x");
      if (addr[i] < 16) {
        //Serial.print('0');
      }
      addrDirectory[thermosFound][i] = addr[i];
      //Serial.print(addr[i], HEX);
      if (i < 7) {
        //Serial.print(", ");
      }
    }
    if ( OneWire::crc8( addr, 7) != addr[7]) {
      //Serial.print(F("CRC is not valid!\n"));
      return;
    } else {


      Serial.println(F("Enter 8 alphanumeric chars then press enter"));
      while (!check) {
        readData = getCharArray(arraySize, &index);
        if (readData != NULL) {
          Serial.print(F("Entered: "));
          for (int i = 0; i <= index; i++) {
            Serial.print(readData[i]);
          }
          Serial.println(F(""));
          check = checkAPIKey(readData, index, 8);
          index = 0;
        }
      }
      clearSerialBuffer();
      check = false;

      for (int i = 0; i < 8; i++) {
        apiKeyThermos[thermosFound][i] = readData[i];
      }
      Serial.print(F("Device id: "));
      for (int i = 0; i < 8; i++) {
        Serial.print(addrDirectory[thermosFound][i], HEX);
      }

      Serial.print(F(" API key:"));
      for (int i = 0; i < 8; i++) {
        Serial.print(apiKeyThermos[thermosFound][i]);
      }
      Serial.println(F(""));
      thermosFound++;
    }
  }
  Serial.println(F(""));
  ds.reset_search();
  return;
}


void registerTemperature(int thermNumber) {
  byte i;
  byte type_s = 0; //to check for thermometer type
  byte present = 0; //
  byte dataTemperature[12]; //temperature data
  byte addr[8];
  for (int i = 0; i < 8; i++) {
    addr[i] = addrDirectory[thermNumber][i];
  }
  /*
    if (!ds.search(addr)) {
      ds.reset_search();
      delay(220);
      thermoCounter++;
      return;
    }

    //Check if CRC is valid
    if (OneWire::crc8(addr, 7) != addr[7]) {
      thermoCounter++;
      return;
    }*/
  //type_s=0 for DS18B20
  thermoCounter = 0;
  BOOL_TRUE(flags1, thermoBounce);

  ds.reset();
  ds.select(addr);
  ds.write(0x44, 1);        // start conversion, with parasite power on

  delay(753);     // A little more than 750 ms here

  present = ds.reset();
  ds.select(addr);
  ds.write(0xBE);         // Read Scratchpad

  /*Serial.print("  Data = ");
    Serial.print(present, HEX);
  */  Serial.print(" ");
  for ( i = 0; i < 9; i++) {           // 9 bytes
    dataTemperature[i] = ds.read();
  }
  /* Serial.print(" CRC=");
    Serial.print(OneWire::crc8(data, 8), HEX);
    Serial.println();*/
  // Convert the data to actual temperature
  // because the result is a 16 bit signed integer, it should
  // be stored to an "int16_t" type, which is always 16 bits
  // even when compiled on a 32 bit processor.

  int16_t raw = (dataTemperature[1] << 8) | dataTemperature[0];
  if (type_s) {
    raw = raw << 3; // 9 bit resolution default
    if (dataTemperature[7] == 0x10) {
      // "count remain" gives full 12 bit resolution
      raw = (raw & 0xFFF0) + 12 - dataTemperature[6];
    }
  } else {
    byte cfg = (dataTemperature[4] & 0x60);
    // at lower res, the low bits are undefined, so let's zero them
    if (cfg == 0x00) raw = raw & ~7;  // 9 bit resolution, 93.75 ms
    else if (cfg == 0x20) raw = raw & ~3; // 10 bit res, 187.5 ms
    else if (cfg == 0x40) raw = raw & ~1; // 11 bit res, 375 ms
    //// default is 12 bit resolution, 750 ms conversion time
  }
  BOOL_TRUE(flags1, ThermoPresent);
  celsius[thermNumber] = (float)raw / 16.0;

}



//---------------------------------------------------------------Arithmetic Methods---------------------------
//this methods recevie an array and a size, then they find the max or minx value in given array inside the first SIZE spaces
float findMaximum(int SIZE, float avgArray[]) {
  float highest = avgArray[0];
  for (int i = 0; i < SIZE; i++) {
    if (highest < avgArray[i]) {
      highest = avgArray[i];
    }
  }
  return highest;
}

float findMinimum(int SIZE, float avgArray[]) {
  float lowest = avgArray[0];
  for (int i = 0; i < SIZE; i++) {
    if (lowest > avgArray[i]) {
      lowest = avgArray[i];
    }
  }
  return lowest;
}
/*void getMaximum() {
  maxPos = 0;
  float maximum = temperatures[0].measurement;
  for (byte i = 1; i < contT; i++) {
    if (maximum < temperatures[i].measurement) {
      maxPos = i;
      maximum = temperatures[i].measurement;
    }
  }
  }

  void getMinimum() {
    minPos = 0;
  float minimum = temperatures[0].measurement;
  for (byte i = 1; i < contT; i++) {
    if (minimum > temperatures[i].measurement) {
      minPos = i;
      minimum = temperatures[i].measurement;
    }
  }
  }*/

//this method recevies an array and a size, then it calculates the average value in given array inside the first SIZE spaces
float getAverage(int SIZE, float movingAvg[]) {

  float average = 0;
  for (byte i = 0; i < SIZE; i++) {

    /*Serial.print(F("In array:"));
      Serial.println(movingAvg[i]);
      Serial.flush();*/
    average = average + movingAvg[i];

  }
  average = average / SIZE;
  return average;
}

//changes chars to decimals for SDI ids
byte charToDec(char i) {
  if ((i >= '0') && (i <= '9')) return i - '0';
  if ((i >= 'a') && (i <= 'z')) return i - 'a' + 10;
  if ((i >= 'A') && (i <= 'Z')) return i - 'A' + 37;
  else return i;
}

//shift wind Array to the left, then stores lastest reading
void shiftArray(float windSpeed) {
  memmove(&movingAvg[0], &movingAvg[1], sizeof(movingAvg) - sizeof(*movingAvg));
  movingAvg[11] = windSpeed;
}




//----------------------------------------------------------Iridium Management------------------------------

//blink lead while sending data with iridium
boolean ISBDCallback() {
  digitalWrite(LED_BUILTIN, (millis() / 1000) % 2 == 1 ? HIGH : LOW);
  return true;
}

void satelliteRoutine() {

  byte tries = 0; //time out fatal error variable for satellite comm
  //Turn Iridium on. Wait for start up, being serial comm, wait a bit and start reading.
  digitalWrite(IridiumControl, HIGH);
  //150 sec for start up time, 120 sec minimum

  /*    delay(60000);
      delay(60000);
      delay(30000);
  */
  /* nss.begin(19200);

    //isbd.attachConsole(Serial);
    isbd.setPowerProfile(1);
    isbd.adjustSendReceiveTimeout(50);
    isbd.begin();
  */
  frameRoutine();
  //reset variables for Iridium, turn off module
  BOOL_FALSE(flags2, success);

  /*
    isbd.sleep();
    digitalWrite(IridiumControl, LOW);
  */
  Serial.flush();
}

//Build frame depending on what Data was available/is requiered
void frameRoutine() {

  int signalQuality = -1; //default value for iridium modem minimum signal quality
  //possible to prealocate char array with malloc and keep track of current pos with counter, more efficient?

  //reserve() method makes an "array" of the string and instead of causing memory fragmentation, makes it so
  //the string is manipulated inside it. once the method ends space is freed
  String stringFrame;
  String temporal;


  //this pre-allocates a 270 char buffer in memory to work with.
  if (!(stringFrame.reserve(270))) {
    Serial.println(F("Not enough space"));
    return;
  }

  byte tries = 0;
  //Serial.print(F("RTC:"));
  temporal = String_RTC();
  stringFrame += temporal;
  //Serial.println(temporal);

  //Serial.print(F(" GPS:"));
  temporal = String_GPS();
  stringFrame += temporal;
  //Serial.println(temporal);

  //Serial.print(F(" Termo:"));
  temporal = String_Thermo();
  stringFrame += temporal;
  //Serial.println(temporal);

  //Serial.print(F(" SDI:"));
  temporal = String_SDI12();
  stringFrame += temporal;
  //Serial.println(temporal);

  //Serial.print(F(" Wind Vane:"));
  temporal = String_WindVane();
  stringFrame += temporal;
  //Serial.println(temporal);

  //Serial.print(" Rain:");
  //temporal = String_Rain();
  //stringFrame += temporal;
  //Serial.println(temporal);

  temporal = String_Higrometer();
  stringFrame += temporal;

  temporal = String_CRC(stringFrame);
  stringFrame = String(F("#GX8K2F3T,")) + stringFrame + temporal;
  char *chars = const_cast<char*>(stringFrame.c_str());

  Serial.println(F(""));
  Serial.println(chars);
  Serial.flush();

  if (getState() == satOut) {
    int err;

    while (BOOL_READ(flags2, success) == 0) {
      if (tries < SatelliteTries) {
        err = isbd.getSignalQuality(signalQuality);

        if (err != 0) {
          Serial.print(F("Sig Quality fail: err "));
          Serial.println(err);
          tries++;
        }

        Serial.print(F("Quality "));
        Serial.println(signalQuality);
        if (signalQuality < 1) {
          tries++;
          err = 11;
        } else {
          err = isbd.sendSBDText(chars);
        }
        if (err != 0) {
          Serial.print(F("sendText err "));
          Serial.println(err);
          tries++;
        } else {
          BOOL_TRUE(flags2, success);
        }
      } else {
        BOOL_TRUE(flags2, success);
        Serial.println(F("Error with SDB module"));
      }
      delay(6000);
    }
  }
  //Serial.println(F("Frame Routine Done"));
}


String String_RTC() {

  if (referenceTime == 0) {
    BOOL_TRUE(flags1, RTCFail);
  }

  String RTC_String = "";
  //timestamp
  if (!(BOOL_READ(flags1, RTCFail))) {
    //Serial.println(F("RTC present"));
    RTC_String = F("5.0:");
    RTC_String += String(referenceTime, DEC);
    RTC_String += F(",");
  }
  return RTC_String;
}

String String_GPS() {

  String GPS_String = "";

  if (BOOL_READ(flags2, noGPS_Signal)) {
    Serial.println(F("GPS present"));
    GPS_String += F("3.0:");
    GPS_String += String(longitude, 7) + ",";

    GPS_String += F("1:");
    GPS_String += String(latitude, 7) + ",";

    if (!BOOL_READ(flags2, noAltitude)) {
      GPS_String += F("2:");
      GPS_String += String(altitude, 2) + ",";
    }
  }
  //timestamp
  return GPS_String;
}


String String_SDI12() {

  String soil = "";
  //taken used to keep count of where we are on the SDI array
  //everytime we find one we increase by one
  int taken = 0;
  // scan address space 0-9
  for (char i = '0'; i <= '9'; i++) if (isTaken(i)) {
      soil += takeMeasurement(i) + F("@");
      soil += SDI_array[taken].apikey;
      soil += F(",");
      taken++;
    }

  // scan address space a-z
  for (char i = 'a'; i <= 'z'; i++) if (isTaken(i)) {
      soil += takeMeasurement(i) + F("@");
      soil += SDI_array[taken].apikey;
      soil += F(",");
      taken++;
    }

  // scan address space A-Z
  for (char i = 'A'; i <= 'Z'; i++) if (isTaken(i)) {
      soil += takeMeasurement(i) + F("@");
      soil += SDI_array[taken].apikey;
      soil += F(",");
      taken++;
    };

  return soil;
}

String String_Thermo() {

  String thermo_String = "";
  String temporal = F("@");
  for (int j = 0; j < thermosFound; j++) {
    for (int i = 0; i < 8; i++) {
      temporal += apiKeyThermos[j][i];
    }

    for (int i = 0; i < thermosFound; i++) {
      if (BOOL_READ(flags1, ThermoPresent)) {
        thermo_String += F("1.0:");
        thermo_String +=  String(float(celsius[i]));
      }
      thermo_String += temporal + F(",");
    }
  }
  return thermo_String;
}

String String_WindVane() {

  String meditions = "";

  if (BOOL_READ(flags3, anemometerP)) {

    if ((vanePos > 0 || vanePos < 360)) {
      meditions = F("4.0:");

      meditions += String(vanePos, 2) + ",";
    }

    meditions += F("2:");
    //1mph=0.447 m/s
    meditions += String((movingAvg2min * 0.447), 2);

    meditions += ",";

    if (BOOL_READ(flags3, windGust)) {
      meditions += F("3:Gust,");
    }

  }
  return meditions;
}


String String_Rain() {

  String rain = "";

  if (!(BOOL_READ(flags3, anemometerP))) {
    rain = F("6.0:");

    float average = (mm2 + mm1) / 2;

    mm2 = mm1;
    mm1 = 0;

    rain += String(average, 2);
    rain += F(",");
  }
  return rain;
}

String String_Higrometer() {

  // period is elapsed time
  unsigned long elapsedTime = finishTime - startTime;
  // frequency is inverse of period, adjusted for clock period

  //float freq = F_CPU / float (elapsedTime);  // each tick is 62.5 ns at 16 MHz

  float freq = -553.21 + (elapsedTime * 0.49);

  String interval = F("");

  int ADCValue = analogRead(ADCPin);
  float temp = calculateTemp(ADCValue);

  if (temp > 0 || temp < 100) {
    interval += F("2.0:");
    interval += String(temp, 2);
    interval += "";
  }

  if (elapsedTime != 0) {
    // each tick is 62.5 ns at 16 MHz
    if (freq >= 0 && freq < 101 ) {
      interval += F(",1:");
      interval += String(freq);
    }
  } else {
    interval += F("");
  }
  return interval;
}

String String_CRC(String stringFrame) {
  String CRC_String = "";
  String crc;

  byte bytes[stringFrame.length() + 1];
  stringFrame.getBytes(bytes, stringFrame.length() + 1);
  byte crc8 = CRC8(bytes, stringFrame.length());


  if (String(crc8, HEX).length() == 1) {
    crc = "0" + String(crc8, HEX);
  } else {
    crc = String(crc8, HEX);
  }

  CRC_String = "*" + crc ;

  return CRC_String;
}

byte CRC8(const byte *data, byte len) {
  byte crc = 0x00;
  while (len--) {
    crc ^= *data++;
    for (byte i = 0; i < 8; i++) {
      if (crc & 0x80) crc = (crc << 1) ^ 0x07;
      else crc <<= 1;
    }
  }
  return crc;
}


//-----------------------------------------------RTC Management--------------------------------------
void setRTC() {
  //RTCFail =1 in case we can't connect to it
  if (! rtc.begin()) {
    BOOL_TRUE(flags1, RTCFail);
  } else {
    BOOL_FALSE(flags1, RTCFail);
  }
}

void readDS1307time() {
  DateTime now = rtc.now();

  //checks if RTC is not present
  if ((now.year() == 2165 || now.month() == 165 || now.year() == 165) && (now.unixtime() != 0) ) {
    BOOL_TRUE(flags1, RTCFail);
    Serial.println(F("RTC dead"));
  } else {
    BOOL_FALSE(flags1, RTCFail);
  }
  //update todays reference time only if RTC's working and readtries has reset
  if (readTries == 0 && !(BOOL_READ(flags1, RTCFail)) && now.unixtime() != 0 ) {
    referenceTime = now.unixtime();
  }
}
/*
  void displayTime() {

  // retrieve data from D1307
  readDS1307time();
  DateTime now = rtc.now();
  Serial.print(now.year(), DEC);
  Serial.print('/');
  Serial.print(now.month(), DEC);
  Serial.print('/');
  Serial.print(now.day(), DEC);
  Serial.print(" (");
  Serial.print(now.hour(), DEC);
  Serial.print(':');
  Serial.print(now.minute(), DEC);
  Serial.print(':');
  Serial.print(now.second(), DEC);
  Serial.println();
  Serial.print("Timestamp");
  Serial.print(now.unixtime());
  Serial.print("s = ");
  }
*/
//------------------------------------------SDI-12--------------------------------------

/*Recieves the address of the sensor, reads its data, and stores it on a string
  @param {char} i*/
String printBufferToScreen(char i) {
  String soilSensorMeditions;
  int addressCount = 0;
  String buffer = F("7.");
  buffer += String(addressCount, DEC);
  buffer += F(":");
  addressCount++;
  mySDI12.read(); // consume address
  mySDI12.read(); // consume addres
  while (mySDI12.available()) {
    char c = mySDI12.read();
    if (c == '+' || c == '-') {
      buffer += ',';
      buffer += String(addressCount, DEC) + ":";
      addressCount++;
      if (c == '-') buffer += '-';
    }
    else if ((c != '\n') && (c != '\r')) {
      buffer += c;
    }

    //Serial.print(c);
    delay(50);
  }
  //Serial.println(F(""));
  //Serial.flush();
  soilSensorMeditions = buffer;

  //Serial.println(soilSensorMeditions);
  return soilSensorMeditions;
}
//cheks if any sdi12 device is present and sets array
void discoverSDI12() {
  for (byte i = '0'; i <= '9'; i++) if (checkActive(i)) setTaken(i); // scan address space 0-9
  for (byte i = 'a'; i <= 'z'; i++) if (checkActive(i)) setTaken(i); // scan address space a-z
  for (byte i = 'A'; i <= 'Z'; i++) if (checkActive(i)) setTaken(i); // scan address space A-Z
}


/*Recieves the address of the sensor, and sends the commands to read the data
  @param {char} i
*/
String takeMeasurement(char i) {
  String command = "";
  command += i;
  command += F("M!"); // SDI-12 measurement command format  [address]['M'][!]
  mySDI12.sendCommand(command);
  // wait for acknowlegement with format [address][ttt (3 char, seconds)][number of measurments available, 0-9]
  String sdiResponse = "";
  delay(30);
  while (mySDI12.available())  // build response string
  {
    char c = mySDI12.read();

    if ((c != '\n') && (c != '\r'))
    {
      sdiResponse += c;
      delay(5);
    }
  }
  mySDI12.clearBuffer();


  // find out how long we have to wait (in seconds).
  unsigned int wait = 0;
  wait = sdiResponse.substring(1, 4).toInt();

  // Set up the number of results to expect
  // int numMeasurements =  sdiResponse.substring(4,5).toInt();

  unsigned long timerStart = millis();
  while ((millis() - timerStart) < (1000 * wait)) {
    if (mySDI12.available()) // sensor can interrupt us to let us know it is done early
    {
      mySDI12.clearBuffer();
      break;
    }
  }
  // Wait for anything else and clear it out
  delay(30);
  mySDI12.clearBuffer();

  // in this example we will only take the 'DO' measurement
  command = "";
  command += i;
  command += F("D0!"); // SDI-12 command to get data [address][D][dataOption][!]
  mySDI12.sendCommand(command);
  while (!mySDI12.available() > 1); // wait for acknowlegement
  delay(300); // let the data transfer
  command = printBufferToScreen(i);
  mySDI12.clearBuffer();
  command += ',';
  return command;
}

/*This quickly checks if the address has already been taken by an active sensor
  @param {byte} i
  @returns {byte} addressRegister[i]
*/
boolean isTaken(char i) {
  i = charToDec(i); // e.g. convert '0' to 0, 'a' to 10, 'Z' to 61.
  byte j = i / 8;   // byte #
  byte k = i % 8;   // bit #
  return addressRegister[j] & (1 << k); // return bit status
}

/*This sets the bit in the proper location within the addressRegister
  to record that the sensor is active and the address is taken.
  @param {byte} i
  @returns {bool} initStatus
*/
boolean setTaken(byte i) {
  boolean initStatus = isTaken(i);
  i = charToDec(i); // e.g. convert '0' to 0, 'a' to 10, 'Z' to 61.
  byte j = i / 8;   // byte #
  byte k = i % 8;   // bit #
  addressRegister[j] |= (1 << k);
  return !initStatus; // return false if already taken
}

boolean checkActive(char i) {
  String myCommand = "";
  myCommand += (char) i;                 // sends basic 'acknowledge' command [address][!]
  myCommand += F("!");

  for (int j = 0; j < 3; j++) {          // goes through three rapid contact attempts
    mySDI12.sendCommand(myCommand);
    if (mySDI12.available() > 1) break;
    delay(30);
  }
  if (mySDI12.available() > 2) {   // if it hears anything it assumes the address is occupied
    mySDI12.clearBuffer();
    return true;
  }
  else {   // otherwise it is vacant.
    mySDI12.clearBuffer();
  }
  return false;
}

//------------------------------------------HigroMeter Capture Unit----------------------------------
//call to get higrometer Data
void higrometerRoutine() {

  set_inputCapture();
  finishTime = 0;
  startTime = 0;
  unsigned long higroTimeout = millis();

  while ( ((millis() - higroTimeout) < 300000) ) {

    if (!(BOOL_READ(flags2, triggered))) {
    } else {
      // period is elapsed time
      unsigned long elapsedTime = finishTime - startTime;
      // frequency is inverse of period, adjusted for clock period
      float freq = F_CPU / float (elapsedTime);  // each tick is 62.5 ns at 16 MHz
      /*
            Serial.print (elapsedTime);
            Serial.print (F(" cts "));

            Serial.print (F("F:"));
            Serial.print (freq);
            Serial.println (F(" Hz"));
      */  break;
    }
  }
}

//prepared input capture unit
void set_inputCapture() {
  noInterrupts ();  // protected code
  BOOL_TRUE(flags2, first);
  BOOL_FALSE(flags2, triggered); // re-arm for next time
  // reset Timer 1
  TCCR1A = 0;
  TCCR1B = 0;

  TIFR1 = bit (ICF1) | bit (TOV1);  // clear flags so we don't get wrong interrupts
  TCNT1 = 0;          // Counter to zero
  overflowCount = 0;  // Overflows to zero

  // Timer 1 - counts clock pulses
  TIMSK1 = bit (TOIE1) | bit (ICIE1);   // interrupt on Timer 1 overflow and input capture
  // start Timer 1, no prescaler
  TCCR1B =  bit (CS10) | bit (ICES1);  // plus Input Capture Edge Select (rising on D8)
  interrupts ();
}

//-----------------------------------------------Wind Vane Routines--------------------------------------------

void windRoutine() {

  float windSpeed;
  //If during interrupt I've read data in the correct order
  if (BOOL_READ(flags3, cycle1)) {
    noInterrupts(); //turn off interrupts

    BOOL_FALSE(flags3, cycle1);

    windSpeed = speed(anemometerDifference);
    vanePos = angleDegrees(vaneDifference, anemometerDifference);
    //error checking-noise conditions
    if ( (windSpeed != 0.0) && (vanePos != 360.0) && (vanePos > 0.0) ) {
      Serial.print(F("mph "));
      Serial.println(windSpeed, 2);
      Serial.print(F("a "));
      Serial.println(vanePos, 2);
      //if data is correct detach interrupts to avoid noise until 5 seconds have passed
      detachInterrupt(digitalPinToInterrupt(vanePin));
      detachInterrupt(digitalPinToInterrupt(anemometerPin));
    }
    interrupts();
  }
  //if 5 seconds have passed register in array last temperature read
  if (BOOL_READ(flags3, seconds5)) {

    BOOL_FALSE(flags3, seconds5);
    shiftArray(windSpeed = speed(anemometerDifference));
    //reset data
    anemometerDifference = 0;
    vaneDifference = 0;
    attachInterrupt(digitalPinToInterrupt(anemometerPin), anemometer, RISING);
    attachInterrupt(digitalPinToInterrupt(vanePin), vane, RISING);
  }

  if (second60Count >= 60) {
    //reset counter, get Average and save for this minute
    second60Count = 0;

    BOOL_TRUE(flags1, min30);
    BOOL_TRUE(flags3, debugFlag);

    avgThisMinute = getAverage(12, movingAvg);
    //Serial.print(F("avg:"));
    //Serial.println(avgThisMinute);
    //save Maximum value from array in moving Max and increment counter
    movingMax[pos10Array] = findMaximum(12, movingAvg);
    //Serial.print(F("Max:"));
    //Serial.println(movingMax[pos10Array]);
    movingMin[pos10Array] = findMinimum(12, movingAvg);
    //Serial.print(F("Min:"));
    //Serial.println(movingMin[pos10Array]);
    pos10Array++;
    //reset when after last position was reached
    if (pos10Array > 9) {
      pos10Array = 0;
    }
    //get average from last 2 averages, then update values
    movingAvg2min = (avgThisMinute + avgLastMinute) / 2;
    avgLastMinute = avgThisMinute;

    checkGust();

    BOOL_TRUE(flags3, debugFlag);
    BOOL_TRUE(flags1, min30);
  }
}

//if there's already a gust check if it's still going
//if not, check if it started
void checkGust() {
  //Serial.println(F("Checking Gust"));
  //if biggest maximum of last 10 min - last average of this minute is >= 3 knots 3.45 mph
  if (BOOL_READ(flags3, windGust)) {
    //Serial.print(F("still exists with: "));
    //Serial.print(findMaximum(10, movingMax));
    //Serial.print(F("-"));
    //Serial.println(avgThisMinute);
    if ( (findMaximum(10, movingMax) - avgThisMinute) >= 3.45) {
      //Serial.println(F("It stopped now"));
      BOOL_FALSE(flags3, windGust);
    }
  } else {

    //Serial.print(F("Avg is:"));

    //Serial.println(avgThisMinute);
    //check if minute average is >= than 9 knots 10.35mph
    if ( avgThisMinute >= 10.35) {

      //Serial.println(F("it exists"));
      //check if maximum-minimum of last ten minutes is >= than 10 knots 11.5078mph
      if ( (findMaximum(10, movingMax) - findMinimum(10, movingMin)) >= 11.5 ) {
        //check if difference between max speed and average of last 10 minutes is >= 5 knots 5.75mph
        if ((findMaximum(10, movingMax) - avgThisMinute) >= 5.75) {
          BOOL_TRUE(flags3, windGust);
        }
      }
    }
  }

  BOOL_TRUE(flags1, min30);
  BOOL_TRUE(flags3, debugFlag);
}



//given ms between closures, returns speed in mph
float speed(long closureRate) {
  float rps = 1000.0 / (float)closureRate;

  if (0.010 < rps && rps < 3.23) {
    return -0.11 * (rps * rps) + 2.93 * rps - 0.14;
  }
  else if (3.23 <= rps && rps < 54.362) {
    return 0.005 * (rps * rps) + 2.20 * rps + 1.11;
  }
  else if (54.36 <= rps && rps < 66.33) {
    return 0.11 * (rps * rps) - 9.57 * rps + 329.87;
  }
  else {
    return 0.0;
  }
}

float angleDegrees(long vaneDifference, long anemometerDifference) {
  float angle = (((float)vaneDifference / (float)anemometerDifference) * (float)360.0);
  while (angle > 360) {
    angle = angle - 360;
  }
  angle = 360 - angle;
  return angle;
}


//---------------------------------------------------ADC----------------------------------------------------------
float calculateTemp(int analogReading) {

  float temp = 0.1457 * (analogReading) - 70.6484;

  return temp;
}




//-----------------------------------------------------------GPS Methods------------------------------------
void gpsRoutine() {
  //Turn GPS on. Wait for start up, being serial comm, wait a bit and start reading.
  digitalWrite(GPSControl, HIGH);
  //delay(32000);


  GPS_Serial.begin(9600);

  latitude = 0.0;
  longitude = 0.0;
  altitude = 0.0;

  //check if GPS data is available, then try to get data GPSTries times
  for (byte i = 0; i < GPSTries; i++) {
    BOOL_TRUE(flags2, noGPS_Signal);
    BOOL_FALSE(flags2, noAltitude);
    do {
      checkHeader();
      commaSkip(2);
      getLatitude();
      commaSkip(1);
      getLongitude();
      commaSkip(4);
      getAltitude();
      newLineWait();
    } while ( BOOL_READ(flags1, gpsRoutineDone) != 1);
    BOOL_FALSE(flags1, gpsRoutineDone);
    //delay(6000);
    //Serial.print("Tries GPS: ");
    //Serial.println(i);
    if (BOOL_READ(flags2, noGPS_Signal) != 0) {
      break;
    }
  }

  //Turn GPS Off After Routine is Done
  GPS_Serial.end();

  digitalWrite(GPSControl, LOW);
}




//-----------------------------------------GPS Management----------------------------
//wait until there's data. Emergency error break in while loop
char readGPS() {
  byte emergency = 0;
  char rc = F("0");
  BOOL_TRUE(flags1, GPSPresent);

  if (GPS_Serial.available()) {
    rc = GPS_Serial.read();
  } else {
    //Serial.println(F("GPS err"));
    BOOL_TRUE(flags1, gpsRoutineDone);
    BOOL_FALSE(flags1, GPSPresent);
    BOOL_FALSE(flags2, noGPS_Signal);
  }
  return rc;
}



//------------------------------GPS String Analysis----------------------------------


//Checks for header stored in data[] array
void checkHeader() {
  static const char dataGPS[6] = "$GPGGA"; //Line of data we'll search for + a NULL char

  if ( BOOL_READ(flags1, newGPSLine) ) {
    for (byte i = 1; i < 6; i++) {
      rc = readGPS();
      if (rc != dataGPS[i]) {
        BOOL_FALSE(flags1, newGPSLine);
      } else {
        //Serial.print(rc);
      }
    }
  }
}


//Read n=number of commas, next data char will be either a comma or a number
void commaSkip(int skippedCommas) {
  if ( BOOL_TRUE(flags1, newGPSLine) ) {
    for (byte i = 0; i < skippedCommas; i++) {
      rc = '0';
      while (rc != ',') {
        rc = readGPS();
      }
    }
    //Serial.println(" ");
  }
}

//Gets the corresponsing altitude only if header was found
void getAltitude() {
  if ( BOOL_READ(flags1, newGPSLine) && BOOL_READ(flags1, GPSPresent)) {
    //Serial.println(F(""));
    for (byte i = 0; i <= 5; i++) {
      rc = readGPS();
      altitudeA[i] = rc;
      //Serial.print(rc);
      if ((rc != '.') && (rc > '9' || rc < '0')) {
        BOOL_TRUE(flags2, noAltitude);
        BOOL_TRUE(flags1, gpsRoutineDone);
        return;
      }
    }
    //Serial.println(F(""));
    altitudeA[7] = '\0';
    altitude = (float)atof(altitudeA);

    Serial.println(F("Alti "));
    Serial.println(altitude);
    BOOL_TRUE(flags1, gpsRoutineDone);
  }
}

//Gets the corresponding latitude only if header was found
void getLatitude() {
  if ( BOOL_READ(flags1, newGPSLine) ) {
    //Serial.println(F(""));

    for (byte i = 0; i <= 9; i++) {
      rc = readGPS();
      latitudeA[i] = rc;
      //Serial.print(rc);
      if ((rc > '9' || rc < '0') && (rc != '.')) {

        BOOL_FALSE(flags1, GPSPresent);
        BOOL_FALSE(flags2, noGPS_Signal);
        return 0;
      }
    }
    //Serial.println("");
    latitudeA[11] = '\0';
    latitude = (float)atof(latitudeA);
    latitude = latitude / 100;
    //Determine sign for latitude

    commaSkip(1);

    rc = readGPS();
    if (rc == 'S') {
      latitude = latitude * (-1.0);
    }

    Serial.print(F("Lat "));
    Serial.println(latitude, 7);
  }
}


//Gets the corresponsing longitude only if header was found
void getLongitude() {
  if ( BOOL_READ(flags1, newGPSLine) && BOOL_READ(flags1, GPSPresent)) {
    //Serial.println(F(""));
    for (byte i = 0; i <= 10; i++) {
      rc = readGPS();
      longitudeA[i] = rc;
      //Serial.print(rc);
      if ((rc > '9' || rc < '0') && (rc != '.')) {
        BOOL_FALSE(flags1, GPSPresent);
        BOOL_FALSE(flags2, noGPS_Signal);
        return 0;
      }
    }

    //Serial.println(F(""));


    longitudeA[12] = '\0';
    longitude = (float)atof(longitudeA);
    longitude = longitude / 100;

    commaSkip(1);

    rc = readGPS();
    if (rc == 'W') {
      longitude = longitude * (-1.0);
    }

    Serial.println(F("Long "));
    Serial.println(longitude, 7);

  }
}

//Reads bits from GPS until new line is found. New lines all start with '$'
void newLineWait() {
  while ( BOOL_READ(flags1, newGPSLine) != 1) {
    rc = readGPS();
    if (rc == '$') {
      BOOL_TRUE(flags1, newGPSLine);
      //Serial.print(rc);
    }
  }
}


//-------------------------------------------------API assign----------------------------------------------------


//Method for giving new address to sensors
/*1-Disconnect all sensors.
  //2-Connect new sensor to data Pin.
  //3-Write APIKey you want associated to sensor in terminal
  //4-Press enter. Leds will flash when done
  //3-Disconnect all sensors and connect new sensor
  //4-Press push button
  //5-Repeat 1-4 for all sensors
*/
void addressAssign() {

  bool check = false;
  int arraySize = int(APISize) - 1;
  char pos;
  char *readData;
  int index = 0;

  byte count = 0; //number of SDI-12s found

  Serial.println(F("Assigning addresses Routine"));
  Serial.println(F("Please push button when only one sensor is connected"));
  Serial.println(F(""));

  byte state = getState();

  while (state == discovery) {

    buttonWait();

    state = getState();
    if (state != byte(discovery)) {
      break;
    }
    Serial.println(F("Scanning..."));
    pos = '!';
    while (pos == '!') {
      pos = SDI_Search();
    }

    Serial.println(F(""));
    Serial.print(F("Sensor id was: "));
    Serial.println(pos);
    reassignAddress(pos);
    blinky(3);
    Serial.println(F("Sensor id is: "));
    Serial.println(char(SDIcount - 1));
    Serial.println(F(""));
    printInfo(pos);

    Serial.println(F("Enter 8 alphanumerical chars then press enter"));
    while (!check) {
      readData = getCharArray(arraySize, &index);
      if (readData != NULL) {
        Serial.print(F("Entered: "));
        for (int i = 0; i <= index; i++) {
          Serial.print(readData[i]);
        }
        Serial.println(F(""));
        check = checkAPIKey(readData, index, 8);
        index = 0;
      }
    }
    clearSerialBuffer();
    check = false;


    SDI_array[count].id = pos;
    for (int i = 0; i < 8; i++) {
      SDI_array[count].apikey[i] = readData[i];
    }
    Serial.print(F("Device id: "));
    Serial.print(SDI_array[count].id);
    Serial.print(F("  API key: "));
    Serial.println(SDI_array[count].apikey);

    count++;
    free(readData);

    Serial.println(F("Insert next sensor then push button"));
  }
}


char SDI_Search() {

  char pos = '!';
  //Search for sdi12 devices on bus

  for (byte i = '0'; i <= '9'; i++) {
    if (checkActive(i)) pos = (char) i;
  }

  for (byte i = 'a'; i <= 'z'; i++) {
    if (checkActive(i))pos = (char) i;
  }

  for (byte i = 'A'; i <= 'Z'; i++) {
    if (checkActive(i)) pos = (char) i;
  }

  return pos;
}


void reassignAddress(char pos) {
  String myCommand;
  Serial.println(F("Readdressing sensor"));
  myCommand += (char) pos;
  myCommand += F("A");
  myCommand += (char) SDIcount;
  myCommand += F("!");
  mySDI12.sendCommand(myCommand);

  delay(300);
  mySDI12.clearBuffer();

  if (SDIcount == '9') {
    SDIcount = 'a';
  } else if (SDIcount == 'z') {
    SDIcount = 'A';
  } else if (SDIcount == 'Z') {
    Serial.println(F("No more sensors can be addressed!"));
    while (1) {
      blinky(1);
    }
  } else {
    SDIcount++;
  }
  Serial.println(F("Success. Rescanning for verification."));
}



// gets identification information from a sensor, and prints it to the serial port
// expects a character between '0'-'9', 'a'-'z', or 'A'-'Z'.
void printInfo(char i) {
  String  soilSensorInfo = "";
  int j;
  String command = "";
  command += (char) i;
  command += F("I!");
  for (j = 0; j < 1; j++) {
    mySDI12.sendCommand(command);
    delay(30);
    if (mySDI12.available() > 1) break;
    if (mySDI12.available()) mySDI12.read();
  }

  while (mySDI12.available()) {
    char c = mySDI12.read();
    if ((c != '\n') && (c != '\r')) soilSensorInfo = soilSensorInfo + c;
    delay(5);
  }

  Serial.println(F("Soil Sensor Information: "));
  Serial.println(soilSensorInfo);
  Serial.println(F(""));;
}

/*checks if a char array meets certain characterisitcs.
  receives pointer to array, number of chars in it and expected size */

bool checkAPIKey(char *API, int index, int dataSize) {
  bool check = false;
  Serial.print(F("Total characters: "));
  Serial.println(index);
  if (index == dataSize) {
    check = true;
  } else {
    Serial.println(F("Wrong size"));
  }
  for (int i = 0; i < index; i++) {
    if ( (!isdigit(API[i])) && (!isalpha(API[i]))) {
      Serial.print(F("Non permitted: "));
      Serial.println(API[i]);
      check = false;
    }
  }

  if (check) {
    Serial.println(F("Data ok"));
  } else {
    Serial.print(F("Reenter "));
    Serial.println(F(" 8 alphanumeric characters"));
  }
  Serial.println(F(""));;

  return check;
}



//Generic Polling Button Routine with Debouncing
void buttonWait() {
  while (1) {
    if (digitalRead(Button) == LOW) {
      delay(15);
      if (digitalRead(Button) == LOW) {
        break;
      }
    }
  }
}

void blinky(int times) {
  for (int i = 0; i++; i < times) {
    digitalWrite(LED, LOW);
    delay(500);
    digitalWrite(LED, HIGH);
    delay(500);
    digitalWrite(LED, LOW);
    delay(500);
  }
}
