//This program is for PeetBros humidity sensor, it uses interrupts
//To measure time between pulses

//------------------------------------Definitions----------------------------
#define humidityPin 3
#define humidityInterrupt 2
#define LED 13

#define humidityState 2
#define humidityActivate 4

//micros() saved in hidrometerNow. Important to save as fast as possible
volatile unsigned long higrometerUp = 0;
volatile unsigned long higrometerDown = 0;
volatile unsigned long higrometerNow = 0;

volatile unsigned long higroArray[10]={0,0,0,0,0,0,0,0,0,0};
int i=0;

byte flags3 = 0;

//-------------------------------------Macros------------------------------
//Set to 1,0 or read bit at "pos" of byte "by" respectively
#define BOOL_TRUE(by,pos)  by|=pos
#define BOOL_FALSE(by,pos) by&= ~(pos)
#define BOOL_READ(by,pos)  bool(by&pos)


void setup() {

  setSerial();
  
  pinMode(humidityPin, INPUT_PULLUP);
  pinMode(LED, OUTPUT);
  // Falling interrupt guarantess more stability
  attachInterrupt(digitalPinToInterrupt(humidityPin), humidity, FALLING);
  interrupts();

  Serial.println(F("Initialization Complete"));
}

void loop() {
  // put your main code here, to run repeatedly:
  humidityRoutine();
}

//-----------------------Interrupts------------------------------
void humidity() {
  higrometerNow = micros();
  detachInterrupt(digitalPinToInterrupt(humidityPin));
  BOOL_TRUE(flags3, humidityActivate);
}

void humidityRoutine() {
  
  if (BOOL_READ(flags3, humidityActivate)) {
    BOOL_FALSE(flags3, humidityActivate);

    if (BOOL_READ(flags3, humidityState)) {
      higrometerDown=higrometerNow;
      BOOL_FALSE(flags3,humidityState);
      attachInterrupt(digitalPinToInterrupt(humidityPin), humidity,RISING);
      noInterrupts();
      Serial.print(higrometerDown-higrometerUp);
      Serial.println(" us");
      Serial.flush();
      interrupts(); 
    }else{
      higrometerUp=higrometerNow;
      BOOL_TRUE(flags3,humidityState);
      attachInterrupt(digitalPinToInterrupt(humidityPin), humidity,FALLING); 
    }

    if( (higrometerUp>higrometerDown) && (higrometerUp!=0) && (higrometerDown!=0)){

    }else{
      higroArray[i]=higrometerNow;
      i++;
    }
    

    if(i==10){
      i=0;
      volatile unsigned long average=0;
      for(int j=0;j<10;j++){
        average=average+higroArray[j];
      }
      average=average/10;
      Serial.print(F("Avg "));
      Serial.println(average);
    }

  
  }
}


//-------------------------Set Serial-----------------------------------------
void setSerial() {
  Serial.begin(115200);
  while (!Serial) {
    //wait for port to open
  }
}
