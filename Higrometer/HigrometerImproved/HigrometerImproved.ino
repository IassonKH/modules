// Input: Pin D8

#include "SoftwareSerial.h"
#include "IridiumSBD.h"
#include <OneWire.h>
#define first 2
#define triggered 4

//-------------------------------------Macros------------------------------
//Set to 1,0 or read bit at "pos" of byte "by" respectively
#define BOOL_TRUE(by,pos)  by|=pos
#define BOOL_FALSE(by,pos) by&= ~(pos)
#define BOOL_READ(by,pos)  bool(by&pos)


OneWire  ds(10);  // Sensor on DIGITAL pin 10 (4.7K resistor necessary between pin 10 and 3.3V from source)
SoftwareSerial nss(11, 12);

volatile unsigned long overflowCount;
volatile unsigned long startTime;
volatile unsigned long finishTime;
byte flags3 = 0;

void setup ()
{
  Serial.begin(115200);
  Serial.println("Frequency Counter");
  // set up for interrupts
  set_inputCapture();
} // end of setup

void loop() {

  higrometerRoutine();
  // wait till we have a reading
}

void higrometerRoutine() {

  if (!(BOOL_READ(flags3, triggered))) {
    return;
  }
  // period is elapsed time
  unsigned long elapsedTime = finishTime - startTime;
  // frequency is inverse of period, adjusted for clock period
  float freq = F_CPU / float (elapsedTime);  // each tick is 62.5 ns at 16 MHz

  Serial.print (elapsedTime);
  Serial.print (" cts ");

  Serial.print ("F:");
  Serial.print (freq);
  Serial.println (" Hz");

  delay (500);

  set_inputCapture();
}

// timer overflows (every 65536 counts)
ISR(TIMER1_OVF_vect) {
  overflowCount++;
}


ISR(TIMER1_CAPT_vect) {
  //save counter inmediately
  unsigned int timer1CounterValue;
  timer1CounterValue = ICR1;
  unsigned int overflowCopy = overflowCount;

  // if just missed an overflow
  if ((TIFR1 & bit (TOV1)) && timer1CounterValue < 0x7FFF) {
    overflowCopy++;
  }

  // wait until we notice last one
  if (BOOL_READ(flags3, triggered)) {
    return;
  }

  if (BOOL_READ(flags3, first)) {
    startTime = (overflowCopy << 16) + timer1CounterValue;
    BOOL_FALSE(flags3, first);
    return;
  }

  finishTime = (overflowCopy << 16) + timer1CounterValue;
  BOOL_TRUE(flags3, triggered);
  TIMSK1 = 0;    // no more interrupts for now
}

void set_inputCapture() {
  noInterrupts ();  // protected code
  BOOL_TRUE(flags3, first);
  BOOL_FALSE(flags3, triggered); // re-arm for next time
  // reset Timer 1
  TCCR1A = 0;
  TCCR1B = 0;

  TIFR1 = bit (ICF1) | bit (TOV1);  // clear flags so we don't get wrong interrupts
  TCNT1 = 0;          // Counter to zero
  overflowCount = 0;  // Overflows to zero

  // Timer 1 - counts clock pulses
  TIMSK1 = bit (TOIE1) | bit (ICIE1);   // interrupt on Timer 1 overflow and input capture
  // start Timer 1, no prescaler
  TCCR1B =  bit (CS10) | bit (ICES1);  // plus Input Capture Edge Select (rising on D8)
  interrupts ();
}


