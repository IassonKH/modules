
/*
  PCINT2

  Pin D0 TX Serial Programming/debug
  Pin D1 RX Serial Programming/debug
  Pin D2 Anemometer interrupts
  Pin D3 Anemometer interrupts
  Pin D4 Stepper Enable for Iridium module
  Pin D5 GPS Vcc, turns on or off
  Pin D6 RTC Vcc, turn on to permit reading. Only relevant with ds1307
  Pin D7

  PCINT0

  Pin D8 "Sleep" Iridium. Not really used
  Pin D9 TX from GPS
  Pin D10 Thermometer Data
  Pin D11 RX Iridium Satellite
  Pin D12 TX Iridium Satellite
  Pin D13 RX from GPS. really not u    sed

  PCINT1

  Pin A0
  Pin A1
  Pin A2
  Pin A3
  Pin A4 SDA from RTC
  Pin A5 SCL from RTC
  Pin A6
  Pin A7
*/
//------------------------------------Application Notes------------------
/*
  Wind Vane test code. Pin interrupts recevied on D2 and D3, possible trouble
  with direction?
  Needs to be tested
  Useful links:
  https://mechinations.files.wordpress.com/2016/11/wind-2-0a.pdf
  https://mechinations.files.wordpress.com/2017/01/wind-3-1.pdf
  https://mechinations.net/2014/12/27/building-a-nmea-0183-wind-instrument/
  http://forum.arduino.cc/index.php?topic=309770.0
  https://github.com/brookpatten/Arduino/blob/master/anemometer2/anemometer2.ino
  http://www.42.co.nz/freeboard/technical/interfacing/freeboardshield.html
  https://cdn.shopify.com/s/files/1/0115/9192/files/Pro-D_Anemometer.pdf?635


  Anemometer has 4 cables, two of them go to ground.
  The other two to Arduino input with a pullup to Vcc
  follow schematic on this link if in doubt.
  https://mechinations.net/2014/12/27/building-a-nmea-0183-wind-instrument/

*/

//------------------------------------Definitions------------------------



#define vaneInterrupt 0
#define anemometerInterrupt 1
//bit position of each flag
#define send_Data           1
#define thermoBounce        2
#define newGPSLine          4
#define gpsRoutineDone      8
#define RTCFail             16
#define GPSPresent          32
#define ThermoPresent       64
#define min30               128
#define noGPS_Signal        1
#define success             2
#define noAltitude          4
#define found               8
#define first               16
#define triggered           32
#define interruptAnemometer 1
#define interruptVane       2
#define cycle1              4
#define cycle2              8
#define seconds5            16
#define windGust            32
#define debugFlag           64


//Pins 4 and 5 for transistor switching, 13 is debug Led
#define vanePin         2
#define anemometerPin   3
#define IridiumControl  4
#define GPSControl      5
#define DATAPIN         7
#define Thermos         10
#define LED             13

//-------------------------------------Macros------------------------------
//Set to 1,0 or read bit at "pos" of byte "by" respectively
#define BOOL_TRUE(by,pos)  by|=pos
#define BOOL_FALSE(by,pos) by&= ~(pos)
#define BOOL_READ(by,pos)  bool(by&pos)

#include "SimpleTimer.h"

//wind sensor vars
volatile unsigned long previousAnemometer = 0;
volatile unsigned long newAnemometer = 0;
volatile unsigned long anemometerDifference = 0;

SimpleTimer timerSeconds; // timer for seconds declared with SimpleTimer Library


volatile unsigned long newVane = 0;
volatile unsigned long vaneDifference = 0;

volatile float strenghtGust = 0;
volatile float avg3Hours = 0;
volatile float movAvgWindDirection = 0;


float vanePos   = 0.0;   //last angle° from reference vane has

float movingAvg[12];  //avg of wind Speeds of last 60 seconds, each value represents last wind speed of last 5 seconds
float movingMax[10];  //maximum wind Speeds of the last 10 minutes, each position is the max wind speed of a whole minute
float movingMin[10];  //minimum wind Speeds of the last 10 minutes, each position is the min wind speed of a whole minute

float movingAvg2min = 0;  //avg of moving avg of the last 2 min
float avgLastMinute = 0;  //to save last minute avg
float avgThisMinute = 0;  //to save current last minute avg
byte flags3 = 0;


byte readTries = 0; //Count how many times I've tried to read data
byte second5Count = 0;
byte second60Count = 0;
byte pos10Array = 0; //Position in 10 wind average I'm currently at


//-------------------------Interrupts------------------------------------------
//Anemometer Interrupt Routine
void anemometer() {

  //save time value and detach interrupt to avoid multiple firing of routine
  detachInterrupt(digitalPinToInterrupt(anemometerPin));
  newAnemometer = millis();

  //incase of overflow skip reading
  if (newAnemometer - previousAnemometer > newAnemometer + previousAnemometer) {
    previousAnemometer = newAnemometer;
    attachInterrupt(digitalPinToInterrupt(anemometerPin), anemometer, RISING);
    return;
  }
  //if we have 2 anemometers interrupts between one vane interrupt
  if ((newVane >= previousAnemometer) && (newVane <= newAnemometer)) {
    BOOL_TRUE(flags3, cycle1); //prepare cycle to calculate needed data
    anemometerDifference = newAnemometer - previousAnemometer; //save values before updating data
    vaneDifference = (anemometerDifference) - (newAnemometer - newVane);
  }
  previousAnemometer = newAnemometer; //update data for next interrupt and re-arm it
  attachInterrupt(digitalPinToInterrupt(anemometerPin), anemometer, RISING);
}


//Vane Interrupt Routine, save time data, un-arm and re-arm interrupt to avoid multiple firings
void vane() {
  detachInterrupt(digitalPinToInterrupt(vanePin));
  newVane = millis();
  attachInterrupt(digitalPinToInterrupt(vanePin), vane, RISING);
}

//-----------------------------------------------------MAIN------------------------------------------

void setup() {
  // put your setup code here, to run once:

  setSerial();
  Serial.println(F("Starting..."));
  pinMode(anemometerPin, INPUT);
  pinMode(vanePin, INPUT);
  pinMode(LED, OUTPUT);


  attachInterrupt(digitalPinToInterrupt(anemometerPin), anemometer, RISING);
  attachInterrupt(digitalPinToInterrupt(vanePin), vane, RISING);
  interrupts();

  Serial.println(F("Initialization Complete"));

}

void loop() {


  timerSeconds.run();
  windRoutine();
}


//-------------------------Set Serial-----------------------------------------
void setSerial() {
  Serial.begin(115200);
  while (!Serial) {
    //wait for port to open
  }
}



//-----------------------------------------------Wind Vane Routines--------------------------------------------
void windRoutine() {

  float windSpeed;
  //If during interrupt I've read data in the correct order
  if (BOOL_READ(flags3, cycle1)) {
    noInterrupts(); //turn off interrupts

    BOOL_FALSE(flags3, cycle1);

    windSpeed = speed(anemometerDifference);
    vanePos = angleDegrees(vaneDifference, anemometerDifference);
    //error checking-noise conditions
    if ( (windSpeed != 0.0) && (vanePos != 360.0) && (vanePos > 0.0) ) {
      Serial.print(F("mph "));
      Serial.println(windSpeed, 2);
      Serial.print(F("a "));
      Serial.println(vanePos, 2);
      //if data is correct detach interrupts to avoid noise until 5 seconds have passed
    }//if it was noise turn interrupts on again
    interrupts();
  }
}

//if there's already a gust check if it's still going
//if not, check if it started
void checkGust() {
  //Serial.println(F("Checking Gust"));
  //if biggest maximum of last 10 min - last average of this minute is >= 3 knots 3.45 mph
  if (BOOL_READ(flags3, windGust)) {
    if (strenghtGust < speed(anemometerDifference)) {
      strenghtGust = speed(anemometerDifference);
    }
    //Serial.print(F("still exists with: "));
    //Serial.print(findMaximum(10, movingMax));
    //Serial.print(F("-"));
    //Serial.println(avgThisMinute);
    if ( (findMaximum(10, movingMax) - avgThisMinute) >= 3.45) {
      //Serial.println(F("It stopped now"));
      BOOL_FALSE(flags3, windGust);
    }
  } else {

    //Serial.print(F("Avg is:"));
    //Serial.println(avgThisMinute);
    //check if minute average is >= than 9 knots 10.35mph
    if ( avgThisMinute >= 10.35) {

      //Serial.println(F("it exists"));
      //check if maximum-minimum of last ten minutes is >= than 10 knots 11.5078mph
      if ( (findMaximum(10, movingMax) - findMinimum(10, movingMin)) >= 11.5 ) {
        //check if difference between max speed and average of last 10 minutes is >= 5 knots 5.75mph
        if ((findMaximum(10, movingMax) - avgThisMinute) >= 5.75) {
          BOOL_TRUE(flags3, windGust);
          strenghtGust = speed(anemometerDifference);
        }
      }
    }
  }

  BOOL_TRUE(flags3, debugFlag);
}



//given ms between closures, returns speed in mph
float speed(long closureRate) {
  float rps = 1000.0 / (float)closureRate;

  if (0.010 < rps && rps < 3.23) {
    return -0.11 * (rps * rps) + 2.93 * rps - 0.14;
  }
  else if (3.23 <= rps && rps < 54.362) {
    return 0.005 * (rps * rps) + 2.20 * rps + 1.11;
  }
  else if (54.36 <= rps && rps < 66.33) {
    return 0.11 * (rps * rps) - 9.57 * rps + 329.87;
  }
  else {
    return 0.0;
  }
}

float angleDegrees(long vaneDifference, long anemometerDifference) {
  float angle = (((float)vaneDifference / (float)anemometerDifference) * (float)360.0);
  while (angle > 360) {
    angle = angle - 360;
  }
  angle = 360 - angle;
  return angle;
}

//latest reading will be in last position of array
//shift is value that will be replaced from array
void shiftArray(float windSpeed) {
  memmove(&movingAvg[0], &movingAvg[1], sizeof(movingAvg) - sizeof(*movingAvg));
  movingAvg[11] = windSpeed;
}



//-----------------------------------------------Extra Timer------------------------------

//Timer for time period, executed every second

void secondsCount() {
  //Serial.println(seconds);
  //flash led
  digitalWrite(LED, !digitalRead(LED));
  //seconds counts days, takeReading data acquisition period, 5 count and 60 count are for windVane

  second5Count++;
  second60Count++;

  if (second5Count >= 5) {
    //flag to register last read speed and vanePos
    BOOL_TRUE(flags3, seconds5);
    second5Count = 0;
  }
}
/*If RTC died, count extra days every 8 times we read data. 8*3 hour period=24 hours
*/



//---------------------------------------------------------------Arithmetic Methods---------------------------

//this methods recevie an array and a size, then they find the max or minx value in given array inside the first SIZE spaces
float findMaximum(int SIZE, float avgArray[]) {
  float highest = avgArray[0];
  for (int i = 0; i < SIZE; i++) {
    if (highest < avgArray[i]) {
      highest = avgArray[i];
    }
  }
  return highest;
}

float findMinimum(int SIZE, float avgArray[]) {
  float lowest = avgArray[0];
  for (int i = 0; i < SIZE; i++) {
    if (lowest > avgArray[i]) {
      lowest = avgArray[i];
    }
  }
  return lowest;
}

//this method recevies an array and a size, then it calculates the average value in given array inside the first SIZE spaces
float getAverage(int SIZE, float movingAvg[]) {

  float average = 0;
  for (byte i = 0; i < SIZE; i++) {
    average = average + movingAvg[i];
  }
  average = average / SIZE;
  return average;
}


