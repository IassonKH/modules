

//------------------------------------Definitions------------------------
#define anemometerPin 3
#define anemometerInterrupt 1
#define Led 13
#define vanePin 2
#define vaneInterrupt 0
#define debounce 75
#define LED 13

#define cycle1 1
#define cycle2 2

#define anemometerTurn 2
#define vaneTurn 4
#define interruptAnemometer 8
#define interruptVane 16
#define checkAnemometer 32
#define checkVane 64

//-------------------------------------Macros------------------------------
//Set to 1,0 or read bit at "pos" of byte "by" respectively
#define BOOL_TRUE(by,pos)  by|=pos
#define BOOL_FALSE(by,pos) by&= ~(pos)
#define BOOL_READ(by,pos)  bool(by&pos)

#include "SimpleTimer.h"

SimpleTimer debounceTimers;  //to count debounce

unsigned short debounceAnemometer = 0;
unsigned short debounceVane = 0;

//wind sensor vars
volatile unsigned long previousAnemometer = 0;
volatile unsigned long newAnemometer = 0;
volatile unsigned long anemometerDifference = 0;

volatile unsigned long newVane = 0;
volatile unsigned long vaneDifference = 0;

float windSpeed;
float vanePos;

byte flags2 = 0;
byte flags3 = 0;

bool actualState1=0;
bool actualState2=0;
long timeN=0;

void setup() {
  // put your setup code here, to run once:

  setSerial();
  //Serial.println(F("Starting..."));
  pinMode(anemometerPin, INPUT_PULLUP);
  pinMode(vanePin, INPUT_PULLUP);
  pinMode(LED, OUTPUT);

  //when called they will count the debounce time

  interrupts();

  Serial.println(F("Initialization Complete"));

}

void loop() {
  while(millis()<5000){
    Serial.print("A");
    Serial.print(millis());
    Serial.print(" ");
    Serial.println(digitalRead(anemometerPin));

    Serial.print("V");
    Serial.print(millis());
    Serial.print(" ");
    Serial.println(digitalRead(vanePin));
  }
 
 /*

  if (digitalRead(vanePin) == HIGH && digitalRead(anemometerPin) == HIGH) {
  } else {
    Serial.print(millis());
    Serial.print(" ");
    
    if (digitalRead(vanePin) == HIGH) {
      Serial.print("1 ");
    } else {
      Serial.print("0 ");
    }
    
    if (digitalRead(anemometerPin) == HIGH) {
      Serial.println("1");
    } else {
      Serial.println("0");
    }
    
    
  }*/
  delay(1);

}




//-------------------------Set Serial-----------------------------------------
void setSerial() {
  Serial.begin(115200);
  while (!Serial) {
    //wait for port to open
  }
}

//-------------------------Interrupts------------------------------------------
void anemometer() {
  detachInterrupt(digitalPinToInterrupt(anemometerPin));
  BOOL_TRUE(flags2, checkAnemometer);
}

void vane() {
  detachInterrupt(digitalPinToInterrupt(vanePin));
  BOOL_TRUE(flags2, checkVane);
}

