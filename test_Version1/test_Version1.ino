//Version 1 of software, it contains hygrometer, sdi, thermometer, barometer, gps.
//This wasn't a final version 
/*
  PCINT2

  Pin D0 TX Serial Programming/debug
  Pin D1 RX Serial Programming/debug
  Pin D2 Anemometer Interrupts for wind direction (green wire)
  Pin D3 Anemometer Interrupts for wind speed (yellow wire)
  Pin D4 Enable 1
  Pin D5 Enable 2
  Pin D6
  Pin D7 SDI-12 Data Pin for soil sensors(blue wire)

  PCINT0

  Pin D8
  Pin D9  TX from GPS
  Pin D10 Thermometer Data
  Pin D11 RX Iridium Satellite
  Pin D12 TX Iridium Satellite
  Pin D13 Led

  PCINT1

  Pin A0 Rain Polling
  Pin A1
  Pin A2
  Pin A3 Temperature + humidity sensor Vin
  Pin A4 SDA from RTC
  Pin A5 SCL from RTC
  Pin A6 Analog only
  Pin A7 Analog only
*/

//------------------------------------Application Notes------------------
/*
  Transistor must be used for powering up GPS, Iridium with a digital pin. GPS cannot receive while comms with Iridium are going and vice-versa.

    Digital Pins 0-7  - Port D - PCINT2
    Digital Pins 8-13 - Port B - PCINT0
    Analog Pins A0-A5 - Port C - PCINT1

  SDI and software Serial both try to assign this pins to a ISR. SDI is in pcint 2(pin 7), sw keeps everyother one
  modify .cpp by commenting out declaration as seen here depending on application:
  https://github.com/EnviroDIY/Arduino-SDI-12/issues/8
  Software Serial.cpp is in your computer at Arduino/hardware/arduino/avr/libraries/SoftwareSerial/src

  DO NOT USE ANY SOFTWARE SERIALS ON DIGITAL PINS 0-7!!!

  SDI12 library taken from here: https://github.com/EnviroDIY/Arduino-SDI-12
  Iridium library taken from here:https://github.com/mikalhart/IridiumSBD
  RTClibrary taken from here: https://github.com/adafruit/RTClib
  SimpleTimer library taken from here: https://github.com/jfturcot/SimpleTimer
*/
//------------------------------------Definitions------------------------

#define DS1307_I2C_ADDRESS 0x68

#define vaneInterrupt 0
#define anemometerInterrupt 1

//Pins 4 and 5 for transistor switching, 13 is debug Led
#define vanePin         2
#define anemometerPin   3
#define IridiumControl  4
#define GPSControl      5
#define DATAPIN         7
#define Thermos         10
#define LED             13

#define thermoError 10 //thermometer has a problem
#define GPSTries 4 //do a few GPS read tries to try to obtain data

#define SatelliteTries 2

//bit position of each flag
#define send_Data           1
#define thermoBounce        2
#define newGPSLine          4
#define gpsRoutineDone      8
#define RTCFail             16
#define GPSPresent          32
#define ThermoPresent       64
#define min30               128
#define noGPS_Signal        1
#define success             2
#define noAltitude          4
#define found               8
#define first               16
#define triggered           32
#define interruptAnemometer 1
#define interruptVane       2
#define cycle1              4
#define cycle2              8
#define seconds5            16
#define windGust            32
#define debugFlag           64


//-------------------------------------Macros------------------------------
//Set to 1,0 or read bit at "pos" of byte "by" respectively
#define BOOL_TRUE(by,pos)  by|=pos
#define BOOL_FALSE(by,pos) by&= ~(pos)
#define BOOL_READ(by,pos)  bool(by&pos)

//-------------------------------------Libraries---------------------------
#include "IridiumSBD.h"
#include <OneWire.h>
#include "SoftwareSerial.h"
#include "SimpleTimer.h"
#include "RTClib.h"
//#include "avr/power.h"
#include "SDI12.h"
#include <AM2320.h>

//----------------------------------Modify time intervals-----------------

#define measurementsNumber 3 //times I will take measurements before sending data

//----------------------------------Constants & Global variables----------

byte flags1 = 0;
byte flags2 = 0;
byte flags3 = 0;

//addressRegister for SDI-12
byte addressRegister[8] = {
  0B00000000,
  0B00000000,
  0B00000000,
  0B00000000,
  0B00000000,
  0B00000000,
  0B00000000,
  0B00000000
};

unsigned long seconds = 0; // seconds counter
short takeReading = 0; //value in seconds between each data acquisition

SimpleTimer timerSeconds; // timer for seconds declared with SimpleTimer Library

byte readTries = 0; //Count how many times I've tried to read data
byte second5Count = 0;
byte second60Count = 0;
byte pos10Array = 0; //Position in 10 wind average I'm currently at

//variables changed within interrupts for the vane. anemometerDifference
//returned to 0 when no interrupts have happened in the last 5 seconds
volatile unsigned long previousAnemometer = 0;
volatile unsigned long newAnemometer = 0;
volatile unsigned long anemometerDifference = 0;

volatile unsigned long newVane = 0;
volatile unsigned long vaneDifference = 0;

volatile unsigned long startGust = 0;
volatile unsigned long endGust = 0;


unsigned long referenceTime = 0; //will reset on year 2038

float vanePos   = 0.0;   //last angle° from reference vane has

float celsius;
float externalCelsius[measurementsNumber]={0};
float hygroCelsius[measurementsNumber]={0};
float hygroHumidity[measurementsNumber]={0};
float rainAvg[24] = { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1};

float movingAvg[12];  //avg of wind Speeds of last 60 seconds, each value represents last wind speed of last 5 seconds
float movingMax[10];  //maximum wind Speeds of the last 10 minutes, each position is the max wind speed of a whole minute
float movingMin[10];  //minimum wind Speeds of the last 10 minutes, each position is the min wind speed of a whole minute

float movingAvg2min = 0;  //avg of moving avg of the last 2 min
float avgLastMinute = 0;  //to save last minute avg
float avgThisMinute = 0;  //to save current last minute avg

float latitude  = 0.0;  //gps data
float longitude = 0.0;
float altitude  = 0.0;

float in = 0; // inches of first minute converted to millimeters by the rain gauge
float mm = 0; // inches of second minute to give average of 2 minutes

bool rainState;

//arrays to convert gps data to chars
char longitudeA[12];
char latitudeA[11];
char altitudeA[7];
//receive buffer for serial comms
char rc;

OneWire  ds(Thermos);  // Sensor on DIGITAL pin 10 (4.7K resistor necessary between pin 10 and 3.3V from source)
SoftwareSerial nss(11, 12);  //Iridium object created
SoftwareSerial GPS_Serial(9, 13); //TX from GPS to Arduino Pin 9
IridiumSBD isbd(nss, 13); //to sleep iridium, not really used
RTC_DS1307 rtc;  //instance rtc object
SDI12 mySDI12(DATAPIN);  //instance SDI12 object
AM2320 th;


//Array to store data of every soil sensor
struct SDI_info {
  char apikey[8];
  char id;
};
//save space for three sensors
struct SDI_info SDI_array[1];
//---------------------------------------External Interrupts-------------------------------------------
//Anemometer Interrupt Routine
void anemometer() {
  //if we choose anemometer routines over pluviometers, anemometerP=1

  //save time value and detach interrupt to avoid multiple firing of routine
  detachInterrupt(digitalPinToInterrupt(anemometerPin));
  newAnemometer = millis();

  //incase of overflow skip reading
  if (newAnemometer - previousAnemometer > newAnemometer + previousAnemometer) {
    previousAnemometer = newAnemometer;
    attachInterrupt(digitalPinToInterrupt(anemometerPin), anemometer, RISING);
    return;
  }
  //if we have 2 anemometers interrupts between one vane interrupt
  if ((newVane >= previousAnemometer) && (newVane <= newAnemometer)) {
    BOOL_TRUE(flags3, cycle1); //prepare cycle to calculate needed data
    anemometerDifference = newAnemometer - previousAnemometer; //save values before updating data
    vaneDifference = (newAnemometer - previousAnemometer) - (newAnemometer - newVane);
  }
  previousAnemometer = newAnemometer; //update data for next interrupt and re-arm it
  attachInterrupt(digitalPinToInterrupt(anemometerPin), anemometer, RISING);
}


//Vane Interrupt Routine, save time data, un-arm and re-arm interrupt to avoid multiple firings
void vane() {
  detachInterrupt(digitalPinToInterrupt(vanePin));
  newVane = millis();
  attachInterrupt(digitalPinToInterrupt(vanePin), vane, RISING);
}

//-----------------------------------------------------MAIN------------------------------------------

void setup() {
  //--------------------------Serial Debug-----------------------------------
  //sets serial comms
  setSerial();
  //initialize sensors and environment
  initialize();

  //--------------------------RTC Module-------------------------------------
  setRTC();

  //--------------------------First Run--------------------------------------
  //Gets reference time
  readDS1307time();

  //Search for sdi12 devices on bus, even if we already assigned apikeys to each
  discoverSDI12();
  SDI_array[0].apikey[8] = F("SDISoil1");

  //enable external interrupts on respective Pins
  attachInterrupt(digitalPinToInterrupt(anemometerPin), anemometer, RISING);
  attachInterrupt(digitalPinToInterrupt(vanePin), vane, RISING);
}

void loop() {

  timerSeconds.run();
  //----------------Data Read----------------------------------------

  windRoutine();
  rainCheck();
  readData();
  //----------------Data Send----------------------------------------
  sendData();

}

//-------------------------Serial Comms-----------------------------------------------

void setSerial() {
  Serial.begin(115200);
  while (!Serial) {
    //wait for port to open
  }
}


void initialize() {

  Serial.println(F("Boot"));
  //define pins as outputs and inputs
  pinMode(LED, OUTPUT);  //debug on board led
  pinMode(IridiumControl, OUTPUT); //transistor to Satellite
  pinMode(GPSControl, OUTPUT); //transistor to GPS
  pinMode(A3, OUTPUT); //pin designed to turn on/off humidity + temperature sensor

  pinMode(anemometerPin, INPUT); //interrupt pins turned on for wind vane
  pinMode(vanePin, INPUT);
  pinMode(A0, INPUT);  //pin polled for rain sensor

  //turn Iridium,GPS hygrometer off
  digitalWrite(IridiumControl, LOW); //turn off Iridium
  digitalWrite(GPSControl, LOW);    //turn off GPS
  digitalWrite(A3, LOW); //turn off humidity + temperature sensor

  rainState = digitalRead(A0); //get initial rain meter state

  //start SDI bus and wait a little bit
  mySDI12.begin();
  delay(500);

  //library is in ms, will enter "secondsCount" every second approx
  timerSeconds.setInterval(1000, secondsCount);

  //flags for system
  BOOL_TRUE(flags1, send_Data);
  BOOL_FALSE(flags1, thermoBounce);
  BOOL_TRUE(flags1, newGPSLine);
  BOOL_FALSE(flags1, gpsRoutineDone);
  BOOL_FALSE(flags1, RTCFail);
  BOOL_TRUE(flags1, GPSPresent);
  BOOL_TRUE(flags1, ThermoPresent);
  BOOL_TRUE(flags1, min30);
  BOOL_FALSE(flags2, noGPS_Signal);
  BOOL_FALSE(flags2, success);
  BOOL_FALSE(flags2, noAltitude);
  BOOL_FALSE(flags3, interruptAnemometer);
  BOOL_FALSE(flags3, interruptVane);
  BOOL_FALSE(flags3, cycle1);
  BOOL_TRUE(flags3, debugFlag);

  interrupts();

  Serial.println(F("R"));
}
//-------------------------------Data Read---------------------------
//reads thermometer every hour
void readData() {
  byte thermoCounter = 0;
  //min30 is a flag to define data has to be read, time between data acquisition can be changed in secondsCount()
  if ( BOOL_READ(flags1, min30) ) {

    BOOL_FALSE(flags1, min30);
    //power up internal modules of arduino
    //powerUpRoutine();

    //get time if possible from RTC
    readDS1307time();

    while (thermoCounter < thermoError) {
      celsius = registerTemperature();
      externalCelsius[readTries] = celsius;
      Serial.println("a");
      Serial.println(externalCelsius[readTries]);
      if (celsius != -100.5) {
        thermoCounter = 11;
      } else {
        thermoCounter++;
      }
    }
    thermoCounter = 0;

    hygroCelsius[readTries] = temperatureRead();
    Serial.println("b");
    Serial.println(hygroCelsius[readTries]);
    hygroHumidity[readTries]= humidityRead();
    Serial.println("c");
    Serial.println(hygroHumidity[readTries]);
    readTries++;

    if (readTries >= measurementsNumber) {

      BOOL_TRUE(flags1, send_Data);

    } else {
      //Power down internal modules to save power
      //lowPowerRoutine();
    }
  }

}

//------------------------------------Data Send--------------------------
void sendData() {

  if (BOOL_READ(flags1, send_Data)) {

    BOOL_FALSE(flags1, send_Data);
    //Serial.println(F("Sending data"));
    //get data from GPS
    gpsRoutine();

    //try to send Data. If no satellite is connected data will be sent via serial comms
    satelliteRoutine();

    //reset amount of times I've read sucessfully
    readTries = 0;
    //lowPowerRoutine();
    //reset period time counter
    seconds = 0;
  }

}

//-----------------------------------------------Extra Timer------------------------------

//Timer for time period, executed every second

void secondsCount() {
  //Serial.println(seconds);
  //flash led
  digitalWrite(LED, !digitalRead(LED));
  //seconds counts days, takeReading data acquisition period, 5 count and 60 count are for windVane
  seconds++;
  takeReading++;
  second5Count++;
  second60Count++;

  if (second5Count >= 5) {
    //flag to register last read speed and vanePos
    BOOL_TRUE(flags3, seconds5);
    second5Count = 0;
  }
  //reading every 60 seconds
  if (takeReading >= 60) {
    //Serial.println(F("1 min passed"));
    BOOL_TRUE(flags1, min30);
    BOOL_TRUE(flags3, debugFlag);
    takeReading = 0;
  }
  if (seconds >= 86304) {
    /*If RTC died, count extra days every 86400 seconds
      value was experimentally compensated to 86304 to account
      for drift due to temperature in device*/
    if (BOOL_READ(flags1, RTCFail)) {
      referenceTime = referenceTime + seconds;
      BOOL_TRUE(flags1, min30);
      BOOL_TRUE(flags3, debugFlag);
    }
    seconds = 0;
  }
}
//-----------------------------------------------Wind Vane Routines--------------------------------------------
void windRoutine() {

  float windSpeed;
  //If during interrupt I've read data in the correct order
  if (BOOL_READ(flags3, cycle1)) {
    noInterrupts(); //turn off interrupts

    BOOL_FALSE(flags3, cycle1);

    windSpeed = speed(anemometerDifference);
    vanePos = angleDegrees(vaneDifference, anemometerDifference);
    //error checking-noise conditions
    if ( (windSpeed != 0.0) && (vanePos != 360.0) && (vanePos > 0.0) ) {
      //Serial.print(F("mph "));
      //Serial.println(windSpeed, 2);
      //Serial.print(F("a "));
      //Serial.println(vanePos, 2);
      //if data is correct detach interrupts to avoid noise until 5 seconds have passed
      detachInterrupt(digitalPinToInterrupt(vanePin));
      detachInterrupt(digitalPinToInterrupt(anemometerPin));
    }//if it was noise turn interrupts on again
    interrupts();
  }
  //if 5 seconds have passed register in array last wind data read
  if (BOOL_READ(flags3, seconds5)) {
    BOOL_FALSE(flags3, seconds5);
    shiftArray(windSpeed = speed(anemometerDifference));
    //reset data
    anemometerDifference = 0;
    vaneDifference = 0;
    attachInterrupt(digitalPinToInterrupt(anemometerPin), anemometer, RISING);
    attachInterrupt(digitalPinToInterrupt(vanePin), vane, RISING);
  }

  if (second60Count >= 60) {
    //reset counter, get Average and save for this minute
    second60Count = 0;

    BOOL_TRUE(flags1, min30);
    BOOL_TRUE(flags3, debugFlag);

    avgThisMinute = getAverage(12, movingAvg);
    //Serial.print(F("avg:"));
    //Serial.println(avgThisMinute);
    //save Maximum value from array in moving Max and increment counter
    movingMax[pos10Array] = findMaximum(12, movingAvg);
    //Serial.print(F("Max:"));
    //Serial.println(movingMax[pos10Array]);
    movingMin[pos10Array] = findMinimum(12, movingAvg);
    //Serial.print(F("Min:"));
    //Serial.println(movingMin[pos10Array]);
    pos10Array++;
    //reset when after last position was reached
    if (pos10Array > 9) {
      pos10Array = 0;
    }
    //get average from last 2 averages, then update values
    movingAvg2min = (avgThisMinute + avgLastMinute) / 2;
    avgLastMinute = avgThisMinute;

    checkGust();

    BOOL_TRUE(flags3, debugFlag);
    BOOL_TRUE(flags1, min30);
  }
}

//if there's already a gust check if it's still going
//if not, check if it started
void checkGust() {
  //Serial.println(F("Checking Gust"));
  //if biggest maximum of last 10 min - last average of this minute is >= 3 knots 3.45 mph
  if (BOOL_READ(flags3, windGust)) {
    //Serial.print(F("still exists with: "));
    //Serial.print(findMaximum(10, movingMax));
    //Serial.print(F("-"));
    //Serial.println(avgThisMinute);
    if ( (findMaximum(10, movingMax) - avgThisMinute) >= 3.45) {
      //Serial.println(F("It stopped now"));
      BOOL_FALSE(flags3, windGust);
    }
  } else {

    //Serial.print(F("Avg is:"));

    //Serial.println(avgThisMinute);
    //check if minute average is >= than 9 knots 10.35mph
    if ( avgThisMinute >= 10.35) {

      //Serial.println(F("it exists"));
      //check if maximum-minimum of last ten minutes is >= than 10 knots 11.5078mph
      if ( (findMaximum(10, movingMax) - findMinimum(10, movingMin)) >= 11.5 ) {
        //check if difference between max speed and average of last 10 minutes is >= 5 knots 5.75mph
        if ((findMaximum(10, movingMax) - avgThisMinute) >= 5.75) {
          BOOL_TRUE(flags3, windGust);
        }
      }
    }
  }

  BOOL_TRUE(flags1, min30);
  BOOL_TRUE(flags3, debugFlag);
}



//given ms between closures, returns speed in mph
float speed(long closureRate) {
  float rps = 1000.0 / (float)closureRate;

  if (0.010 < rps && rps < 3.23) {
    return -0.11 * (rps * rps) + 2.93 * rps - 0.14;
  }
  else if (3.23 <= rps && rps < 54.362) {
    return 0.005 * (rps * rps) + 2.20 * rps + 1.11;
  }
  else if (54.36 <= rps && rps < 66.33) {
    return 0.11 * (rps * rps) - 9.57 * rps + 329.87;
  }
  else {
    return 0.0;
  }
}

float angleDegrees(long vaneDifference, long anemometerDifference) {
  float angle = (((float)vaneDifference / (float)anemometerDifference) * (float)360.0);
  while (angle > 360) {
    angle = angle - 360;
  }
  angle = 360 - angle;
  return angle;
}

//--------------------------------------Hygrometer-----------------------------------------

float humidityRead() {

  digitalWrite(A3, HIGH); //turn on humidity + temperature sensor
  delay(5000);

  String val; //string for end result
  byte tries = 0;//try to get data 10 times
  float humidity;

  while (tries < 10) {
    switch (th.Read()) {
      case 2:
        tries++;
        break;
      case 1:
        //Serial.println(F("Sensor off"));
        tries++;
        break;
      case 0:
        val = th.Humidity;
        if (val == F("0") || val == F("0.0") ) {
          tries++;
          break;
        }

        break;
    }
  }
  humidity = val.toFloat();
  return humidity;
}

float temperatureRead() {

  String val; //string for end result
  byte tries = 0;//try to get data 10 times
  float temp;

  while (tries < 10) {
    switch (th.Read()) {
      case 2:
        tries++;
        break;
      case 1:
        tries++;
        break;
      case 0:
        val = th.cTemp;
        if (val == F("0") || val == F("0.0")) {
          tries++;
          break;
        }
        tries = 11;
        break;
    }
  }

  digitalWrite(A3, LOW); //turn off humidity + temperature sensor
  temp = val.toFloat();
  return temp;
}

//---------------------------------------Rain Gauge----------------------------------------
void rainCheck() {


  if (rainState != digitalRead(A0)) {

    //Serial.println(digitalRead(A0));

    in = in + 0.01;
    mm = in * 25.4;

    rainState = !rainState;
  }
}

//------------------------------------------SDI-12--------------------------------------

/*Recieves the address of the sensor, reads its data, and stores it on a string
  @param {char} i*/
String printBufferToScreen(char i) {
  String soilSensorMeditions;
  int addressCount = 0;
  String buffer = F("7.");
  buffer += String(addressCount, DEC);
  buffer += F(":");
  addressCount++;
  mySDI12.read(); // consume address
  mySDI12.read(); // consume addres
  while (mySDI12.available()) {
    char c = mySDI12.read();
    if (c == '+' || c == '-') {
      buffer += ',';
      buffer += String(addressCount, DEC) + ":";
      addressCount++;
      if (c == '-') buffer += '-';
    }
    else if ((c != '\n') && (c != '\r')) {
      buffer += c;
    }
    delay(50);
  }
  //Serial.println(F(""));
  //Serial.flush();
  soilSensorMeditions = buffer;

  //Serial.println(soilSensorMeditions);
  return soilSensorMeditions;
}
//cheks if any sdi12 device is present and sets array
void discoverSDI12() {
  for (byte i = '0'; i <= '9'; i++) if (checkActive(i)) setTaken(i); // scan address space 0-9
  for (byte i = 'a'; i <= 'z'; i++) if (checkActive(i)) setTaken(i); // scan address space a-z
  for (byte i = 'A'; i <= 'Z'; i++) if (checkActive(i)) setTaken(i); // scan address space A-Z
}


/*Recieves the address of the sensor, and sends the commands to read the data
  @param {char} i
*/
String takeMeasurement(char i) {
  String command = "";
  command += i;
  command += F("M!"); // SDI-12 measurement command format  [address]['M'][!]
  mySDI12.sendCommand(command);
  // wait for acknowlegement with format [address][ttt (3 char, seconds)][number of measurments available, 0-9]
  String sdiResponse = "";
  delay(30);
  while (mySDI12.available())  // build response string
  {
    char c = mySDI12.read();
    //Serial.print(c);

    if ((c != '\n') && (c != '\r'))
    {
      sdiResponse += c;
      delay(5);
    }
  }
  mySDI12.clearBuffer();


  // find out how long we have to wait (in seconds).
  unsigned int wait = 0;
  wait = sdiResponse.substring(1, 4).toInt();

  // Set up the number of results to expect
  // int numMeasurements =  sdiResponse.substring(4,5).toInt();

  unsigned long timerStart = millis();
  while ((millis() - timerStart) < (1000 * wait)) {
    if (mySDI12.available()) // sensor can interrupt us to let us know it is done early
    {
      mySDI12.clearBuffer();
      break;
    }
  }
  // Wait for anything else and clear it out
  delay(30);
  mySDI12.clearBuffer();

  // in this example we will only take the 'DO' measurement
  command = "";
  command += i;
  command += F("D0!"); // SDI-12 command to get data [address][D][dataOption][!]

  mySDI12.sendCommand(command);
  while (!mySDI12.available() > 1); // wait for acknowlegement
  delay(300); // let the data transfer
  command = printBufferToScreen(i);
  mySDI12.clearBuffer();
  command += ',';
  //Serial.println(command);
  return command;
}

/*This quickly checks if the address has already been taken by an active sensor
  @param {byte} i
  @returns {byte} addressRegister[i]
*/
boolean isTaken(char i) {
  i = charToDec(i); // e.g. convert '0' to 0, 'a' to 10, 'Z' to 61.
  byte j = i / 8;   // byte #
  byte k = i % 8;   // bit #
  return addressRegister[j] & (1 << k); // return bit status
}

/*This sets the bit in the proper location within the addressRegister
  to record that the sensor is active and the address is taken.
  @param {byte} i
  @returns {bool} initStatus
*/
boolean setTaken(byte i) {

  boolean initStatus = isTaken(i);
  i = charToDec(i); // e.g. convert '0' to 0, 'a' to 10, 'Z' to 61.
  byte j = i / 8;   // byte #
  byte k = i % 8;   // bit #
  addressRegister[j] |= (1 << k);
  return !initStatus; // return false if already taken
}

boolean checkActive(char i) {
  String myCommand = "";
  myCommand += (char) i;                 // sends basic 'acknowledge' command [address][!]
  myCommand += F("!");

  for (int j = 0; j < 3; j++) {          // goes through three rapid contact attempts
    mySDI12.sendCommand(myCommand);
    if (mySDI12.available() > 1) break;
    delay(30);
  }
  if (mySDI12.available() > 2) {   // if it hears anything it assumes the address is occupied
    mySDI12.clearBuffer();
    return true;
  }
  else {   // otherwise it is vacant.
    mySDI12.clearBuffer();
  }
  return false;
}


//-----------------------------------------------------------GPS Methods------------------------------------
void gpsRoutine() {

  //Turn GPS on. Wait for start up, being serial comm, wait a bit and start reading.
  digitalWrite(GPSControl, HIGH);
  delay(32000);
  GPS_Serial.begin(9600);

  latitude = 0.0;
  longitude = 0.0;
  altitude = 0.0;

  //check if GPS data is available, then try to get data GPSTries times
  for (byte i = 0; i < GPSTries; i++) {
    BOOL_TRUE(flags2, noGPS_Signal);
    BOOL_FALSE(flags2, noAltitude);
    do {
      checkHeader();
      commaSkip(2);
      getLatitude();
      commaSkip(1);
      getLongitude();
      commaSkip(4);
      getAltitude();
      newLineWait();
    } while ( BOOL_READ(flags1, gpsRoutineDone) != 1);
    BOOL_FALSE(flags1, gpsRoutineDone);
    //delay(6000);
    //Serial.print("Tries GPS: ");
    //Serial.println(i);
    if (BOOL_READ(flags2, noGPS_Signal) != 0) {
      break;
    }
  }

  //Turn GPS Off After Routine is Done
  GPS_Serial.end();

  digitalWrite(GPSControl, LOW);
}


//-----------------------------------------GPS Management----------------------------
//wait until there's data. Emergency error break in while loop
char readGPS() {
  byte emergency = 0;
  char rc = F("0");
  BOOL_TRUE(flags1, GPSPresent);

  if (GPS_Serial.available()) {
    rc = GPS_Serial.read();
  } else {
    //Serial.println(F("GPS err"));
    BOOL_TRUE(flags1, gpsRoutineDone);
    BOOL_FALSE(flags1, GPSPresent);
    BOOL_FALSE(flags2, noGPS_Signal);
  }
  return rc;
}



//------------------------------GPS String Analysis----------------------------------

//Checks for header stored in data[] array
void checkHeader() {
  static const char dataGPS[6] = "$GPGGA"; //Line of data we'll search for + a NULL char

  if ( BOOL_READ(flags1, newGPSLine) ) {
    for (byte i = 1; i < 6; i++) {
      rc = readGPS();
      if (rc != dataGPS[i]) {
        BOOL_FALSE(flags1, newGPSLine);
      } else {
        //Serial.print(rc);
      }
    }
  }
}


//Read n=number of commas, next data char will be either a comma or a number
void commaSkip(int skippedCommas) {
  if ( BOOL_TRUE(flags1, newGPSLine) ) {
    for (byte i = 0; i < skippedCommas; i++) {
      rc = '0';
      while (rc != ',') {
        rc = readGPS();
      }
    }
    //Serial.println(" ");
  }
}

//Gets the corresponsing altitude only if header was found
void getAltitude() {
  if ( BOOL_READ(flags1, newGPSLine) && BOOL_READ(flags1, GPSPresent)) {
    //Serial.println(F(""));
    for (byte i = 0; i <= 5; i++) {
      rc = readGPS();
      altitudeA[i] = rc;
      //Serial.print(rc);
      if ((rc != '.') && (rc > '9' || rc < '0')) {
        BOOL_TRUE(flags2, noAltitude);
        BOOL_TRUE(flags1, gpsRoutineDone);
        return;
      }
    }
    //Serial.println(F(""));
    altitudeA[7] = '\0';
    altitude = (float)atof(altitudeA);

    //Serial.println(F("Alti "));
    //Serial.println(altitude);
    BOOL_TRUE(flags1, gpsRoutineDone);
  }
}

//Gets the corresponding latitude only if header was found
void getLatitude() {
  if ( BOOL_READ(flags1, newGPSLine) ) {
    //Serial.println(F(""));

    for (byte i = 0; i <= 9; i++) {
      rc = readGPS();
      latitudeA[i] = rc;
      //Serial.print(rc);
      if ((rc > '9' || rc < '0') && (rc != '.')) {

        BOOL_FALSE(flags1, GPSPresent);
        BOOL_FALSE(flags2, noGPS_Signal);
        return 0;
      }
    }
    //Serial.println("");
    latitudeA[11] = '\0';
    latitude = (float)atof(latitudeA);
    latitude = latitude / 100;
    //Determine sign for latitude

    commaSkip(1);

    rc = readGPS();
    if (rc == 'S') {
      latitude = latitude * (-1.0);
    }

    //Serial.print(F("Lat "));
    //Serial.println(latitude, 7);
  }
}


//Gets the corresponsing longitude only if header was found
void getLongitude() {
  if ( BOOL_READ(flags1, newGPSLine) && BOOL_READ(flags1, GPSPresent)) {
    //Serial.println(F(""));
    for (byte i = 0; i <= 10; i++) {
      rc = readGPS();
      longitudeA[i] = rc;
      //Serial.print(rc);
      if ((rc > '9' || rc < '0') && (rc != '.')) {
        BOOL_FALSE(flags1, GPSPresent);
        BOOL_FALSE(flags2, noGPS_Signal);
        return 0;
      }
    }

    //Serial.println(F(""));


    longitudeA[12] = '\0';
    longitude = (float)atof(longitudeA);
    longitude = longitude / 100;

    commaSkip(1);

    rc = readGPS();
    if (rc == 'W') {
      longitude = longitude * (-1.0);
    }

    //Serial.println(F("Long "));
    //Serial.println(longitude, 7);

  }
}

//Reads bits from GPS until new line is found. New lines all start with '$'
void newLineWait() {
  while ( BOOL_READ(flags1, newGPSLine) != 1) {
    rc = readGPS();
    if (rc == '$') {
      BOOL_TRUE(flags1, newGPSLine);
      //Serial.print(rc);
    }
  }
}


//----------------------------------------------------------Temperature Management-------------------------------

float registerTemperature() {

  byte i;
  byte type_s = 0; //to check for thermometer type
  byte present = 0; //
  byte dataTemperature[12]; //temperature data
  byte addr[8];
  float celsius = -100.5;

  if (!ds.search(addr)) {
    ds.reset_search();
    delay(220);
    return;
  }

  //Check if CRC is valid
  if (OneWire::crc8(addr, 7) != addr[7]) {
    return;
  }
  type_s = 0; //for DS18B20

  ds.reset();
  ds.select(addr);
  ds.write(0x44, 1);        // start conversion, with parasite power on

  delay(753);     // A little more than 750 ms here

  present = ds.reset();
  ds.select(addr);
  ds.write(0xBE);         // Read Scratchpad

  for ( i = 0; i < 9; i++) {
    dataTemperature[i] = ds.read();
  }
  /* Serial.print(" CRC=");
    Serial.print(OneWire::crc8(data, 8), HEX);
    Serial.println();*/
  // Convert the data to actual temperature
  // because the result is a 16 bit signed integer, it should
  // be stored to an "int16_t" type, which is always 16 bits
  // even when compiled on a 32 bit processor.

  int16_t raw = (dataTemperature[1] << 8) | dataTemperature[0];
  if (type_s) {
    raw = raw << 3; // 9 bit resolution default
    if (dataTemperature[7] == 0x10) {
      // "count remain" gives full 12 bit resolution
      raw = (raw & 0xFFF0) + 12 - dataTemperature[6];
    }
  } else {
    byte cfg = (dataTemperature[4] & 0x60);
    // at lower res, the low bits are undefined, so let's zero them
    if (cfg == 0x00) raw = raw & ~7;  // 9 bit resolution, 93.75 ms
    else if (cfg == 0x20) raw = raw & ~3; // 10 bit res, 187.5 ms
    else if (cfg == 0x40) raw = raw & ~1; // 11 bit res, 375 ms
    //// default is 12 bit resolution, 750 ms conversion time
  }
  celsius = float (raw) / 16.0;
 
  return celsius;
}


//-----------------------------------------------RTC Management--------------------------------------
void setRTC() {
  //RTCFail =1 in case we can't connect to it
  if (! rtc.begin()) {
    BOOL_TRUE(flags1, RTCFail);
  } else {
    BOOL_FALSE(flags1, RTCFail);
  }
}

void readDS1307time() {
  DateTime now = rtc.now();

  //checks if RTC is not present
  if ((now.year() == 2165 || now.month() == 165 || now.year() == 165) && (now.unixtime() != 0) ) {
    BOOL_TRUE(flags1, RTCFail);
    Serial.println(F("RTC dead"));
  } else {
    BOOL_FALSE(flags1, RTCFail);
    //Serial.println(now.unixtime());
  }

  //update todays reference time only if RTC's working and readtries has reset
  if (readTries == 0 && !(BOOL_READ(flags1, RTCFail))) {
    referenceTime = now.unixtime();
  }

}


//---------------------------------------------------------------Arithmetic Methods---------------------------

//this methods recevie an array and a size, then they find the max or minx value in given array inside the first SIZE spaces
float findMaximum(int SIZE, float avgArray[]) {
  float highest = avgArray[0];
  for (int i = 0; i < SIZE; i++) {
    if (highest < avgArray[i]) {
      highest = avgArray[i];
    }
  }
  return highest;
}

float findMinimum(int SIZE, float avgArray[]) {
  float lowest = avgArray[0];
  for (int i = 0; i < SIZE; i++) {
    if (lowest > avgArray[i]) {
      lowest = avgArray[i];
    }
  }
  return lowest;
}

//this method recevies an array and a size, then it calculates the average value in given array inside the first SIZE spaces
float getAverage(int SIZE, float movingAvg[]) {

  float average = 0;
  for (byte i = 0; i < SIZE; i++) {
    average = average + movingAvg[i];
  }
  average = average / SIZE;
  return average;
}

//changes chars to decimals for SDI ids
byte charToDec(char i) {
  if ((i >= '0') && (i <= '9')) return i - '0';
  if ((i >= 'a') && (i <= 'z')) return i - 'a' + 10;
  if ((i >= 'A') && (i <= 'Z')) return i - 'A' + 37;
  else return i;
}

//shift wind Array to the left, then stores lastest reading
void shiftArray(float windSpeed) {
  memmove(&movingAvg[0], &movingAvg[1], sizeof(movingAvg) - sizeof(*movingAvg));
  movingAvg[11] = windSpeed;
}

//----------------------------------------------------------Iridium Management------------------------------

//blink lead while sending data with iridium
boolean ISBDCallback() {
  digitalWrite(LED_BUILTIN, (millis() / 1000) % 2 == 1 ? HIGH : LOW);
  return true;
}

void satelliteRoutine() {

  detachInterrupt(digitalPinToInterrupt(anemometerPin));
  detachInterrupt(digitalPinToInterrupt(vanePin));
  byte tries = 0; //time out fatal error variable for satellite comm
  //Turn Iridium on. Wait for start up, being serial comm, wait a bit and start reading.
  digitalWrite(IridiumControl, HIGH);
  //150 sec for start up time, 120 sec minimum

  delay(60000);
  delay(60000);
  delay(30000);


  isbd.setPowerProfile(1);
  nss.begin(19200);

  //isbd.attachConsole(Serial);
  isbd.setPowerProfile(1);
  isbd.adjustSendReceiveTimeout(50);
  isbd.begin();
  frameRoutine();
  //reset variables for Iridium, turn off module
  BOOL_FALSE(flags2, success);

  nss.end();
  isbd.sleep();
  digitalWrite(IridiumControl, LOW);

  Serial.flush();

  attachInterrupt(digitalPinToInterrupt(anemometerPin), anemometer, RISING);
  attachInterrupt(digitalPinToInterrupt(vanePin), vane, RISING);
}

//Build frame depending on what Data was available/is requiered
void frameRoutine() {

  int signalQuality = -1; //default value for iridium modem minimum signal quality
  //possible to prealocate char array with malloc and keep track of current pos with counter, more efficient?

  //reserve() method makes an "array" of the string and instead of causing memory fragmentation, makes it so
  //the string is manipulated inside it. once the method ends space is freed
  String stringFrame;
  String temporal;


  //this pre-allocates a 270 char buffer in memory to work with.
  if (!(stringFrame.reserve(270))) {
    Serial.println(F("Not enough space"));
    return;
  }

  byte tries = 0;

  //Serial.print(F("Termo:"));
  temporal = String_Thermo();
  stringFrame += temporal;
  //Serial.println(temporal);

  //Serial.print(F("GPS:"));
  temporal = String_GPS();
  stringFrame += temporal;
  //Serial.println(temporal);

  //Serial.print(F("Wind Vane:"));
  temporal = String_WindVane();
  stringFrame += temporal;
  //Serial.println(temporal);

  //Serial.print(F("RTC:"));
  temporal = String_RTC();
  stringFrame += temporal;
  //Serial.println(temporal);

  //Serial.print(F("Rain:"));
  temporal = String_Rain();
  stringFrame += temporal;
  //Serial.println(temporal);

  //Serial.print(F("SDI:"));
  temporal = String_SDI12();
  stringFrame += temporal;
  //Serial.println(temporal);

  //Serial.print(F("Hygro"));
  temporal = String_Higrometer();
  stringFrame += temporal;
  //Serial.println(temporal);


  temporal = String_CRC(stringFrame);
  stringFrame = String(F("#GX8K2F3T,")) + stringFrame + temporal;
  char *chars = const_cast<char*>(stringFrame.c_str());

  Serial.println(F(""));
  Serial.println(chars);
  Serial.flush();


  int err;

  while (BOOL_READ(flags2, success) == 0) {
    if (tries < SatelliteTries) {
      err = isbd.getSignalQuality(signalQuality);

      if (err != 0) {
        Serial.print(F("Sig Quality fail: err "));
        Serial.println(err);
        tries++;
      }

      Serial.print(F("Quality "));
      Serial.println(signalQuality);
      if (signalQuality < 1) {
        tries++;
        err = 11;
      } else {
        err = isbd.sendSBDText(chars);
      }
      if (err != 0) {
        Serial.print(F("sendText err "));
        Serial.println(err);
        tries++;
      } else {
        BOOL_TRUE(flags2, success);
      }
    } else {
      BOOL_TRUE(flags2, success);
      Serial.println(F("Error with SDB module"));
    }
    delay(6000);
  }

  Serial.println(F("Frame Routine Done"));
}


String String_RTC() {

  if (referenceTime == 0) {
    BOOL_TRUE(flags1, RTCFail);
  }

  String RTC_String = "";
  //timestamp
  if (!(BOOL_READ(flags1, RTCFail))) {
    //Serial.println(F("RTC present"));
    RTC_String += String(referenceTime, DEC);
    RTC_String += F(",");
  }
  return RTC_String;
}

String String_GPS() {

  String GPS_String = "";

  if (BOOL_READ(flags2, noGPS_Signal)) {
    Serial.println(F("GPS present"));
    GPS_String += String(longitude, 7) + ",";
    GPS_String += String(latitude, 7) + ",";

    if (!BOOL_READ(flags2, noAltitude)) {
      GPS_String += String(altitude, 2) + ",";
    }
  }
  //timestamp
  return GPS_String;
}


String String_SDI12() {

  String soil = "";
  //taken used to keep count of where we are on the SDI array
  //everytime we find one we increase by one
  int taken = 0;
  // scan address space 0-9
  for (char i = '0'; i <= '9'; i++) if (isTaken(i)) {

      soil += takeMeasurement(char(i)) + F("@");
      for (int j = 0; j < 8; j++) {
        soil += SDI_array[taken].apikey[j];
      }
      soil += F(",");
      taken++;
    }

  // scan address space a-z
  for (char i = 'a'; i <= 'z'; i++) if (isTaken(i)) {
      soil += takeMeasurement(i) + F("@");
      for (int j = 0; j < 8; j++) {
        soil += SDI_array[taken].apikey[j];
      }      soil += F(",");
      taken++;
    }

  // scan address space A-Z
  for (char i = 'A'; i <= 'Z'; i++) if (isTaken(i)) {
      soil += takeMeasurement(i) + F("@");
      for (int j = 0; j < 8; j++) {
        soil += SDI_array[taken].apikey[j];
      }
      soil += F(",");
      taken++;
    };

  return soil;
}

String String_Thermo() {

  String thermo_String = "";
  String temporal = F("@");


  if (BOOL_READ(flags1, ThermoPresent)) {
    for (int i = 0; i < measurementsNumber; i++) {
      thermo_String +=  String(externalCelsius[i], 2);
      thermo_String += F(",");
    }
  }

  return thermo_String;
}

String String_WindVane() {

  String meditions = "";

  if ((vanePos > 0 || vanePos < 360)) {
    meditions += String(vanePos, 2) + ",";
  }

  //1mph=0.447 m/s
  meditions += String((movingAvg2min * 0.447), 2);

  meditions += ",";

  if (BOOL_READ(flags3, windGust)) {
    meditions += F("G,");
  }

  return meditions;
}


String String_Rain() {

  String rain = "";
  /*    float average = (mm2 + mm1) / 2;

      mm2 = mm1;
      mm1 = 0;
  */
  rain += String(mm, 2);
  rain += F(",");
  mm = 0;
  return rain;
}

String String_Higrometer() {

  String temp;
  String val;
  for (byte j = 0; j < measurementsNumber; j++) {
    temp += String(hygroHumidity[j], 2);
    temp += ",";
  }

  for (byte i = 0; i < measurementsNumber; i++) {
    temp += String(hygroCelsius[i], 2);
    temp += ",";
  }
  /*  digitalWrite(A3, HIGH); //turn off humidity + temperature sensor
    delay(5000);
    String val; //string for end result
    byte tries = 0;//try to get data 10 times
    String temporal;
    while (tries < 10) {
      switch (th.Read()) {
        case 2:
          Serial.println(F("CRC F"));
          tries++;
          break;
        case 1:
          //Serial.println(F("Sensor off"));
          tries++;
          break;
        case 0:
          temporal = th.Humidity;
          if (temporal == F("0")) {
            tries++;
            break;
          } else {
            val = val + temporal;
          }

          temporal = th.cTemp;
          if (temporal == F("0")) {
            tries++;
            break;
          } else {
            val += temporal;
          }
          tries = 11;

          /*Serial.print("  Humidity = ");
            Serial.print(th.Humidity);
            Serial.println("%");
            Serial.print("  Temperature = ");
            Serial.print(th.cTemp);
            Serial.println("*C");
            Serial.println();

          break;
      }
    }
    digitalWrite(A3, LOW); //turn off humidity + temperature sensor
  */
  return val;
}

String String_CRC(String stringFrame) {
  String CRC_String = "";
  String crc;

  byte bytes[stringFrame.length() + 1];
  stringFrame.getBytes(bytes, stringFrame.length() + 1);
  byte crc8 = CRC8(bytes, stringFrame.length());


  if (String(crc8, HEX).length() == 1) {
    crc = "0" + String(crc8, HEX);
  } else {
    crc = String(crc8, HEX);
  }

  CRC_String = "*" + crc ;

  return CRC_String;
}

byte CRC8(const byte * data, byte len) {
  byte crc = 0x00;
  while (len--) {
    crc ^= *data++;
    for (byte i = 0; i < 8; i++) {
      if (crc & 0x80) crc = (crc << 1) ^ 0x07;
      else crc <<= 1;
    }
  }
  return crc;
}




