//This program uses echo sound sensor JSN v2. It was developed due to the diferences between JSN and JSN v2
//v1.0  program will give distance from sensor connected to arduino every 2 seconds.
//Reported working distance is between 20cm and 800cm

#define ultraSoundSignal    9 // Trigger conversion pin
#define ultraSoundSignalIN  7 // Ultrasound signal in pin. Here the signal will change when sound arrives

//------------setup system

void setup(){
  Serial.begin(115200);
  pinMode(ultraSoundSignalIN, INPUT_PULLUP);   //Switch signal sound in pin
  pinMode(ultraSoundSignal, OUTPUT); // Switch signal trigger pin to output
}

//Device will call ping() every 2 seconds and print out the total distance read
void loop(){
  Serial.println(ping());
  Serial.println();
  delay(2000); //delay 2 seconds.
}

//method that "pings" sound and gives back distance as a long. Trigger is first fired and then time is counted until sounds comes back
unsigned long ping() {

  unsigned long ultrasoundValue = 0; 
  
  digitalWrite(ultraSoundSignal, LOW); // Send low pulse
  delayMicroseconds(2); // Wait for 2 microseconds
  digitalWrite(ultraSoundSignal, HIGH); // Send high pulse
  delayMicroseconds(20); // Wait for 20 microseconds
  digitalWrite(ultraSoundSignal, LOW); // Holdoff
  
  //pulseIn counts time between flanks on a pin. sets timeout at 8 seconds
  unsigned long echo = pulseIn(ultraSoundSignalIN, HIGH,80000); //Listen for echo
  
  Serial.println(echo);
  ultrasoundValue = (echo / 58.138); //convert to CM
  return ultrasoundValue;

}

