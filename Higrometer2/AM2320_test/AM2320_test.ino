//Test using Library for digital AM2320 sensor
//Temperature must always be read after humidity due to how the sensors cursor works

#include <Wire.h>
#include <AM2320.h>

AM2320 th;

//Runs once at start up, Pin A3 is optionally used to power up device
//It can be conected to V+ normally if not 
void setup() {
  Serial.begin(115200);
  Wire.begin();

  pinMode(A3,OUTPUT);  
  digitalWrite(A3, HIGH);
}

//Main with a while(1), switch determines if sensor is available or not
//then we read its data

void loop() {
  Serial.println("Chip = AM2320");
  switch(th.Read()) {
    case 2:
      Serial.println("  CRC failed");
      break;
    case 1:
      Serial.println("  Sensor offline");
      break;
    case 0:
      Serial.print("  Humidity = ");
      Serial.print(th.Humidity);
      Serial.println("%");
      Serial.print("  Temperature = ");
      Serial.print(th.cTemp);
      Serial.println("*C");
      Serial.println();
      break;
  }
  delay(2000);
}
