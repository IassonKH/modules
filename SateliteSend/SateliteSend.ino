#include <IridiumSBD.h>
#include <SoftwareSerial.h>
#define SatelliteControl 4

SoftwareSerial nss(11, 12);
IridiumSBD isbd(nss, 13);

void setup()
{
  int signalQuality = -1;


  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(SatelliteControl, OUTPUT);
  digitalWrite(SatelliteControl,HIGH);
  pinMode(5, OUTPUT);
  digitalWrite(5,LOW);
  
  
  Serial.begin(115200);
  Serial.println("1");
  
  nss.begin(19200);

  isbd.attachConsole(Serial);
  isbd.begin();

Serial.println("2");
  int err = isbd.getSignalQuality(signalQuality);
  if (err != 0)
  {
    Serial.print("SignalQuality failed: error ");
    Serial.println(err);
    return;
  }

  Serial.print("Signal quality is ");
  Serial.println(signalQuality);

  err = isbd.sendSBDText("&7,y46JGR7c,ub97qVK6,,,,13.1,29.8,7.2,109,2725,126,1*07");
  if (err != 0)
  {
    Serial.print("sendSBDText failed: error ");
    Serial.println(err);
    return;
  }

  Serial.println("Hey, it worked!");
  Serial.print("Messages left: ");
  Serial.println(isbd.getWaitingMessageCount());
}

void loop()
{
   digitalWrite(LED_BUILTIN, HIGH);
}

bool ISBDCallback()
{
   digitalWrite(LED_BUILTIN, (millis() / 1000) % 2 == 1 ? HIGH : LOW);
   return true;
}
