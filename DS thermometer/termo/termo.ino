#include <OneWire.h>
 
int DS18S20_Pin = 10; //DS18S20 En pin  10
 
//Temperature chip i/o
OneWire ds(DS18S20_Pin);  // En pin 10
 
void setup(void) {
  Serial.begin(115200);
}
 
void loop(void) {
  float temperature = registerTemperature();
  Serial.println(temperature);
 
  delay(100);
   
}
 

float registerTemperature() {
  float celsius;
  byte i;
  byte type_s = 0; //to check for thermometer type
  byte present = 0; //
  byte dataTemperature[12]; //temperature data
  byte addr[8];
  
    if (!ds.search(addr)) {
      ds.reset_search();
      delay(220);
      return;
    }

    //Check if CRC is valid
    if (OneWire::crc8(addr, 7) != addr[7]) {
      return;
    }
  type_s=0; //for DS18B20
  
  ds.reset();
  ds.select(addr);
  ds.write(0x44, 1);        // start conversion, with parasite power on

  delay(753);     // A little more than 750 ms here

  present = ds.reset();
  ds.select(addr);
  ds.write(0xBE);         // Read Scratchpad

  /*Serial.print("  Data = ");
    Serial.print(present, HEX);
  */  Serial.print(" ");
  for ( i = 0; i < 9; i++) {           // 9 bytes
    dataTemperature[i] = ds.read();
  }
  /* Serial.print(" CRC=");
    Serial.print(OneWire::crc8(data, 8), HEX);
    Serial.println();*/
  // Convert the data to actual temperature
  // because the result is a 16 bit signed integer, it should
  // be stored to an "int16_t" type, which is always 16 bits
  // even when compiled on a 32 bit processor.

  int16_t raw = (dataTemperature[1] << 8) | dataTemperature[0];
  if (type_s) {
    raw = raw << 3; // 9 bit resolution default
    if (dataTemperature[7] == 0x10) {
      // "count remain" gives full 12 bit resolution
      raw = (raw & 0xFFF0) + 12 - dataTemperature[6];
    }
  } else {
    byte cfg = (dataTemperature[4] & 0x60);
    // at lower res, the low bits are undefined, so let's zero them
    if (cfg == 0x00) raw = raw & ~7;  // 9 bit resolution, 93.75 ms
    else if (cfg == 0x20) raw = raw & ~3; // 10 bit res, 187.5 ms
    else if (cfg == 0x40) raw = raw & ~1; // 11 bit res, 375 ms
    //// default is 12 bit resolution, 750 ms conversion time
  }
  celsius = (float)raw / 16.0;
  return celsius;
}




 
float getTemp(){
  //da temperature en celsius
 
  byte data[12];
  byte addr[8];
 
  if ( !ds.search(addr)) {
      //no hay mas sensores en la cadena, resetea
      ds.reset_search();
      return -100;
  }
 
  if ( OneWire::crc8( addr, 7) != addr[7]) {
      Serial.println("CRC no valido!");
      return -1000;
  }
 
  if ( addr[0] != 0x10 && addr[0] != 0x28) {
      Serial.print("Aparato no reconocido");
      return -1000;
  }
 
  ds.reset();
  ds.select(addr);
  ds.write(0x44,1); // start conversion, with parasite power on at the end
 
  byte present = ds.reset();
  ds.select(addr);    
  ds.write(0xBE); // Read Scratchpad
 
   
  for (int i = 0; i < 9; i++) { // we need 9 bytes
    data[i] = ds.read();
  }
   
  ds.reset_search();
   
  byte MSB = data[1];
  byte LSB = data[0];
 
  float tempRead = ((MSB << 8) | LSB); //using two's compliment
  float TemperatureSum = tempRead / 16;
   
  return TemperatureSum;
   
}
