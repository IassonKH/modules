//This code works for Peet-bros humidity and temperature sensor
//It is used to calibrate the device by giving raw input. To calibrate a controlled environment and a reference temp are needed
// Input: Pin D8

#include "SoftwareSerial.h"
#include "IridiumSBD.h"
#include <OneWire.h>
#define first 2
#define triggered 4
#define ADCPin 17

//-------------------------------------Macros------------------------------
//Set to 1,0 or read bit at "pos" of byte "by" respectively
#define BOOL_TRUE(by,pos)  by|=pos
#define BOOL_FALSE(by,pos) by&= ~(pos)
#define BOOL_READ(by,pos)  bool(by&pos)


OneWire  ds(10);  // Sensor on DIGITAL pin 10 (4.7K resistor necessary between pin 10 and 3.3V from source)

volatile unsigned long overflowCount;
volatile unsigned long startTime;
volatile unsigned long finishTime;
byte flags3 = 0;


//Thermometer management variables
float celsius;

void setup() {
  Serial.begin(115200);
  //Serial.println("Frequency Counter");
  // set up for interrupts
  set_inputCapture();
} // end of setup

void loop() {
//higrometerRoutine();
 analogTemp();
 registerTemperature();
 Serial.print(celsius); 
 Serial.println(F(" C° "));
 /*Serial.print(celsius); 
 Serial.print(F(" C° "));
 Serial.print(millis());
 Serial.print(F(" ms"));
 */Serial.println(F(""));
  delay(500);
  // wait till we have a reading
}


void analogTemp(){
  int ADCValue=analogRead(ADCPin);
  Serial.print(ADCValue);
  Serial.print(F(" adc "));

  float temp=calculateTemp(ADCValue);
  
  Serial.print(temp,2);
  Serial.print(F(" anaC° "));
}

float calculateTemp(int analogReading) {

  float temp = 0.1457 * (analogReading) -70.6484;

  return temp;
}


void higrometerRoutine() {

  if (!(BOOL_READ(flags3, triggered))) {
    return;
  }
  // period is elapsed time
  unsigned long elapsedTime = finishTime - startTime;
  // frequency is inverse of period, adjusted for clock period
  float freq = -553.21 + (elapsedTime * 0.49);

  Serial.print (freq);
  Serial.print (" % humidity  ");

  //Serial.print ("F:");
  //Serial.print (freq);
  //Serial.println (" Hz");

  delay (500);

  set_inputCapture();
}


// timer overflows (every 65536 counts)
ISR(TIMER1_OVF_vect) {
  overflowCount++;
}


ISR(TIMER1_CAPT_vect) {
  //save counter inmediately
  unsigned int timer1CounterValue;
  timer1CounterValue = ICR1;
  unsigned int overflowCopy = overflowCount;

  // if just missed an overflow
  if ((TIFR1 & bit (TOV1)) && timer1CounterValue < 0x7FFF) {
    overflowCopy++;
  }

  // wait until we notice last one
  if (BOOL_READ(flags3, triggered)) {
    return;
  }

  if (BOOL_READ(flags3, first)) {
    startTime = (overflowCopy << 16) + timer1CounterValue;
    BOOL_FALSE(flags3, first);
    return;
  }

  finishTime = (overflowCopy << 16) + timer1CounterValue;
  BOOL_TRUE(flags3, triggered);
  TIMSK1 = 0;    // no more interrupts for now
}

void set_inputCapture() {
  noInterrupts ();  // protected code
  BOOL_TRUE(flags3, first);
  BOOL_FALSE(flags3, triggered); // re-arm for next time
  // reset Timer 1
  TCCR1A = 0;
  TCCR1B = 0;

  TIFR1 = bit (ICF1) | bit (TOV1);  // clear flags so we don't get wrong interrupts
  TCNT1 = 0;          // Counter to zero
  overflowCount = 0;  // Overflows to zero

  // Timer 1 - counts clock pulses
  TIMSK1 = bit (TOIE1) | bit (ICIE1);   // interrupt on Timer 1 overflow and input capture
  // start Timer 1, no prescaler
  TCCR1B =  bit (CS10) | bit (ICES1);  // plus Input Capture Edge Select (rising on D8)
  interrupts ();
}



void registerTemperature() {
  byte i;
  byte type_s = 0; //to check for thermometer type
  byte present = 0; //
  byte dataTemperature[12]; //temperature data
  byte addr[8];  //address of thermometer

  if (!ds.search(addr)) {
    ds.reset_search();
    delay(220);
  
    return;
  }

  //Check if CRC is valid
  if (OneWire::crc8(addr, 7) != addr[7]) {
  
    return;
  }
  //type_s=0 for DS18B20
  /*  Following code finds Thermometer type if unknown
    //Serial.println();
    // the first ROM byte indicates which chip
    switch (addr[0]) {
    case 0x10:
      //Serial.println("  Chip = DS18S20");  // or old DS1820
      type_s = 1;
      break;
    case 0x28:
      //Serial.println("  Chip = DS18B20");
      type_s = 0;
      break;
    case 0x22:
      //Serial.println("  Chip = DS1822");
      type_s = 0;
      break;
    default:
      //Serial.println("Device is not a DS18x20 family device.");
      thermoCounter++;
      return;
    }*/
 
  ds.reset();
  ds.select(addr);
  ds.write(0x44, 1);        // start conversion, with parasite power on

  delay(753);     // A little more than 750 ms here

  present = ds.reset();
  ds.select(addr);
  ds.write(0xBE);         // Read Scratchpad

  /*  //Serial.print("  Data = ");
    //Serial.print(present, HEX);
    //Serial.print(" ");*/
  for ( i = 0; i < 9; i++) {           // 9 bytes
    dataTemperature[i] = ds.read();
  }
  /* //Serial.print(" CRC=");
    //Serial.print(OneWire::crc8(data, 8), HEX);
    //Serial.println();*/
  // Convert the data to actual temperature
  // because the result is a 16 bit signed integer, it should
  // be stored to an "int16_t" type, which is always 16 bits
  // even when compiled on a 32 bit processor.

  int16_t raw = (dataTemperature[1] << 8) | dataTemperature[0];
  if (type_s) {
    raw = raw << 3; // 9 bit resolution default
    if (dataTemperature[7] == 0x10) {
      // "count remain" gives full 12 bit resolution
      raw = (raw & 0xFFF0) + 12 - dataTemperature[6];
    }
  } else {
    byte cfg = (dataTemperature[4] & 0x60);
    // at lower res, the low bits are undefined, so let's zero them
    if (cfg == 0x00) raw = raw & ~7;  // 9 bit resolution, 93.75 ms
    else if (cfg == 0x20) raw = raw & ~3; // 10 bit res, 187.5 ms
    else if (cfg == 0x40) raw = raw & ~1; // 11 bit res, 375 ms
    //// default is 12 bit resolution, 750 ms conversion time
  }

  celsius = (float)raw / 16.0;
}



