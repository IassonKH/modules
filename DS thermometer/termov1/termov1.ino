
//------------------------------------Definitions------------------------
//#define SLOW_CLOCK
#define PRECISION_READ
//------------------------------------Libraries---------------------------
#include <avr/sleep.h>
#include <avr/power.h>
#include <avr/wdt.h>
#include <OneWire.h>
#include <SoftwareSerial.h>

//----------------------------------Constants y Global variables-------

bool hacerEnvio = 0; 
bool wdr = 0; //bandera de wd para el main
bool thermoBounce=0; //redundancy for forcing a thermometer read
bool newGpsLine = 1;          //bool becomes 0 if the data we want is not sent
bool gpsDataReady = 0;        //to break loop when GPS data is obtained
bool dataValidation = 0;
bool gpsRoutineDone=0;

int contT = 0; //veces que wdt se ha despertado
double contH = 0; //horas que han pasado
int contSAux = 0; //se le suma tiempo de wake up 0.065s+0.002s a cada ciclo para evitar errores
int auxSobrante = 0;
int thermoCounter=0; //times thermo wasn't found
int maxPos; //array position for highest and lowest temperatures
int minPos;

char altitudeA[11];
char lattitudeA[10];
char rc;

const int thermoError=10; //thermometer has a problem
const int constMin = 2; //modificar para cambiar frecuencia
const int constHoras = 48; //en la que pasan acciones de envio de datos- constHoras/2=horas
const int constSegAux = 597; //597*0.067s= 39.999s. 5 Ciclos de 8 segundos.
const int measurementsNumber=14;
const char check = 'A';       //Flag that will tell us if data exists
const char dataGPS[7] = "$GPRMC"; //Line of data we'll search for + null char

float lattitude=0.0;
float altitude=0.0;

//---------------------------------Virtual Serial----------------------------------
OneWire  ds(10);  // Sensor on DIGITAL pin 10 (4.7K resistor necessary between pin 10 and 3.3V from source)
SoftwareSerial GPS_Serial(9, 11); //TX from GPS to Arduino Pin 9


//-------Temperature Variables-----------

struct temperature{
  float measurement;
  double _time;
};//Struct for Array registering temperatures

temperature temperatures[measurementsNumber];

byte i;  
byte type_s; //to check for thermometer type
byte present = 0; //
byte dataTemperature[12]; //temperature data
byte addr[8];  //address of thermometer
float celsius, fahrenheit;

//-------------------------------------Interrupts----------------------------
// watchdog interrupt
ISR (WDT_vect) {
  wdt_disable();
  wdr = 1;
}

//-------------------------------------Setup---------------------------------
void setup() {
  //Un-comment def SLOW_LOCK for power saving. Sets baud_rate to 9600 regardless
  setPrescaler();
  //--------------------------GPS Serial-------------------------------------
  setGPS();
  //--------------------------ADCs-------------------------------------------
  setADC();
  //--------------------------Serial Debug-----------------------------------
  setSerial();

}

//-----------------------------------------Loop------------------------------
void loop() {
  //Debugging method
  //flash();
  //----------------Lectura de Datos----------------------------------------
  leerDatos();
  //----------------Envio de Datos------------------------------------------
  enviarDatos();
  //----------------Conteo de tiempo----------------------------------------
  //conteoTiempo();
  //----------------Rutina de Bajo Consumo----------------------------------
  lowPowerRutine();
  delay(1000);
}


//-----------------------------------------Blink Led------------------------
//Metodo estandar para flashear un led, uso para debuggeo
/*void flash () {
  pinMode (LED, OUTPUT);
  //flashear i=4 veces
  for (byte i = 0; i < 4; i++) {
    digitalWrite (LED, HIGH);
    delay (5);
    digitalWrite (LED, LOW);
    delay (95);
  }
  //regresar a bajo consumo
  pinMode (LED, OUTPUT);
  digitalWrite (LED, LOW);
}*/

//---------------------------------------------Conteo de Tiempo------------------------------
/*void conteoTiempo() {
  if (wdr == 1) {
    wdr = 0;
    contT++;
    //225*8s=30 min
    contSAux++;
    //compensa 24s cada 359 ciclos de 8 segundos

    if (contSAux == constSegAux) {
      contT = contT + 5;
      contSAux = 0;
    }
    if (contT >= constMin) {
      //resset de 30 min y mantengo sobrante en caso de que exista
      contT = contT - 225;
      contH++;
      //48*30m=24 horas
    } if (contH == constHoras) {
      hacerEnvio = 1;
      contH = 0;
    }
  }
}*/

//--------------------------------------------------Envio de Datos---------------------------
void enviarDatos() {
   gpsRoutineDone=0;
  if (hacerEnvio == 1) {
    hacerEnvio = 0;
    tempRoutine();
    Serial.println(" ");
    gpsRoutine();
    Serial.println(" ");
  }
}

//--------------------------------------------------Lectura de Datos---------------------------
void leerDatos() {
  //ADC_Read();
  //Serial_Read();

  //Try to read thermometer 10 times
  while(thermoBounce==0){
    
    registerTemperature();
    temperatureArray();
    
    if(thermoCounter==thermoError){
     //if all 10 reads fail, send a warning
      Serial.println("Thermometer Error");
    }
  }
  thermoBounce=0;
  
}

//--------------------------------------------------Usar ADCs----------------------------------
void ADC_Read() {
  if (hacerEnvio == 1) {
    //pinMode(analogPin, INPUT);
    //temperatura = analogRead(analogPin);
  }
}

//---------------------------------------------------Leer Serial----------------------------------
void Serial_Read() {
  if (hacerEnvio == 1) {
  }
}

//--------------------------------------------------Bajo Consumo------------------------------
void lowPowerRutine() {
  //apagar adcs
  ADCSRA = 0;
  //--------------------------Reseteo banderas------------------------------
  // clear banderas de reset
  MCUSR = 0;
  WDTCSR = bit (WDCE) | bit (WDE);
  // settear modo de interrupcion e intervalo. Info en Datasheet
  WDTCSR = bit (WDIE) | bit (WDP3) | bit (WDP0);  //set WDIE y delay 8s
  wdt_reset();
  //-----------------------Sleep Mode--------------------------------------
  set_sleep_mode (SLEEP_MODE_PWR_DOWN);
  noInterrupts ();
  sleep_enable();
  interrupts();
  sleep_cpu ();
  //cancelar sleep como precaución
  sleep_disable();
}


//-------------------------Set Prescaler------------------------------------
void setPrescaler() {
#ifdef SLOW_CLOCK
  noInterrupts();
  CLKPR = _BV(CLKPCE);
  CLKPR = _BV(CLKPS0); //frecuencia de 16Mhz entre 2
  interrupts();
#else

#endif
}

//-------------------------Set ADCs------------------------------------------
void setADC() {
#ifdef PRECISION_READ
  //mas resolucion
  //analogReadResolution(10);
#else
  //descartar 2 bits menos significativos
  //analogReadResolution(8);
#endif
}

//-------------------------Set Serial-----------------------------------------
void setSerial() {
#ifdef SLOW_CLOCK
  Serial.begin(19200);
#else
  Serial.begin(9600);
#endif

  while (!Serial) {
    //esperar a que se abra puerto
  }
  //Serial.println("Exito");
}

//------------------------Set GPS---------------------------------------------
void setGPS(){
  #ifdef SLOW_CLOCK
    GPS_Serial.begin(19200);
  #else
    GPS_Serial.begin(9600);
  #endif
}

//-----------------------Temperature Management-------------------------------
void tempRoutine(){
    getMaximum();
    getMinimum();
    float average=getAverage();
    Serial.print("Max today ");
    Serial.print(temperatures[maxPos].measurement);
    Serial.print(" at: ");
    Serial.println(temperatures[maxPos]._time); 
    Serial.print("Min today ");
    Serial.print(temperatures[minPos].measurement);
    Serial.flush();
    Serial.print(" at: ");
    Serial.println(temperatures[minPos]._time);
    Serial.print("Average today ");
    Serial.println(average);
    Serial.flush();
}



void registerTemperature(){
  if ( !ds.search(addr)) {
    ds.reset_search();
    delay(250);
    thermoCounter++;
    return;
  }
//Get ROM info
/*Serial.print("ROM =");
  for( i = 0; i < 8; i++) {
    Serial.write(' ');
    Serial.print(addr[i], HEX);
  }*/
//Check CRC is valid
  if (OneWire::crc8(addr, 7) != addr[7]) {
      //Serial.println("CRC is not valid!");
      thermoCounter++;
      return;
  }
  Serial.println();
  // the first ROM byte indicates which chip
  switch (addr[0]) {
    case 0x10:
      //Serial.println("  Chip = DS18S20");  // or old DS1820
      type_s = 1;
      break;
    case 0x28:
      //Serial.println("  Chip = DS18B20");
      type_s = 0;
      break;
    case 0x22:
      //Serial.println("  Chip = DS1822");
      type_s = 0;
      break;
    default:
      //Serial.println("Device is not a DS18x20 family device.");
      thermoCounter++;
      return;
  } 
  thermoCounter=0;
  thermoBounce=1;
  ds.reset();
  ds.select(addr);
  ds.write(0x44, 1);        // start conversion, with parasite power on at the end
  
  delay(800);     // A little more than 750 ms here

  present = ds.reset();
  ds.select(addr);    
  ds.write(0xBE);         // Read Scratchpad

/*  Serial.print("  Data = ");
  Serial.print(present, HEX);
  Serial.print(" ");*/
  for ( i = 0; i < 9; i++) {           // 9 bytes
    dataTemperature[i] = ds.read();
    //Serial.print(data[i], HEX);
    //Serial.print(" ");
  }
 /* Serial.print(" CRC=");
  Serial.print(OneWire::crc8(data, 8), HEX);
  Serial.println();*/
  // Convert the data to actual temperature
  // because the result is a 16 bit signed integer, it should
  // be stored to an "int16_t" type, which is always 16 bits
  // even when compiled on a 32 bit processor.
  int16_t raw = (dataTemperature[1] << 8) | dataTemperature[0];
  if (type_s) {
    raw = raw << 3; // 9 bit resolution default
    if (dataTemperature[7] == 0x10) {
      // "count remain" gives full 12 bit resolution
      raw = (raw & 0xFFF0) + 12 - dataTemperature[6];
    }
  } else {
    byte cfg = (dataTemperature[4] & 0x60);
    // at lower res, the low bits are undefined, so let's zero them
    if (cfg == 0x00) raw = raw & ~7;  // 9 bit resolution, 93.75 ms
    else if (cfg == 0x20) raw = raw & ~3; // 10 bit res, 187.5 ms
    else if (cfg == 0x40) raw = raw & ~1; // 11 bit res, 375 ms
    //// default is 12 bit resolution, 750 ms conversion time
  }
  celsius = (float)raw / 16.0;
  fahrenheit = celsius * 1.8 + 32.0;
/*  Serial.print(celsius);
  Serial.println(" Celsius");
  Serial.print(fahrenheit);
  Serial.println(" Fahrenheit");
  Serial.flush();*/
}  

void temperatureArray(){


  temperatures[contT-1]= (temperature) {celsius,contH};
  Serial.print("Celsius:" );
  Serial.println(celsius);
  Serial.print("Hour: ");
  Serial.println(contH);
  Serial.flush();

   contT++;
  if(contH==(measurementsNumber/2)){
    Serial.println("Day over");
    Serial.flush();
    contT=0;
    hacerEnvio=1;
    contH=0;
  }
  
  contH=contT/2;
  if(contT%2!=0){
    contH=contH+0.3;
  }
}

//-------------------------------------Arithmetic Methods---------------------------
void getMaximum(){
  
  float maximum=temperatures[0].measurement;
  for(int i=0; i<measurementsNumber;i++){
    if(maximum<temperatures[i].measurement){
      maxPos=i;
    }
  }
}

void getMinimum(){
  
  float minimum=temperatures[0].measurement;
  for(int i=0; i<measurementsNumber;i++){
    if(minimum>temperatures[i].measurement){
      minPos=i;
    }
  }
}

float getAverage(){
  
  float average=0;
  for(int i=0; i<measurementsNumber;i++){
    average=average+temperatures[i].measurement;    
  }
  average=average/measurementsNumber;
  return average;  
}

//-----------------------------------GPS Methods------------------------------------
void gpsRoutine(){

  while(lattitude&&altitude==0.0){
    checkHeader();
    frameSkip(11);
    checkDataReady();
    frameSkip(1);
    getLattitude();
    frameSkip(3); 
    getAltitude();
    newLineWait();
  }
 lattitude=0.0;
 altitude=0.0;
}




//-----------------------------------------GPS Management----------------------------
//wait until there's data. Emergency error break in while loop
char readGPS(){
  int emergency = 0;
  char rc;

  while (!GPS_Serial.available()) {
    emergency++;
    delay(10);
    if (emergency > 1000) {
      Serial.println("GPS or Serial fatal error");
      break;
    }
  }
  rc = GPS_Serial.read();
  return rc;
}



//------------------------------GPS String Analysis----------------------------------
//Checks for header stored in data[] array
void checkHeader(){
  if (newGpsLine == 1) {
    for (int i = 1; i < 6; i++) {
      rc = readGPS();
      if (rc != dataGPS[i]) {
        newGpsLine = 0;
      } else {
        Serial.print(rc);
      }
    }
  }  
  altitudeA[11]={0};
  lattitudeA[10]={0};
}

//Moves n=skippedBits number of bits only if header was found
void frameSkip(int skippedBits){
  if (newGpsLine == 1) {
    for (int i = 0; i < skippedBits; i++) {
      rc = readGPS();
    }
    Serial.println(" ");
  }  
}

//Comapres with char "check" to see if GPS location data is available only if header was found
void checkDataReady(){
    if (newGpsLine == 1) {
      rc = readGPS();
      Serial.println(rc);
      if (rc != check) {
        newGpsLine == 0;
        Serial.println("No connection");
        newGpsLine=0;
      }else {
        Serial.println("Data ready");
    }
  }
}

//Gets the corresponding lattitude only if header was found
void getLattitude(){
  if (newGpsLine == 1) {
    Serial.println(" ");
    for (int i = 0; i<= 9; i++) {
      rc = readGPS();
      lattitudeA[i]=rc;
      Serial.print(rc);
    }
    Serial.println("");
    lattitude=atof(&lattitudeA[10]);
    Serial.print("Latitude ");
    Serial.println(lattitudeA);
    Serial.flush();
  }   
}
//Gets the corresponsing altitude only if header was found
void getAltitude(){
  if (newGpsLine == 1) {
    Serial.println(" ");
    for (int i = 0; i <= 10; i++) {
      rc = readGPS();
      altitudeA[i]=rc;
      Serial.print(rc);
    }
    Serial.println("");
    altitude=atof(&altitudeA[11]);
    Serial.println("Altitude ");
    Serial.println(altitudeA);
    Serial.flush();
  }
  gpsRoutineDone=1;
}

//Reads bits from GPS until new line is found. New lines all start with '$'
void newLineWait(){
  while(newGpsLine!=1){
    rc=readGPS();
    if(rc=='$') {
      newGpsLine = 1;
      Serial.print(rc);
    }
  }  
}


