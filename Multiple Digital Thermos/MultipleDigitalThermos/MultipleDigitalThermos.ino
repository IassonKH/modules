/* This sketch looks for 1-wire devices and
  prints their addresses (serial number) to
  the UART, in a format that is useful in Arduino sketches
  Tutorial:
  http://www.hacktronics.com/Tutorials/arduino-1-wire-address-finder.html
*/

#define Thermometer 10

#include <OneWire.h>

OneWire  ds(Thermometer);  // Connect your 1-wire device to pin 10


byte addrDirectory[3][8];            //maximum amount of thermometers permitted
byte thermosFound = 0;      //counter for total amount of thermometers found

void setup(void) {
  Serial.begin(115200);
  discoverOneWireDevices();
}

void discoverOneWireDevices(void) {
  byte i;
  byte present = 0;
  byte data[12];
  byte addr[8];

  Serial.print("Looking for 1-Wire devices...\n\r");
  while (ds.search(addr)) {
    Serial.print("\n\rFound \'1-Wire\' device with address:\n\r");
    for ( i = 0; i < 8; i++) {
      Serial.print("0x");
      if (addr[i] < 16) {
        Serial.print('0');
      }
      addrDirectory[thermosFound][i] = addr[i];
      Serial.print(addr[i], HEX);
      if (i < 7) {
        Serial.print(", ");
      }
    }
    if ( OneWire::crc8( addr, 7) != addr[7]) {
      Serial.print("CRC is not valid!\n");
      return;
    } else {
      thermosFound++;
    }
  }
  Serial.print("\n\r\n\rThat's it.\r\n");
  ds.reset_search();
  return;
}

void loop(){
 
  for (int i = 0;i < thermosFound; i++ ) {
    Serial.print("Found with address: ");
    for (int j = 0;j < 8;j++) {
      
      if (addrDirectory[i][j] < 16) {
        Serial.print('0');
      }
      Serial.print(addrDirectory[i][j],HEX);
    }
    Serial.println("");
    Serial.println("");
  }

  delay(500);
}
