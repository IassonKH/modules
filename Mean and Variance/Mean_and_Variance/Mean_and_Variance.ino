//This program takes an int at the start which equals the amount of samples of a RNG
// it will send to a computer to restart it press reset
#define LED             13


//sample variance without mean formula:

/*
            1    n  2    1    n   2
      vx = --- [ Exi  - --- ( Exi ) ]
           n-1  i=1      n   i=1

    Formula is adapted for: Var(X-K)=Var(X) to avoid overflow, K being the first reading
*/

//-------------------------------------Macros------------------------------
//Set to 1,0 or read bit at "pos" of byte "by" respectively
#define BOOL_TRUE(by,pos)  by|=pos
#define BOOL_FALSE(by,pos) by&= ~(pos)
#define BOOL_READ(by,pos)  bool(by&pos)

#define baud 115200

#include "SoftwareSerial.h"
#include "SimpleTimer.h"

long samples = 250000;
long timeStart;
long timeEnd;
long counter = 0; //will count how many numbers I have generated to stop at #samples

typedef struct Variance {
  //algorithm taken from Welfrod's algorithm. Avoids catastrophic cancellation
  //also new reading will always be xn-k. K being constant defined at rest state
  long k = 0;      // constant to reduce size of calculatiosn. var(x-k)=var(x)
  long count = 0;  // # of readings taken
  double mean = 0;   // delta1/count
  double M2 = 0;     // delta1*delta2
  double variance = 0; // M2/count
} Variance;

Variance varianceX;


void setup() {
  // put your setup code here, to run once:
  setSerial();
  randomSeed(1);
  timeStart = millis();
  //k = random(-290.00, 290.00); //first generated sample
}

void loop() {

  long rnd = 0.0;

  while (counter < long(samples)) {
    rnd = random(-290, 290);
    //Serial.print(F("NV:"));
    //Serial.println(rnd);

    updateM2(rnd, &varianceX);
    calculateVariance(&varianceX);
    /*
        Serial.print(F("V:"));
        Serial.println(varianceX.variance);
        Serial.print(F("nV:"));
        Serial.println(rnd);
        Serial.println();
    */
    counter++;
  }

  timeEnd = millis();
  Serial.print(F("V:"));
  Serial.println(varianceX.variance);
  Serial.print(F("SD:"));
  Serial.println(sqrt(varianceX.variance));
  Serial.print(F("u:"));
  Serial.println(varianceX.mean);
  Serial.println();

  Serial.print(F("Done it took: "));
  Serial.print(long(timeEnd - timeStart));
  Serial.println(F(" seconds"));
  while (true);
}

void setSerial() {
  Serial.begin(long(baud));
  while (!Serial) {
    //wait for port to open
  }
  Serial.println(F("R"));
}



//updates M2 and mean
void updateM2(long newValue, struct Variance *variance1) {

  double delta1 = 0; // newValue-mean
  double delta2 = 0; // newValue-(Updated)mean

  (*variance1).count++; //+1 to sample size
  delta1 = (newValue - (*variance1).k) - (*variance1).mean; //xn-(past mean)
  (*variance1).mean += delta1 / (*variance1).count; //update mean
  delta2 = (newValue - (*variance1).k) - (*variance1).mean; //xn-mean
  (*variance1).M2 += delta1 * delta2;    //final result
 
}

//calculate variance
void calculateVariance(struct Variance *variance1) {
  if ((*variance1).count < 2) {
  }
  else {
    (*variance1).variance = (*variance1).M2 / (*variance1).count;
  }
}

