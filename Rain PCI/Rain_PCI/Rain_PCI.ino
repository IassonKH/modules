
float in = 0;
float mm = 0;
bool rainState;

void pciSetup(int pin) {
  *digitalPinToPCMSK(pin) |= bit (digitalPinToPCMSKbit(pin));  // enable pin
  PCIFR  |= bit (digitalPinToPCICRbit(pin)); // clear any outstanding interrupt
  PCICR  |= bit (digitalPinToPCICRbit(pin)); // enable interrupt for the group
}

void setup() {
  //internal pull-up
  rainState=digitalRead(A0);
  pciSetup(A0);
  // put your setup code here, to run once:
  Serial.begin(115200);
  Serial.println(F("Ready"));
}

void loop() {
  // put your main code here, to run repeatedly:

}


// handle pin change interrupt for A0 to A5 here
ISR (PCINT1_vect) {
  if (rainState!=digitalRead(A0)) {
    in = in + 0.01;
    mm = in * 25.4;
    Serial.println(mm);
    rainState=!rainState;
  }
}

