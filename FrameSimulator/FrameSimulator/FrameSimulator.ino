/*
  PCINT2

  Pin D0 TX Serial Programming/debug
  Pin D1 RX Serial Programming/debug
  Pin D2 Anemometer Interrupts for wind direction (green wire)
  Pin D3 Anemometer Interrupts for wind speed (yellow wire)
  Pin D4 Enable 1
  Pin D5 Enable 2
  Pin D6 Enable 3
  Pin D7 SDI-12 Data Pin for soil sensors(blue wire)

  PCINT0

  Pin D8 Higrometer Input Capture Unit
  Pin D9 TX from GPS
  Pin D10 Thermometer Data
  Pin D11 RX Iridium Satellite
  Pin D12 TX Iridium Satellite
  Pin D13 Switch Select 3 (13)

  PCINT1

  Pin A0 Switch Select 1 (14)
  Pin A1 Switch Select 2 (15)
  Pin A2 Push-Button
  Pin A3 Analog Readings (17)
  Pin A4 SDA from RTC
  Pin A5 SCL from RTC
  Pin A6 Analog only
  Pin A7 Analog only

  Switch States
  |A0 | A1 |
  | 0 |  0 | Default Working Mode
  | 0 |  1 | Dev/debug Mode (serial messages printed and reduced run time)
  | 1 |  0 | SDI Address Assign Mode Discovery
  | 1 |  1 |
*/

//------------------------------------Application Notes------------------
/*
  Transistor must be used for powering up GPS,SDI,Higrometer with a digital pin, since not enough current can be given by the pins
  Pin D13 is technically free most of the time. GPS cannot recieve data and Iridium is powered down with stepper
  even so, care must be taken when using said pin.

    Digital Pins 0-7  - Port D - PCINT2
    Digital Pins 8-13 - Port B - PCINT0
    Analog Pins A0-A5 - Port C - PCINT1

  SDI and software Serial both try to assign this pins. SDI is in pcint 2(pin 7), sw keeps everyother one
  modify .cpp by commenting out declaration as seen here depending on application:
  https://github.com/EnviroDIY/Arduino-SDI-12/issues/8
  Software Serial.cpp is in your computer at Arduino/hardware/arduino/avr/libraries/SoftwareSerial/src

  DO NOT USE ANY SOFTWARE SERIALS ON DIGITAL PINS 0-7!!!

  SDI12 library taken from here: https://github.com/EnviroDIY/Arduino-SDI-12
  Iridium library taken from here:https://github.com/mikalhart/IridiumSBD
  RTClibrary taken from here: https://github.com/adafruit/RTClib
  SimpleTimer library taken from here: https://github.com/jfturcot/SimpleTimer
*/

//------------------------------------Definitions------------------------
#define DEVMOD

#define DS1307_I2C_ADDRESS 0x68


#define vaneInterrupt 0
#define anemometerInterrupt 1

//Pins 4 and 5 for transistor switching, 13 is debug Led
#define vanePin         2
#define anemometerPin   3
#define IridiumControl  4
#define GPSControl      5
#define DATAPIN         7
#define Thermos         10
#define LED             13
#define Switch3         13
#define Switch1         14
#define Switch2         15
#define Button          16
#define ADCPin          17

#define measurementsNumber 1 //times I will take measurements before sending data

//bit position of each flag
#define send_Data           1
#define thermoBounce        2
#define newGPSLine          4
#define gpsRoutineDone      8
#define RTCFail             16
#define GPSPresent          32
#define ThermoPresent       64
#define min30               128
#define noGPS_Signal        1
#define success             2
#define noAltitude          4
#define found               8
#define first               16
#define triggered           32
#define interruptAnemometer 1
#define interruptVane       2
#define cycle1              4
#define cycle2              8
#define seconds5            16
#define anemometerP         32
#define windGust            64
#define debugFlag           128


//-------------------------------------Macros------------------------------
//Set to 1,0 or read bit at "pos" of byte "by" respectively
#define BOOL_TRUE(by,pos)  by|=pos
#define BOOL_FALSE(by,pos) by&= ~(pos)
#define BOOL_READ(by,pos)  bool(by&pos)

#include "SoftwareSerial.h"
#include "SimpleTimer.h"

//----------------------------------Constants & Global variables----------

byte flags1 = 0;
byte flags2 = 0;
byte flags3 = 0;


short takeReading = 0; //value in seconds between each data acquisition

SimpleTimer timerSeconds; // timer for seconds declared with SimpleTimer Library


unsigned long seconds = 0; // seconds counter
unsigned long referenceTime = 0; //will reset on year 2038

short frameType = 0;

void setup() {
  //sets serial comms
  setSerial();
  //initialize sensors and environment
  initialize();
}


void initialize() {
  analogReference(DEFAULT);
  timerSeconds.setInterval(1000, secondsCount);
  interrupts();
  randomSeed(analogRead(0));
  Serial.print(F("Initialisation Complete"));
  Serial.print('\r');
}

//-------------------------Serial Comms-----------------------------------------------
void setSerial() {
  Serial.begin(9600);
  while (!Serial) {
    //wait for port to open
  }
}


void loop() {
  //this method updates everytime a second passes
  timerSeconds.run();
  //----------------Data Send----------------------------------------
  sendData();
}



//------------------------------------Data Send--------------------------
void sendData() {

  if (BOOL_READ(flags1, send_Data)) {
    BOOL_FALSE(flags1, send_Data);
    satelliteRoutine();
    seconds = 0;
  }
}


void satelliteRoutine() {

  if (frameType > 14) {
    frameType = 0;
  }

  if (frameType < 12) {
    frameRoutine0();
  }

  if (frameType == 12) {
    frameRoutine1();
  }

  if (frameType == 13) {
    frameRoutine2();
  }

  if (frameType == 14) {
    frameRoutine3();
  }

  frameType++;

  Serial.flush();
}

//-------------------------------------------------------------Build frames depending on what Data was available/is requiered
void frameRoutine0() {

  //reserve() method makes an "array" of the string and instead of causing memory fragmentation, makes it so
  //the string is manipulated inside it. once the method ends space is freed
  String stringFrame=F("GX8K2F3T,");
  String temporal;

  stringFrame.reserve(430);
  

  temporal = String_RTC();
  stringFrame += temporal;
  //Serial.println(temporal);

  //temporal = String_GPS();
  //stringFrame += temporal;
  //Serial.println(temporal);

  temporal = String_Thermo();
  stringFrame += temporal;
  //Serial.println(temporal);

  temporal = String_SDI12();
  stringFrame += temporal;
  // (temporal);

  temporal = String_WindVane();
  stringFrame += temporal;
  //Serial.println(temporal);

  temporal = String_Rain();
  stringFrame += temporal;
  //Serial.println(temporal);

  temporal = String_Higrometer();
  stringFrame += temporal;
  //Serial.println(temporal);

  temporal = String_CRC(stringFrame);

  /*
    byte (*ptr);
    ptr=(byte)&stringFrame;
    temporal = String_CRCbeta(ptr, int(stringFrame.length()));
  */
  stringFrame = "#" + stringFrame + temporal;
  char *chars = const_cast<char*>(stringFrame.c_str());

  Serial.print(chars);
  Serial.print('\r');
  Serial.flush();
}

void frameRoutine1() {

  //reserve() method makes an "array" of the string and instead of causing memory fragmentation, makes it so
  //the string is manipulated inside it. once the method ends space is freed
  String stringFrame=F("GX8K2F3T,");
  String temporal;

  stringFrame.reserve(450);
  

  temporal = String_RTC();
  stringFrame += temporal;
  //Serial.println(temporal);

  temporal = String_GPS();
  //stringFrame += temporal;
  //Serial.println(temporal);

  temporal = String_Thermo();
  stringFrame += temporal;
  //Serial.println(temporal);

  temporal = String_SDI12();
  stringFrame += temporal;
  //Serial.println(temporal);

  temporal = String_WindVane();
  stringFrame += temporal;
  //Serial.println(temporal);

  temporal = String_Rain();
  stringFrame += temporal;
  //Serial.println(temporal);

  temporal = String_Higrometer();
  stringFrame += temporal;
  //Serial.println(temporal);

  temporal = String_CRC(stringFrame);
  //temporal = String_CRC(temporal);
  /*
    byte (*ptr);
    ptr=(byte)&stringFrame;
    temporal = String_CRCbeta(ptr, int(stringFrame.length()));
  */
  stringFrame = "#" + stringFrame + temporal;
  char *chars = const_cast<char*>(stringFrame.c_str());

  Serial.print('\r');
  Serial.print(chars);
  Serial.print('\r');
  Serial.flush();
}

void frameRoutine2() {

  //reserve() method makes an "array" of the string and instead of causing memory fragmentation, makes it so
  //the string is manipulated inside it. once the method ends space is freed
  String stringFrame=F("GX8K2F3T,");
  String temporal;

  stringFrame.reserve(450);

  temporal = String_RTC();
  stringFrame += temporal;
  //Serial.println(temporal);

  temporal = String_GPS();
  stringFrame += temporal;
  //Serial.println(temporal);

  temporal = String_Thermo();
  stringFrame += temporal;
  //Serial.println(temporal);


  temporal = String_WindVane();
  stringFrame += temporal;
  //Serial.println(temporal);

  /*
      byte (*ptr);
      ptr=(byte)&stringFrame;
      temporal = String_CRCbeta(ptr, int(stringFrame.length()));
  */
  stringFrame = "#" + stringFrame + temporal;
  char *chars = const_cast<char*>(stringFrame.c_str());

  Serial.print('\r');
  Serial.print(chars);
  Serial.print('\r');
  Serial.flush();
}


void frameRoutine3() {

  //reserve() method makes an "array" of the string and instead of causing memory fragmentation, makes it so
  //the string is manipulated inside it. once the method ends space is freed
  String stringFrame=F("GX8K2F3T,");
  String temporal;

  stringFrame.reserve(450);

  temporal = String_RTC1();
  stringFrame += temporal;
  //Serial.println(temporal);

  temporal = String_GPS1();
  stringFrame += temporal;
  //Serial.println(temporal);

  temporal = String_Thermo1();
  stringFrame += temporal;
  //Serial.println(temporal);

  temporal = String_SDI121();
  stringFrame += temporal;
  //Serial.println(temporal);

  temporal = String_WindVane1();
  stringFrame += temporal;
  //Serial.println(temporal);

  temporal = String_Rain1();
  stringFrame += temporal;
  //Serial.println(temporal);

  temporal = String_Higrometer1();
  stringFrame += temporal;
  //Serial.println(temporal);

  temporal = String_CRC(stringFrame);
  /*
    byte (*ptr);
    ptr=(byte)&stringFrame;
    temporal = String_CRCbeta(ptr, int(stringFrame.length()));
  */
  stringFrame = "#" + stringFrame + temporal;
  char *chars = const_cast<char*>(stringFrame.c_str());

  Serial.print('\r');
  Serial.print(chars);
  Serial.print('\r');
  Serial.flush();
}




String String_RTC() {

  String RTC_String = "";
  long randomTime = random(1515365448, 1535365448);
  //timestamp
  RTC_String = F("RTC:");
  RTC_String += String(randomTime, DEC);
  RTC_String += F(",");

  if (frameType == 8) {
    RTC_String = "";
  }

  return RTC_String;
}

String String_GPS() {

  float longitude = -99.177718 + random(0, 5);
  float latitude = 19.375873 + random(0, 5);
  float altitude = 2250 + random(0, 5);

  String GPS_String = "";
  if (frameType != 6) {
    GPS_String += F("lng:");
    GPS_String += String(longitude, 7) + ",";

    GPS_String += F("lat:");
    GPS_String += String(latitude, 7) + ",";
    if (frameType != 7) {
      GPS_String += F("alt:");
      GPS_String += String(altitude, 2) + ",";
    }
  }
  //timestamp
  return GPS_String;
}


String String_SDI12() {

  String soil = "";
  //taken used to keep count of where we are on the SDI array
  //everytime we find one we increase by one
  if (frameType != 1) {
    soil += F("soilwc:19.2,soiltemp:28.2,soilbrp:10.2,soilec:274,soilpwec:3535@ABC123DE,");
    if (frameType != 2) {
      soil += F("soilwc:24.7,soiltemp:27.3,soilbrp:13.2,soilec:445,soilpwec:3956@zxYO0wQ3,");
      soil += F("soilwc:17.9,soiltemp:28.3,soilbrp:9.5,soilec:257,soilpwec:3738@zxYO0wQ3,");
    }
  }

  if (frameType == 5) {
    soil = "";
  }
  return soil;
}

String String_Thermo() {
  String thermo_String;
  float temp = random(-30.0, 35.0);
  if (frameType != 3  ) {
    thermo_String = F("temp:");
    thermo_String += String(temp, 2);
    thermo_String += F("@lkjhGFDS,");
    if (frameType != 4) {
      thermo_String += F("temp:");
      temp = random(-30.0, 35.0);
      thermo_String += String(temp, 2);
      thermo_String += F("@91827364,");


      thermo_String += F("temp:");
      temp = random(-30.0, 35.0);
      thermo_String += String(temp, 2);
      thermo_String += F("@WMEd1494,");

    }
  }
  if (frameType == 5) {
    thermo_String = "";
  }
  return thermo_String;
}

String String_WindVane() {

  String meditions = F("winddir:");
  int angle = random(0, 359);
  meditions += String(angle, DEC);
  meditions += F(",windspeed2m:");
  angle = random(0, 35);
  meditions += String(angle, DEC) + ",";


  if (angle >= 15) {
    meditions += F("windgust:");
    meditions += String(millis(), DEC);
    meditions += F(",");
  }

  if (frameType == 9) {
    meditions = "";
  }

  return meditions;
}


String String_Rain() {

  String rain = F("pre1m:");
  float average = random(0.0, 70.5);

  rain += String(average, 2);
  rain += F(",");

  if (frameType == 10) {
    rain = "";
  }

  return rain;
}

String String_Higrometer() {

  String interval = F("atrh:");

  int ADCValue = random(0, 35);
  float humidity = random(0.0, 99.9);
  interval += String(humidity, 2);
  if (frameType != 11) {
    interval += F(",att:");
    interval += String(ADCValue, DEC);
  }

  return interval;
}


String String_RTC1() {

  String RTC_String = "";
  long randomTime = random(0, 448);
  //timestamp
  RTC_String = F("RTC:");
  RTC_String += String(randomTime, DEC);
  RTC_String += F(",");

  if (frameType == 8) {
    RTC_String = "";
  }

  return RTC_String;
}

String String_GPS1() {

  float longitude = -2000.177718 + random(0, 5);
  float latitude = 190.375873 + random(0, 5);
  float altitude = -3000 + random(0, 5);

  String GPS_String = "";
  if (frameType != 6) {
    GPS_String += F("lng:");
    GPS_String += String(longitude, 7) + ",";

    GPS_String += F("lat:");
    GPS_String += String(latitude, 7) + ",";
    if (frameType != 7) {
      GPS_String += F("alt:");
      GPS_String += String(altitude, 2) + ",";
    }
  }
  //timestamp
  return GPS_String;
}


String String_SDI121() {

  String soil = "";
  //taken used to keep count of where we are on the SDI array
  //everytime we find one we increase by one
  if (frameType != 1) {
    soil += F("soilwc:50000,soiltemp:-100,soilbrp:12121,soilec:12349,soilpwec:89912@ABC123DE,");
    if (frameType != 2) {
      soil += F("soilwc:-55555,soiltemp:12346,soilbrp:19892,soilec:293841,soilpwec:123495@zxYO0wQ3,");
    }
  }

  if (frameType == 5) {
    soil = "";
  }
  return soil;
}

String String_Thermo1() {
  String thermo_String;
  float temp = random(120, 200);
  if (frameType != 3  ) {
    thermo_String = F("temp:");
    thermo_String += String(temp, 2);
    thermo_String += F("@lkjhGFDS,");
    if (frameType != 4) {
      thermo_String += F("temp:");
      temp = random(120, 200);
      thermo_String += String(temp, 2);
      thermo_String += F("@91827364,");


      thermo_String += F("temp:");
      temp = random(120, 200);
      thermo_String += String(temp, 2);
      thermo_String += F("@WMEd1494,");

    }
  }
  if (frameType == 5) {
    thermo_String = "";
  }
  return thermo_String;
}

String String_WindVane1() {

  String meditions = F("winddir:");
  int angle = random(361, 720);
  meditions += String(angle, DEC);
  meditions += F(",windspeed2m:");
  angle = random(1000, 2000);
  meditions += String(angle, DEC) + ",";


  if (angle >= 15) {
    meditions += F("windgust:");
    meditions += String(millis(), DEC);
    meditions += F(",");
  }

  if (frameType == 9) {
    meditions = "";
  }

  return meditions;
}


String String_Rain1() {

  String rain = F("pre1m:");
  float average = random(10000, 20000);

  rain += String(average, 2);
  rain += F(",");

  if (frameType == 10) {
    rain = "";
  }

  return rain;
}

String String_Higrometer1() {

  String interval = F("atrh:");

  int ADCValue = random(120, 400);
  float humidity = random(500, 550);
  interval += String(humidity, 2);
  if (frameType != 11) {
    interval += F(",att:");
    interval += String(ADCValue, DEC);
  }

  return interval;
}

/*
  String String_CRCbeta(byte *stringFrame, int stringSize) {
  String crcOutput;
  byte crc = 0x00;

  while (stringSize--) {
    crc ^= *stringFrame++;
    for (byte i = 0; i < 8; i++) {
      if (crc & 0x80) crc = (crc << 1) ^ 0x07;
      else crc <<= 1;
    }
  }

  if (String(crc, HEX).length() == 1) {
    crcOutput = "0" + String(crc, HEX);
  } else {
    crcOutput = String(crc, HEX);
  }

  crcOutput = "*" + crcOutput ;

  return crcOutput;

  }*/

String String_CRC(String stringFrame) {
  String crc;

  byte bytes[stringFrame.length() + 1];
  stringFrame.getBytes(bytes, long(stringFrame.length() + 1));
  byte crc8 = CRC8(bytes, stringFrame.length());


  if (String(crc8, HEX).length() == 1) {
    crc = "0" + String(crc8, HEX);
  } else {
    crc = String(crc8, HEX);
  }

  crc = "*" + crc ;

  return crc;
}

byte CRC8(const byte *data, long len) {
 
  
  byte crc = 0x00;
  while (len--) {
    crc ^= *data++;
    for (byte i = 0; i < 8; i++) {
      if (crc & 0x80) crc = (crc << 1) ^ 0x07;
      else crc <<= 1;
    }
  }
  return crc;
}



//-----------------------------------------------Extra Timer------------------------------

/*
   Timer for time period, executed every second
*/
void secondsCount() {
  //flash led
  digitalWrite(LED, !digitalRead(LED));
  //seconds counts days, takeReading data acquisition period, 5 count and 60 count are for windVane
  takeReading++;
  //reading every 60 seconds
  if (takeReading >= 10) {

    BOOL_TRUE(flags1, send_Data);
    takeReading = 0;
  }

}



